
% !TEX root = NotesDeCours_VersionProf.tex


\part{Ecoulements en conduite}
%\section*{\bfseries 9. Ecoulements en conduite}
%\setcounter{section}{8}
\section*{\bfseries 6. Ecoulements en conduite}


% ================================================================================================ 
% Page de titre :
% ================================================================================================

\begin{frame}

  \color{bleu}

  \begin{flushleft}
    
    \Large
   	\bf
    
    Mécanique des fluides 

  \end{flushleft}
  
  \ligne{3} % remplace: \noindent \thickline{0.5mm}{150}

  \begin{flushright}

    \rm

    \textrm{David} \textsc{Fabre}
    
    \vspace{3mm}
    
    IMFT / UPS
    
    Département de Mécanique
    
    %brancher@imft.fr

  \end{flushright}

\begin{picture}(110, 30)(-3, 2)
  \put( 0,  -0.5){\includegraphics[height=47mm]{osbourne_reynolds1904_crop.jpg}}
  \put( 65, 0){\color{gris} \small \rm Tableau de J. Collierde (1904)}
  \put( 65, 6){\color{gris} \small \rm Osborne REYNOLDS}
  \put( 65, 3){\color{gris} \small \rm (1842 -- 1912)}
\end{picture}

  \vspace{7mm}
  
  \begin{flushright}
    
    \Large
   	\bf
    
     \hypertarget{Chap6}{6. Introduction à la turbulence  : Pertes de charges et couches limites }

  \end{flushright}

  \vspace{5mm}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sommaire :
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\input{QC_Turbulence}

\begin{frame}{Objectifs du chapitre}

\begin{itemize}
\item Savoir identifier et décrire les régimes d'écoulement 
\begin{itemize}
\item Laminaire 
\item Transitionnel
\item Turbulent
\end{itemize}
\item Savoir utiliser l'analyse dimensionnelle et les lois empiriques expérimentales pour prédire :
\begin{itemize}
\item La perte de charge dans une conduite cylindrique,
\item Le frottement sur une paroi plane.
\end{itemize}
\end{itemize}


\small
  

\end{frame}



\handout{
\begin{frame}{Sommaire}

\small
  
\hspace*{2mm}
\begin{tabular}{cc}
		%&
  		\begin{minipage}{62mm}
  			\tableofcontents[firstsection=-7]
      \vspace{15mm}
  		\end{minipage}
  		&   
  		\begin{minipage}{60cm}
		  \vspace*{-5mm}  
  			%\includegraphics[width=40mm]{vagues.jpg} 
  		\end{minipage}
  	\end{tabular}

\vspace{0mm}

\end{frame}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%==========================================================================================
\subsection{Ecoulements en conduite}
%=========================================================================================

%-----------------------------------------------------------------------------------------
\subsubsection{Expérience de Reynolds}
%-----------------------------------------------------------------------------------------
\begin{frame}{Expérience de Reynolds (1883)}
%-----------------------------------------------------------------------------------------

\small

\begin{center}
	\begin{picture}(112, 46)(5, -5)
		\put(0, 0){\includegraphics[width=45mm]{Reynolds_fluid_turbulence_experiment_1883.jpg}}
		\put(60, 6){\includegraphics[width=50mm]{manip_Reynolds_photo.jpg}}
		\put(61, 3){Banc expérimental utilisé par Reynolds,}
		\put(61, 0){exposé à l'Université de Manchester (UK)}
	\end{picture}
\end{center}

\vspace{-10mm}

\begin{center}
	\begin{picture}(97, 15)
		\put(7, 13){\color{rouge} \vector(0, -1){3}}
		\put(10, 10){\color{rouge} injection de colorant}
		\put(7, 0){\includegraphics[width=80mm]{principe_experience_Reynolds.png}}
		\put(88, 2.5){$D=2R$}
		\put(0, 3){\color{bleu} \vector(1, 0){5}}
		\put(1, 4.5){\color{bleu} $U$}
		\put(0, 0){\color{bleu} $\rho, \nu$}
	\end{picture}
\end{center}
\smallskip \pause \qquad  {\color{bleu} $U = \dot m / (\rho S)$ vitesse moyenne ou vitesse débitante, déduite du débit-masse $\dot m$ et de la section $S = \pi R^2$. }

\smallskip \pause {\bf Remarque : } si la section est constante $U$ est constante le long du tuyau

(En revanche si $S$ varie, $U$ varie comme $1/S$).

\end{frame}

\subsubsection{Analyse dimensionnelle}

\begin{frame}{Perte de charge}
\small

{\bf Définition :} 
On appelle charge hydraulique la quantité $e_m = p+\rho g z + \frac{\rho U^2}{2}$.

\medskip \pause
On verra au chapitre 8 qu'il s'agit d'une {\color{rouge} Energie mécanique par unité de volume}.


\medskip \pause
{\bf Définition :} 

On appelle \textcolor{vert}{perte de charge}  la diminution
d'\'energie m\'ecanique entre deux sections d'une conduite rectiligne.

\begin{eqnarray*}
\Delta _1^2 %&=& \rho( \bar{e}_{m_1} - \bar{e}_{m_2} )  \\
               &=& (P_1 + \rho g z_1 + \dfrac{1}{2}\rho U_1^2) 
		- (P_2 + \rho g z_2  + \dfrac{1}{2}\rho U_2^2) \\
\end{eqnarray*}

\medskip \pause Dans le cas d'un tuyau de section $S$, constante, on parle de \textcolor{vert}{perte de charge régulière}. 
Celle-ci est équivalente à une chute de la "pression étoilée" $p^*$.
$$
\Delta _1^2 = (P_1 + \rho g z_1 )  - (P_2 + \rho g z_2 ) \equiv p^*_1 - p^*_2
$$

\end{frame}




%-----------------------------------------------------------------------------------------

%-----------------------------------------------------------------------------------------
\begin{frame}{Analyse dimensionnelle du problème (I)}
%-----------------------------------------------------------------------------------------

\small


\begin{center}
  \begin{picture}(85, 28)(0, 5)
     \put(0, 0){\includegraphics[width=8cm]{conduite.pdf}}
     \put(0, 20){$P_1$}
     \put(0, 17){$z_1$}
     \put(80, 20){$P_2$}
     \put(80,17){$z_2$}
     \put(41, 20){$D$}
     \put(15, 23){$U$}
     \put(15, 17){$\rho, \nu$}
     \put(60, 15){$k$}
     \put(40, 3){\colorbox{white}{$L$}}
  \end{picture}
  \end{center}


\pause \medskip
Formellement, on s'attend à une dépendance de la forme :
$$
\Delta _1^2 = {\cal F}(U,L,D,k,\rho,\nu)
$$

\pause \medskip
 Par  {\color{purple} considération géométrique}, on peut justifier que $\Delta _1^2$ est proportionnel à $L$. On peut donc postuler une dépendance de la forme 
 
 $$
\frac{\Delta _1^2}{L} = {\cal F}(U,D,k,\rho,\nu)
$$


\pause \medskip

En appliquant les principes de l'analyse dimensionnelle {\color{green} (exercice)}, on peut montrer que 


\begin{equation}
	\color{rouge}
  \Delta _1^2 
             = \frac{1}{2} \, \rho \, U^2 \times \frac{L}{D} \times \lambda(Re,\varepsilon)
\end{equation}

où $\lambda(Re,\varepsilon)$ est le {\bf coefficient de pertes de charges} qui ne dépend que de 
deux paramètres : le nombre de Reynolds $Re = U D/\nu$ et la rugosit\'e relative de la paroi $\varepsilon = k/D$.



\vspace{0mm}

\end{frame}




%-----------------------------------------------------------------------------------------
\begin{frame}{Analyse dimensionnelle du problème (II)}
%-----------------------------------------------------------------------------------------

\small


\begin{center}
  \begin{picture}(85, 28)(0, 5)
     \put(0, 0){\includegraphics[width=8cm]{conduite.pdf}}
     \put(0, 20){$P_1$}
     \put(80, 20){$P_2$}
     \put(41, 20){$D$}
     \put(15, 23){$U$}
     \put(15, 17){$\rho, \nu$}
     \put(60, 15){$k$}
     \put(40, 3){\colorbox{white}{$L$}}
  \end{picture}
  \end{center}

Une autre quantité intéressante (mais moins utile en pratique) est la force exercée sur la paroi $\cal S$ de la conduite.


\begin{eqnarray*}
F_z = \left[ \iint_{\cal S}  [ p \vec{n} - \mytensor{\tau} \cdot  \vec{n} ] dS \right] \cdot \vec{e}_z  = \iint_{\cal S}  \tau_{zr}  dS         
\end{eqnarray*}

On montre (cf. exercice complémentaire 3.0) que cette force est reliée à la perte de charge par

$$
F_{z,{\cal F} \rightarrow {\cal S}}  = S \Delta_1^2 =   \frac{1}{2} \, \rho \, S \, U^2 \times \frac{L}{D} \times \lambda(Re,\varepsilon) 
$$


\vspace{0mm}

\end{frame}

\subsubsection{Régimes d'écoulement}

%-----------------------------------------------------------------------------------------
\begin{frame}{Observations expérimentales : régimes d'écoulements}
%-----------------------------------------------------------------------------------------

\small

%\vspace{-5mm}
%\hfill Nombre de Reynolds \quad $\color{rouge}\displaystyle Re = \frac{UD}{\nu} = \rho \frac{UD}{\mu}$

%\bigskip

Reynolds observe schématiquement trois régimes distincts :

\begin{picture}(0, 0)(-70, -2.8)
	\put(0, 0){\movie[width=10mm,poster,externalviewer,showcontrols=false]{\colorbox{bleu}{\color{white}visualisations}}{./Figures/Reynolds_experiment.mp4}}
\end{picture}

\pause

\begin{itemize}[<+-| alert@+>]
\item[]
\begin{picture}(100, 17)
	\put(-6, 12){$Re \mylesssim 2000$ : régime laminaire}  
	\put(65, 4.5){%
		\begin{minipage}{40mm} 
			écoulement stationnaire, \\ axisymétrique, établi, 1D \\ (écoulement de Poiseuille)
		\end{minipage}}
	\put(0, 0){\includegraphics[width=60mm, height=10mm]{Reynolds_laminar_colorized.jpg}}
\end{picture}
\item[]
\begin{picture}(100, 19)
	\put(-6, 12){$2000 \mylesssim Re \mylesssim 4000$ : régime de transition}  
	\put(65, 5){%
		\begin{minipage}{50mm} 
			écoulement instationnaire, \\ 3D par intermittence, \\ sinon relaminarisation
		\end{minipage}}
	\put(0, 0){\includegraphics[width=60mm, height=10mm]{Reynolds_transition_colorized.jpg}}
\end{picture}
\item[]
\begin{picture}(100, 19)
	\put(-6, 12){$4000 \mylesssim Re$ : régime turbulent}  
	\put(65, 4.5){%
		\begin{minipage}{50mm} 
			écoulement très instationnaire, \\ fortement 3D, "chaotique" \\ petits et gros tourbillons
		\end{minipage}}
	\put(0, 0){\includegraphics[width=60mm, height=10mm]{Reynolds_turbulent_colorized.jpg}}
\end{picture}
\end{itemize}

\vspace{0mm}

\end{frame}

%-----------------------------------------------------------------------------------------

%-----------------------------------------------------------------------------------------
\begin{frame}{Régime laminaire $Re \mylesssim 2000$}
%-----------------------------------------------------------------------------------------

\small

Pour $Re \mylesssim 2000$ et pour un tuyau peu ou modérément rugueux ($\epsilon < 0.01$),
Reynolds observe le régime dit \textcolor{vert}{laminaire}
pour lequel l'écoulement est 
stationnaire, axisymétrique, établi, 1D. 

\smallskip
L'expérience montre qu'il s'agit de l'écoulement de \textcolor{vert}{Poiseuille}, 
dont le champ de vitesse est donné par {\color{vert} (cf. exercice complémentaire 3.0)} :

\[
	u(r) =  u_{max} \left( 1 - \frac{r^2}{R^2} \right ) 
	  %   = 2 U \, \left( 1 - \frac{r^2}{R^2} \right )
\]
avec $u_{max} = 2 U$ et 

$$
U = - \frac{R^2}{8\mu} \frac{dp^*}{dx} = \frac{R^2}{8\mu} \left( \frac{(p_1-p_2) + \rho g (z_1-z_2)}{L} \right) = \frac{R^2}{8\mu} \left( \frac{\Delta^1_2}{L} \right)
$$

%$$
%\mbox{ ce qui s'écrit aussi } U = \frac{R^2}{8\mu}  \left( \frac{p_1-p_2}{L} + \rho g \sin \alpha \right) \quad \mbox{ où $\alpha$ est la pente du tuyau.}
%$$

\begin{center}
	\begin{picture}(95, 22)
		\put(0, 0){\includegraphics[width=50mm, height=20mm]{Reynolds_laminar_colorized.jpg}}
		\put(55, 0){\includegraphics[width=40mm]{poiseuille.png}}
		\put(86, 15){\color{bleu} $u(r)$}
		\put(76, 20){\color{bleu} $U$}
	\end{picture}
\end{center}

\pause

On en déduit que le coefficient de perte de charge $  \lambda = \frac{\Delta _1^2}{ \frac{1}{2} \, \rho \, U^2 \times \frac{L}{D} }$ vaut:

$$
\lambda(Re,\epsilon) = \frac{64}{Re}
$$


%\pause


%Remarque :



%Le profil de vitesse parabolique correspondant à l'écoulement de Poiseuille est \textcolor{vert}{une solution exacte} de
%l'équation de Navier--Stokes quelle que soit la valeur du nombre de Reynolds.

%\medskip

%En général, cet écoulement se déstabilise pour $Re \sim 2000$, 
%au delà apparaît \textcolor{vert}{une autre solution}
%de l'équation de Navier--Stokes, plus compliquée, dont on ne connaît pas l'expression mathématique\ldots

%\medskip

%Cependant, dans des conditions très "propres" (perturbations en entrée très faibles, et tube est très lisse), la solution de Poiseuille peut être observée jusqu'à des nombres de Reynolds de l'ordre de $10^5$ !




\vspace{0mm}

\end{frame}

%-----------------------------------------------------------------------------------------
%\subsubsection{Régime de transition}
%-----------------------------------------------------------------------------------------
\begin{frame}{Régime de transition $2000 \mylesssim Re \mylesssim 4000$}
%-----------------------------------------------------------------------------------------

\small

Pour les nombres de Reynolds intermédiaires, compris entre 2000 et 4000 environ (dans des conditions ordinaires), 
on observe un régime dit \textcolor{vert}{transitoire} ou de transition : "bouffées" de turbulence (écoulement instationnaire et 3D), alternant avec des phases de relaminarisation.


\begin{center}
	\begin{picture}(60, 12)
		\put(0, 0){\includegraphics[width=60mm, height=10mm]{Reynolds_transition_colorized.jpg}}
	\end{picture}
\end{center}

\pause
\bigskip

Dans ce régime la perte de charge est caractérisée par une forte {\em intermittence}, c'est-à-dire que sa valeur est difficilement prédictible et varie fortement au cours du temps.

Dans une même expérience, $\Delta_1^2$ peut varier d'un facteur 2 entre deux instants différents !

$$\lambda(Re,\epsilon) \in [ 0.02, 0.04].$$


\pause
\bigskip

Ce régime est le plus complexe à décrire et à comprendre : il fait l'objet
de nombreuses recherches actuelles, aussi bien par le biais de \textcolor{vert}{campagnes expérimentales}
que de \textcolor{vert}{simulations numériques} et d'\textcolor{vert}{analyses théoriques}. 

\medskip


%Ainsi, si le niveau des perturbations en entrée est très faible, et si le tube est très lisse, cet écoulement peut être observé
%jusqu'à des nombres de Reynolds de l'ordre de $10^5$ !


%Les concepts adaptés à l'étude de ce régime de transition proviennent de
%\begin{itemize}
%	\item[\checkmark] la théorie des instabilités hydrodynamiques
%	\item[\checkmark] la théorie des systèmes dynamiques non linéaires
%	\item[\checkmark] la théorie des bifurcations
%	\item[\checkmark] la théorie du contrôle
%	\item[\checkmark] [\mbox{\ldots}]
%\end{itemize}

\vspace{5mm}

\end{frame}

%-----------------------------------------------------------------------------------------
%\subsubsection{Régime turbulent}
%-----------------------------------------------------------------------------------------
\begin{frame}{Régime turbulent $4000 \mylesssim Re$}
%-----------------------------------------------------------------------------------------

\small

Pour les grands nombres de Reynolds, l'écoulement devient \textcolor{vert}{turbulent}
et rentre dans un régime fortement instationnaire, complètement 3D, présentant une structure
spatiale très compliquée \\
avec petits et grands tourbillons, et un comportement temporel "chaotique".

\begin{center}
	\begin{picture}(60, 12)
		\put(0, 0){\includegraphics[width=60mm, height=10mm]{Reynolds_turbulent_colorized.jpg}}
	\end{picture}
\end{center}

Paradoxalement, ce régime n'est pas le plus compliqué à modéliser 
si l'on accepte  de se restreindre
aux \textcolor{rouge}{grandeurs moyennes} de l'écoulement
(\textcolor{vert}{approche statistique}).

\pause

\begin{center}
	\begin{picture}(0, 0)(10, -17)
		\put(0, 0){\includegraphics[width=6mm, height=10mm]{testo_405_alpha.png}}
		\put(7, 8){\footnotesize \slshape \color{rouge} sonde de vitesse}
	\end{picture}
\end{center}

\vspace{-3mm}

\color{rouge}{Mesure de vitesse en un point de l'écoulement :}

\begin{overprint}

\onslide<3|handout:0>

	\begin{picture}(40, 30)(-57, -7)
		\put(0, 0){\includegraphics[width=40mm]{plot_laminar.pdf}}
		\put(41, 3){\color{rouge} laminaire}
		\put(20, -3){\color{black} $t$ (s)}
		\put(-6, 15){\color{black} $u(t)$}
		\put(-7, 12){\color{black} (m/s)}
	\end{picture}

\onslide<4|handout:0>

	\begin{picture}(40, 30)(-57, -7)
		\put(0, 0){\includegraphics[width=40mm]{plot_transition.pdf}}
		\put(41, 3){\color{bleu} laminaire}
		\put(41, 8){\color{rouge} transition}
		\put(20, -3){\color{black} $t$ (s)}
		\put(-6, 15){\color{black} $u(t)$}
		\put(-7, 12){\color{black} (m/s)}
	\end{picture}

\onslide<5|handout:0>

	\begin{picture}(40, 30)(-57, -7)
		\put(0, 0){\includegraphics[width=40mm]{plot_turbulent.pdf}}
		\put(41, 3){\color{bleu} laminaire}
		\put(41, 8){\color{bleu} transition}
		\put(41, 18){\color{rouge} turbulent}
		\put(20, -3){\color{black} $t$ (s)}
		\put(-6, 15){\color{black} $u(t)$}
		\put(-7, 12){\color{black} (m/s)}
	\end{picture}

\onslide<6|handout:0>

	\begin{picture}(40, 30)(-57, -7)
		\put(0, 0){\includegraphics[width=40mm]{plot_turbulent_mean.pdf}}
		\put(41, 3){\color{bleu} laminaire}
		\put(41, 8){\color{bleu} transition}
		\put(41, 18){\color{bleu} turbulent}
		\put(20, -3){\color{black} $t$ (s)}
		\put(-6, 15){\color{black} $u(t)$}
		\put(-7, 12){\color{black} (m/s)}
		\put(0, 18){\color{rouge} \vector(-1, 0){10}}
		\put(-55, 17.5){\color{rouge} vitesse moyenne locale $\bar{u}= \frac{1}{T} \int_0^T u \, dt$}
	\end{picture}

\onslide<7|handout:1>

	\begin{picture}(40, 30)(-57, -7)
		\put(0, 0){\includegraphics[width=40mm]{plot_turbulent_mean.pdf}}
		\put(41, 3){\color{bleu} laminaire}
		\put(41, 8){\color{bleu} transition}
		\put(41, 18){\color{bleu} turbulent}
		\put(20, -3){\color{black} $t$ (s)}
		\put(-6, 15){\color{black} $u(t)$}
		\put(-7, 12){\color{black} (m/s)}
		\put(0, 18){\color{rouge} \vector(-1, 0){10}}
		\put(-55, 17.5){\color{bleu} vitesse moyenne locale $\bar{u} = \frac{1}{T} \int_0^T u \, dt$}
	\end{picture}

	\begin{picture}(0, 0)(-7, -7)
		\put(0, 0){\includegraphics[width=35mm]{profil_turbulent_moyen.png}}
		\put(-2, -4){\color{bleu} Profil de vitesse moyenne (quasi uniforme)}
	\end{picture}



\end{overprint}
%
%Pour $Re >4000$ et $\epsilon$ quelconque, on utilise généralement la formule de 
%\textcolor{vert}{Colebrook}  :
%%\begin{equation}
%%	\color{rouge}
% % \frac{1}{\sqrt{\lambda}} = -2 \log_{10} \left (
%%  \frac{2.51}{Re\sqrt{\lambda}} + \frac{\varepsilon}{3.71}
% % \right)
%%\end{equation}
%
%\begin{equation}
%{\color{rouge}
%\lambda = \frac{1}{\left(-2 \log_{10} \left[  \frac{2.51}{Re\sqrt{\lambda}} + \frac{\varepsilon}{3.71} \right] \right)^2}
%}
%\qquad \qquad 
%	{ \color{gray} \left( \mbox{ ou bien }
%  \frac{1}{\sqrt{\lambda}} = -2 \log_{10} \left (
%  \frac{2.51}{Re\sqrt{\lambda}} + \frac{\varepsilon}{3.71}
%  \right) \quad \right) } 
%\end{equation}

\vspace{0mm}

\end{frame}




%-----------------------------------------------------------------------------------------
\begin{frame}{Coefficient de pertes de charge dans le cas turbulent}
%-----------------------------------------------------------------------------------------

%$$
%\Delta _1^2  = \frac{1}{2} \, \rho \, U^2 \frac{L}{D} \; \lambda(\varepsilon, Re)
%$$
             

\small

Dans le cas turbulent, il n'existe pas de formule du coefficient de perte de charge provenant d'une solution exacte.

En revanche, il existe des formules empiriques obtenues par correlations à partir de mesures expérimentales.
\pause
\begin{itemize}

\item Cas turbulent, régime "hydrodynamiquement lisse" :

Pour $\varepsilon < 0.01\%$ et $Re<10^5$ on peut utiliser  la formule de Blasius
\begin{equation}
  \lambda= 0.316 \, Re^{-1/4}
   \end{equation}

\smallskip 
\pause

\item Cas turbulent "régime très rugueux" :

Si  $\varepsilon > 1\%$ et $Re>10^5$, on observe que la perte de charge est indépendante de Re et donné par la formule approchée (Karman--Nikuradse)
%\begin{equation}
%   \frac{1}{\sqrt{\lambda}} = -2 \log_{10} \left( \frac{\varepsilon}{3.71} \right)
%\end{equation}
\begin{equation}
   \lambda = \frac{1}{\left(-2 \log_{10} \left[ \frac{\varepsilon}{3.71} \right] \right)^2}
   \qquad \qquad {\color{gray} \left( \mbox{ ou bien }  \frac{1}{\sqrt{\lambda}} = -2 \log_{10} \left( \frac{\varepsilon}{3.71} \right) \quad \right) }
\end{equation}


%\pause 

%\tiny
%\smallskip 
\pause


%\smallskip 
%\pause

\item Cas turbulent, cas général

Pour $Re >4000$ et $\epsilon$ quelconque, on utilise généralement la formule de 
\textcolor{vert}{Colebrook}  :
%\begin{equation}
%	\color{rouge}
 % \frac{1}{\sqrt{\lambda}} = -2 \log_{10} \left (
%  \frac{2.51}{Re\sqrt{\lambda}} + \frac{\varepsilon}{3.71}
 % \right)
%\end{equation}

\begin{equation}
{\color{rouge}
\lambda = \frac{1}{\left(-2 \log_{10} \left[  \frac{2.51}{Re\sqrt{\lambda}} + \frac{\varepsilon}{3.71} \right] \right)^2}
}
\qquad \qquad 
	{ \color{gray} \left( \mbox{ ou bien }
  \frac{1}{\sqrt{\lambda}} = -2 \log_{10} \left (
  \frac{2.51}{Re\sqrt{\lambda}} + \frac{\varepsilon}{3.71}
  \right) \quad \right) } 
\end{equation}

(Cette relation est implicite, mais peut être facilement résolue de manière itérative.)

\end{itemize}

%et dans le cas des \'ecoulements dits \textcolor{vert}{hydrauliquement lisses} pour $\varepsilon < 0.01\%$ 
%et $Re<100000$ pour donner la formule de Blasius
%\begin{equation}
  % \lambda= 0.316 \, Re^{-1/4}
%\end{equation}



\vspace{0mm}

\end{frame}

\subsubsection{Diagramme de Moody}

%-----------------------------------------------------------------------------------------
\begin{frame}{Diagramme de Moody}
%-----------------------------------------------------------------------------------------


\small


Les différentes lois pour le coefficient de perte de charge sont tabulées dans le diagramme de Moody :

\smallskip \pause


\begin{center}
  \begin{picture}(65, 50)(0, 4)
     \put(0, 2){\includegraphics[width=65mm]{Moody_diagram.pdf}}
		\setlength{\fboxsep}{2mm}
     \put(58.5, 49.5){\colorbox{white}{$\varepsilon \quad$}}
		\setlength{\fboxsep}{1mm}
  \end{picture}
\end{center}

%\noindent
%Le coefficient de pertes de charge r\'eguli\`eres $\lambda$ est donn\'e en fonction du nombre de Reynolds $Re$ 
%pour diff\'erentes rugosit\'es relatives $\varepsilon$ dans le \textcolor{vert}{diagramme de Moody}. %qui est obtenu par 
%r\'esolution num\'erique de la formule implicite de Colebrook.


\vspace{5mm}

\end{frame}

\begin{frame}{Remarques complémentaires}

\small 
\begin{enumerate}

\item Dans des situations particulièrement contrôlées (tuyau parfaitement lisse, aucune perturbation dans l'environnement),
la solution laminaire peut être observée jusque $Re = 10^6$ ou au delà !

\smallskip
Pour $Re>2000$ La solution de Poiseuille cylindrique est donc \textcolor{red}{métastable}...

\pause

\item Le fait que dans le régime turbulent rugueux $\lambda$ ne dépende plus de $Re$ (et donc de $\mu$) peut sembler paradoxal...

En effet $F_z$ est obtenue en intégrant la contrainte visqueuse pariétale sur la surface, et $\mu$ apparaît explicitement dans la formule  

\begin{eqnarray*}
F_z  =  \iint_{\cal S}  \tau_{zr}  dS  \approx 2 \pi R L \times \tau_{zr}(R)   \approx 2 \pi R L \times  \mu \left[ \frac{\partial u}{\partial r}\right]_{r=R} \quad \Longrightarrow  F_z \mbox{ proportionnel à $\mu$}  
\end{eqnarray*}

$$ \mbox{ or } F_z = \frac{\rho S U^2}{2} \times \frac{L}{D} \times \lambda(\epsilon) 
\quad   \Longrightarrow  F_z  \mbox{ indépendant de $\mu$ !} 
$$

\pause

Explication : on peut écrire $\left[ \frac{\partial u}{\partial r}\right]_{r=R} \approx \frac{U}{\delta}$ où $\delta$ est l'épaisseur de la couche limite.

Le fait que la force soit indépendante de $\mu$ est possible si $\delta$  est proportionnel $\mu$. 

La couche limite devient donc extrêmement fine lorsque $Re$ est grand ($\delta \approx Re^{-1}$).

\end{enumerate}

\end{frame}



%-----------------------------------------------------------------------------------------
\subsubsection{Pertes de charge singulières}
%-----------------------------------------------------------------------------------------
\begin{frame}{Pertes de charge singulières}
%-----------------------------------------------------------------------------------------

\small

On appelle \textcolor{vert}{pertes de charges singuli\`eres} 
la diminution d'\'energie m\'ecanique entre deux sections d'une conduite de part et d'autre d'une \textcolor{vert}{singularit\'e}, c'est-\`a-dire d'une variation \textsl{brusque} de la g\'eom\'etrie de la conduite~:
r\'etr\'ecissement ou \'elargissement brusque, changement de direction (coude) 
ou pr\'esence d'un obstacle.
En effet cette variation brusque des conditions d'\'ecoulement s'accompagne g\'en\'eralement d'une augmentation du brassage du fluide \`a grand nombre de Reynolds et donc des frottements visqueux.


\begin{center}
  \begin{picture}(100, 42)(5, 0)
     \put(0, 0){\includegraphics[width=55mm]{pertes_de_charge_singulieres1_naked.png}}
     \put(56, 0.5){\includegraphics[width=55mm]{pertes_de_charge_singulieres2_naked.png}}
  \end{picture}
\end{center}


\vspace{0mm}

\end{frame}

%-----------------------------------------------------------------------------------------
\begin{frame}{Pertes de charge singulières}
%-----------------------------------------------------------------------------------------

\small

Une estimation par analyse dimensionnelle montre que les pertes de charge singuli\`eres 
s'\'ecrivent
\begin{equation}
	\color{rouge}
  \Delta _1^2 = \frac{1}{2} \, \rho \, U^2 \; K(Re ; {\mbox{géométrie}} ) 
\end{equation}
o\`u $U$ est la vitesse moyenne en amont de la singularit\'e (ou en aval pour un rétrécissement ou une entrée de canalisation) et $K$ d\'esigne le \textcolor{vert}{coefficient de pertes de charge singuli\`eres}. 

On observe que pour $Re\gtrsim10^4$ (c.a.d. lorsque l'écoulement est pleinement turbulent) ce coefficient \underline{ne dépend pas de $Re$} mais seulement de la géométrie de la jonction.

Les valeurs de $K$ sont donn\'ees par des formules empiriques ou semi-empiriques en fonction du type de singularit\'e concern\'ee. 

\vspace{-8mm}

\begin{center}
  \begin{picture}(100, 42)(5, 0)
     \put(0, 0){\includegraphics[width=55mm]{pertes_de_charge_singulieres1_naked.png}}
     \put(56, 0.5){\includegraphics[width=55mm]{pertes_de_charge_singulieres2_naked.png}}
     \put(1, 21){\scriptsize \textcolor{vert}{Démo Exo 8.0}} 
     \put(1, 19.5){\scriptsize $K=(1-S_1/S_2)^2$ }
     \put(15, 28){\scriptsize $S_1$}
     \put(23, 29.1){\scriptsize $S_2$}
     \put(1, 2){\scriptsize $K=(S_2/S_c-1)^2$}
     \put(21, 11){\scriptsize $S_c$}
     \put(23.5, 12){\scriptsize $S_2$}

     \put(29, 29.5){\scriptsize $K=(1-S_1/S_2)^2 \sin \alpha$}
     \put(35, 19.5){\scriptsize $S_1$}
     \put(43.5, 19.5){\scriptsize $S_2$}
     \put(52.5, 19.5){\scriptsize $\alpha$}
     \put(29, 12){\scriptsize $K=(S_2/S_c-1)^2 \sin \alpha$}
     \put(49, 9){\scriptsize $S_c$}
     \put(51.5, 10){\scriptsize $S_2$}
     \put(36, 2){\scriptsize $\alpha$}

     \put(57, 29.5){\scriptsize $K=\sin^2 \alpha + 2\sin^4 \alpha/2$}
     \put(80, 23){\scriptsize $\alpha$}
     \put(57, 12){\scriptsize $K = 0.5$}
     
     \put(85, 23){\scriptsize $K= \dfrac{\alpha}{\pi} \big [0.131 $}
     \put(85, 19.2){\scriptsize $+ 1.847 (D/R)^{7/2}\big]$}
     \put(90, 28){\scriptsize $D$}
     \put(99, 25){\scriptsize $R$}
     \put(108, 26){\scriptsize $\alpha$}
     \put(85, 12){\scriptsize $K = 0.04$}
     
  \end{picture}
\end{center}

Quelques éléments de plomberie standard :

Raccord rectiligne $K = 0.04$ ;  Valve "globe" ouverte $K = 6.4$ ; valve demi-fermée $K = 9.5$ ; 

Coude $90^o$ standard $K= 0.75$ ; coude à grand rayon $K = 0.45$ ;

Raccord en $T$ $K = 0.4$ (ligne$\rightarrow$ligne) / $K=1.3$ 
(ligne$\rightarrow$branche) / $K=1.5$ (branche$\rightarrow$ligne)....

\vspace{0mm}

\end{frame}

\subsubsection{Application : pompe d'un chauffe-eau solaire }
\begin{frame}{Exercice de cours : dimensionnement d'une pompe pour un chauffe-eau solaire}
\small

On souhaite dimensionner une pompe pour faire fonctionner un chauffe-eau solaire.
\medskip 

Le circuit (simplifié) est constitué d'un tuyau de cuivre de diamètre intérieur $d = 8mm$, rugosité $k = 1.5\mu m$,
longueur totale $L = 60m$, comportant 20 coudes $90^o$ standard et 2 vannes globe ouvertes.

Le fluide est un mélange eau-glycérol (viscosité $\nu = 2 \cdot 10^{-6} m^2/s$, de masse volumique $980 kg/m^3$). 

Le débit attendu est $Q = 250 \ell / h$.

\medskip 

Calculez la perte de charge totale, et en déduite la puissance minimale de la pompe nécessaire pour faire fonctionner le circuit.

% U = 1.38 m/s

% Re = 5526

% Delta = 1.11e5

% Puiss =   15.7136


\bigskip

{\em Correction : programme Matlab disponible sur Moodle}



\end{frame}

\subsection{Ecoulement le long d'une plaque plane}

\subsubsection{Définitions et analyse dimensionnelle}


\begin{frame}{Frottement sur une plaque plane : définition et analyse dimensionnelle}

\small

Considérons une plaque plane rectangulaire de longueur $L$, largeur $b$, et rugosité $k$, sous un écoulement incident de vitesse $U$ (tangent à la plaque).

Intéressons nous à la {\bf Force exercée sur la plaque}  $F_v$


\begin{center}
\includegraphics[width=6cm]{PlaquePlane.png}
\end{center}

En appliquant les méthodes de l'analyse dimensionnelle et en tenant compte du fait que $F_v$ est proportionnel à 
$b$ {\color{purple} (considération géométrique) }, on montre que :


$$
F_v = \frac{\rho S U^2}{2} C_f(Re,\epsilon)
$$

où $Re = \frac{U L}{\nu}$, $\epsilon = \frac{k}{L}$, $S = Lb$,

et $C_f$ est le coefficient de frottement (sans dimensions).

\end{frame}

\subsubsection{Régimes d'écoulement}


\begin{frame}{Cas laminaire}

\small

Pour $Re \leq 2\cdot 10^5$ et $\epsilon < 10^{-5}$, la solution observée est une couche limite laminaire (c.a.d. dans laquelle le champ de vitesse est stationnaire).

\begin{center}
\includegraphics[width=60mm]{CL_Laminaire.png}	
\end{center}

\pause
L'écoulement est la solution de Blasius (vue au chapitre 5).

On a notamment montré que la force de frottement vaut :

\medskip

$$
F_v = 0.664 \rho b U^{3/2} \nu^{1/2} L^{1/2}    
$$

Ce qui s'écrit également :

$$
F_v  = \frac{\rho S U^2}{2} C_f(Re) \mbox{ avec } C_f(Re) = 1.328 Re^{-1/2} 
$$ 

\small


\end{frame}


\begin{frame}{Cas transitionnel}
\small

Pour $10^5 < Re < 10^6$, on observe un écoulement transitionnel.

\begin{center}
\includegraphics[width=6cm]{Couche_Limite_Transition.png}
\end{center}

 \pause
  \medskip

Cet écoulement, particulièrement difficile à expliquer et à prédire, est caractérisé dans un premier temps par 
des oscillations appelées {\em ondes de Tollmien-Schlishting}, qui se déstabilisent ensuite pour conduire à un
 état désordonné.
 
 \pause
  \medskip
 Dans ce régime, comme dans le cas de l'écoulement en conduite, le coefficient de frottement est très intermittent
 et peut varier d'un facteur $2$ au cours d'une même expérience. 
 
  
 
 

\end{frame}

\begin{frame}{Cas turbulent}
\small

Lorsque $Re \gg 10^7$ (dans le cas très lisse), ou $Re > 10^5$ (dans le cas rugueux), on observe une couche limite 
pleinement turbulente (sauf au voisinage immédiat du bord d'attaque où la couche limite est tout d'abord laminaire).

\begin{center}
\includegraphics[width=6cm]{CL_Transition.png}
\end{center}

Cet état est très désordonné et caractérisé par la présence de tourbillons de diverses échelles.

\pause \smallskip


\begin{center}
\includegraphics[width=4cm]{CFD_TurbulentBoundaryLayer.jpg}

{\color{bleu} Exemple de simulation numérique d'une couche limite turbulente 

(Universität Stuttgart) } 

\end{center}

\pause \smallskip

Cependant si l'on s'intéresse à des {\em quantités moyennes}, les propriétés  de l'écoulement sont très reproductibles.





\end{frame}

\begin{frame}{Cas turbulent : coefficient de frottement}
\small

Diverses corrélations expérimentales existent pour exprimer le coefficient de frottement dans le cas turbulent.
Citons les suivantes :
\pause
\begin{itemize}
\item Dans le cas très lisse, plusieurs corrélations sont utilisées, par exemple :

\smallskip
$Re = e^{\left[ \frac{0.5572}{\sqrt{C_f}} - \ln (C_f) \right]}$ (loi de Schoenherr, définie par sa fonction réciproque). 

\smallskip
$C_f = 0.030 Re^{-1/7} $ (Loi de puissance $1/7$, utilisée en aéronautique).

\smallskip
$C_f = \frac{0.067}{\left( \log_{10} (Re) -2\right)^2} $ (Loi de Hugues, utilisée en architecture navale).


\smallskip
Remarque : bien que très différentes, ces différentes expressions donnent des résultats identiques à environ $1\%$ près, 
dans la gamme $ 10^6 < Re < 10^9$.
\pause
\smallskip
\item Dans le cas rugueux :

\smallskip
$C_f = 0.032 \epsilon^{1/5}$ (Loi de McNerney).
\end{itemize}


\pause \smallskip

{\bf Remarque :} 

Dans le cas d'un objet élancé de forme plus complexe qu'une plaque plane (aile d'avion, carène de bateau,...) 
diverses méthodes existent pour étendre la validité de ces formules.

Par exemple dans le domaine de l'architecture navale on utilise la loi de Hugues modifiée :

$C_f = \frac{0.067 ( 1 +\kappa) }{\left( \log_{10} (Re) -2\right)^2} $ 

où $\kappa$ est un facteur de forme dépendant de la géométrie (cf. TDs 1.4 et 6.4).





\end{frame}




\subsubsection{Coefficient de frottement}

\begin{frame}{Couche limite sur une plaque plane}
\small

Les différentes corrélations sont rassemblées dans le diagramme suivant, qui est l'équivalent du diagramme de Moody {\color{blue} (cf. Formulaire, section H.1).} :

\begin{center}
\includegraphics[width=6cm]{diagramme_Cf.png}
\end{center}
\end{frame}




\comment{
\begin{frame}{Applications}
\small

Applications :

\medskip 
Exemple 1 : dimensionnement d'un chauffe-eau solaire.

\medskip
Exemple 2 : canalisation en pente.


\end{frame}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\comment{
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%==========================================================================================
\subsection*{Compléments}
%==========================================================================================

%------------------------------------------------------------------------------------------
\begin{frame}{toto} \hypertarget{frame:toto}{}
%------------------------------------------------------------------------------------------

\small

\vspace{0mm}

\end{frame}
}

