close all
clear all

Q=0.02;
W=1;
H=0.1;
Udeb=Q/(H*W);
Fr=Udeb/sqrt(9.81*H)
D=2*H;
Re=Udeb*D/1e-6;
Lambda=0.316/Re^0.25;   % Blasius
DeltaP=1000*Udeb^2/D*Lambda;    % Chute de pression sur 1 mètre
DeltaH=DeltaP/(1000*9.81)       % Chute de hauteur sur 1 mètre

h=[0.004:0.001:0.15];
F=9.81*h+0.5*Q^2./h.^2;
h1=0.1;
F1=9.81*h1+0.5*Q^2./h1.^2;
h2=0.0153;
F2=9.81*h2+0.5*Q^2./h2.^2;

figure;
plot(h,F,'k','LineWidth',2);
FH=9.81*H+0.5*Q^2/H^2;
hold on
plot([0 0.2],[FH FH],'k');
plot([0.1 0.1],[0 5],'k');
plot([0.1 0.1],[FH FH],'ok');
bmax=0.02;
Fbmax=9.81*(H-bmax)+0.5*Q^2/H^2;
plot([0 0.2],[Fbmax Fbmax],':k');
bmax=0.04;
Fbmax=9.81*(H-bmax)+0.5*Q^2/H^2;
plot([0 0.2],[Fbmax Fbmax],'--k');
axis([0 0.15 0 2])
%title(['Profil de F(h) pour Q=',num2str(Q),' $m^3/s$'],'Interpreter','latex');
ylabel ('$F$ (en $m^2s^{-2}$)','Interpreter','latex');
xlabel ('$h$ (en $m$)','Interpreter','latex');   

G=Q^2./h+0.5*9.81*h.^2/2;
G2=Q^2./h2+0.5*9.81*h2.^2/2;
h1bis=0.0958;
G1=Q^2./h1bis+0.5*9.81*h1bis.^2/2;
F1bis=9.81*h1bis+0.5*Q^2./h1bis.^2;
figure;
plot(h,G,'k','LineWidth',2);
hold on
 plot([0 0.2],[G2 G2],'k');
 plot([h2 h2],[0 1],'k');
 plot([0.1 0.1],[0 1],'k:');

axis([0 0.12 0.01 0.04])
ylabel ('$G$ (en $m^2$)','Interpreter','latex');
xlabel ('$h$ (en $m$)','Interpreter','latex');   




