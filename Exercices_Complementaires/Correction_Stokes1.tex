%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[10pt, a4paper]{article}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%--------------------------------------------------------------------------------------------------
% Dimensions :
%--------------------------------------------------------------------------------------------------

\setlength{\textheight}{25.5cm}
\setlength{\textwidth}{16cm}

\setlength{\topmargin}{-22mm}
\setlength{\oddsidemargin}{0mm}
\setlength{\evensidemargin}{0mm}

% \setlength{\columnsep}{20mm}

\setlength{\fboxsep}{1mm}
\setlength{\unitlength}{1mm}

%--------------------------------------------------------------------------------------------------
% Packages :
%--------------------------------------------------------------------------------------------------

\usepackage{latexsym}
\usepackage{graphicx}
\usepackage{pifont}
\usepackage{color}
\usepackage{amsmath}
\usepackage{amssymb}

\usepackage[french]{babel}    % pour franciser le document

%\usepackage[latin1]{inputenc} % pour utiliser les caracteres accentues du claviers
\usepackage[utf8]{inputenc}

%--------------------------------------------------------------------------------------------------
% Divers :
%--------------------------------------------------------------------------------------------------

\definecolor{rougefonce}{rgb}{0.7, 0.2, 0.2}

\usepackage{accents}

\input{mymacros}

\renewcommand{\thickline}[2]{\linethickness{#1} \line(1, 0){#2}}
\renewcommand{\mycaption}[1]{\caption{\sl #1}}
\renewcommand{\myvec}[1]{\vec{#1}}

%\pagestyle{empty}

\graphicspath{{Figures/}} % chemin d'acces au repertoire des figures (par ex.)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{center}

  \textsc{Université Toulouse 3 -- Paul Sabatier \hfill Année universitaire 2022-2023}
  
  \textsc{Mécanique des fluides \hfill L3 Mécanique}
  
  \vspace{0mm}
  
  \begin{center}
    \thickline{0.4mm}{160}
    \\ \vspace{3mm}
  \textbf{\large Exercice complémentaire 4.0 : premier problème de Stokes (correction)}
    \\ %\vspace{1mm}
    \thickline{0.4mm}{160}
  \end{center}

%  \vspace{0mm}
  
\end{center}

\stepcounter{section}

\medskip

%==================================================================================================
\section{Méthode}
%==================================================================================================

\begin{enumerate}


\item Il semble intuitif que la vitesse $v(x,t)$ soit proportionnelle à la vitesse $U_1$ de la plaque : si la plaque va deux fois plus vite on s'attend à ce que l'écoulement induit soit également deux fois plus rapide. Attention cependant car l'intuition n'est pas toujours un guide fiable en physique ! La véritable justification de cette propriété vient du fait que le problème mathématique est {\em linéaire}, ce qui implique que les conséquences (l'écoulement induit) sont proportionnelles aux causes (le mouvement de la plaque). 


\item 
En injectant la forme de la solution $v(x,t) = U_1  v^*(x,t)$ dans le problème de départ et en simplifiant par $U_1$ on trouve que   la fonction $v^*$ est la solution du problème suivant :
\begin{equation}
	\dpdt{v^*} = \nu \, \ddpdx{v^*}
	\label{eq:diffusion*}
\end{equation}
Avec les conditions limite et condition initiale : 
\begin{equation}
\mbox{ CL : } \quad v^*(x=0, t) = 1 \mbox{ pour } t > 0, 
\end{equation}
\begin{equation}
\mbox{ CI : } \quad v^*(x, t=0) = 0 \mbox{ pour } x > 0.
\end{equation}

On applique la première étape de la méthode d'analyse dimensionnelle du chapitre 1, {\em listez les paramètres pertinents et indépendants}. Le problème étant formulé sous forme mathématique cette démarche consiste à lister toutes les quantités mathématiques intervenant dans le problème. On arrive effectivement à $v^*(x,t) = {\cal F } ( x,t, \nu)$.

\item On va maintenant appliquer la méthode des unités intrinsèques. Pour ceci on va mettre le problème sous la forme
$$
\frac{v^*}{1} = \bar{\cal F } \left( \frac{x}{L^*},\frac{t}{T^*}, \frac{\nu}{L^* T*^{-2}} \right). 
$$
Remarquez ici que la fonction $v^*$ étant sans dimensions, son unité physique est 1.
On va ensuite choisir les unités intrinsèques en faisant le choix le plus évident : $L^* = x$, $T^* = t$.
Ceci conduit à :
$$
v^* = \bar{\cal F } \left( 1,1, \frac{\nu t}{x^2} \right). 
$$
En posant $\eta = \frac{x}{\sqrt{\nu t}}$,  et en reconnaissant que 
$ \bar{\cal F } \left( 1,1, \frac{\nu t}{x^2} \right) = \bar{\cal F } \left( 1,1, \eta^{-1/2} \right) = f(\eta)$ on arrive bien à $v^*(x,t) = f(\eta)$
et donc à la forme demandée $v(x,t) = U_1 f(\eta)$.

\,


{\em {\bf Remarque :} on aurait pu arriver à cette forme en appliquant directement la méthode du chapitre 1 à la fonction 
$v(x,t)$, ce qui aurait conduit, en listant les paramètres intervenant dans le problème vérifié par cette fonction, 
à $v(x,t) = {\cal F } ( x,t, \nu, U_1)$.
La méthode des unités intrinsèques aurait alors conduit à :
$$
\frac{v}{L^* T^{*-1} } = \bar{\cal F } \left( \frac{x}{L^*},\frac{t}{T^*}, \frac{\nu}{L^* T^{*-2}}, \frac{U_1}{L^* T^{*-1}} \right),
$$
et ensuite 
$$
 \frac{t}{x} v(x,t) = \bar{\cal F } \left( 1,1, \frac{\nu t}{x^2}, \frac{U_1 t}{x} \right) = f_1(\eta,\xi) \mbox{ avec } \xi = \frac{U_1 t}{x} .
$$
On remarque ensuite que la solution $u(x,t)$ est forcément proportionnelle à $U_1$ (simplification issue de la structure des équations), ce qui implique que la fonction $f(\eta,\xi)$ est proportionnelle à $\xi$ : $f_1(\eta,\xi) = \xi f(\eta)$. On retombe alors sur le même résultat; en effet $u(x,t) = \frac{x}{t} \xi f(\eta) = \frac{x}{t}  \times \frac{U_1 t}{x} \times f(\eta) = U_1 f(\eta) $.
}



%\item
%	Le changement de variable conduit à \dotfill
%	$	
%		\dpdt{v} = \nu \, \ddpdx{v} 
%		\Leftrightarrow 
%		\dfrac{A}{C}\dfrac{\partial v^*}{\partial t^*} = \dfrac{AD}{B^2} \nu^* \, \dfrac{\partial^2v^*}{\partial {x^*}^2}	\quad (a)
%	$
%	
%	\smallskip
%	Or si $\{v^*, x^*, t^*, \nu^*, U_1^*\}$ est solution : \dotfill 
%	$		
%		\dfrac{\partial v^*}{\partial t^*} = \nu^* \, \dfrac{\partial^2v^*}{\partial {x^*}^2} \quad (b)
%	$
%	
%	\smallskip
%	On déduit donc des deux relations $(a)$ et $(b)$ que \dotfill
%	$\dfrac{A}{C} = \dfrac{AD}{B^2}$ soit \fbox{$B = \sqrt{CD}$}
%
%	\smallskip
%	La condition limite en $x=0$ donne : \dotfill
%	$v(0, t) = U_1 \Leftrightarrow A v^*(0, t^*) = E U_1^* \quad (c)$
%	
%	\smallskip
%	Or si $\{v^*, x^*, t^*, \nu^*, U_1^*\}$ est solution : \dotfill 
%	$		
%		v^*(0, t^*) = U_1^* \quad (d)
%	$
%
%	\smallskip
%	On déduit donc des deux relations $(c)$ et $(d)$ que \dotfill
%	\fbox{$A = E$}
%	
%	\smallskip
%	La condition initiale à $t=0$ donne \dotfill
%	$v(x>0, t=0) = 0 \Leftrightarrow A v^*(x^*>0, t^*=0) = 0 \quad (g)$
%	
%	\smallskip
%	Or si $\{v^*, x^*, t^*, \nu^*, U_1^*\}$ est solution : \dotfill 
%	$		
%		v^*(x^*>0, t^*=0) = 0 \quad (h)
%	$
%
%	\smallskip
%	La condition initiale n'impose donc aucune relation supplémentaire.
%	
%\item
%	D'après le théorème mathématique sur les groupes d'invariance,
%	le quintuplet $\{Av, Bx, Ct, D\nu, EU_1\}$ 
%	
%	\smallskip
%	avec $B=\sqrt{CD}$ et $A=E$ est aussi solution, 
%	c'est-à-dire \dotfill $\mathcal{F}(Av, Bx, Ct, D\nu, EU_1)=0 \quad (i)$
%	
%	\smallskip
%	Les 5 facteurs multiplicatifs $A$, $B$, $C$, $D$ et $E$ sont liés par 2 relations indépendantes :
%	il est donc possible d'en choisir 3 arbitrairement.
%	
%	\smallskip
%	Choisissons par exemple $C=1/t$, $D=1/\nu$ et $E=1/U_1$ : 
%	dans ce cas, les relations trouvées dans la question précédente imposent que $A = 1/U_1$ et $B = 1/\sqrt{\nu t}$,
%	
%	\smallskip
%	et la relation $(i)$ s'écrit
%	\fbox{$\mathcal{F}(v/U_1, x/\sqrt{\nu t}, 1, 1, 1)=0$}
%	
%	\smallskip
%	Cette dernière expression indique que la solution du problème s'exprime comme une relation entre
%	$v/U_1$ et $x/\sqrt{\nu t}$, ce qui s'écrit explicitement en termes mathématiques comme 
%	\fbox{$v/U_1 = f(x/\sqrt{\nu t})$}
\end{enumerate}

\medskip

%==================================================================================================
\section{Résolution}
%==================================================================================================


\begin{enumerate}
\item
$\eta = \dfrac{x}{\sqrt{\nu t}}$
d'où \dotfill \fbox{$\dpdx{\eta} = \dfrac{1}{\sqrt{\nu t}}$} 
et \fbox{$\ddpdx{\eta} = 0$}

\smallskip
$\eta = \dfrac{x}{\sqrt{\nu}} \, t^{-1/2}$ avec $\dpdt{}\left (t^n\right) = n t^{n-1}$
d'où \dotfill $\dpdt{\eta} = \dfrac{x}{\sqrt{\nu}} \, \left(-\dfrac{1}{2} t^{-3/2}\right)$ : 
\fbox{$\dpdt{\eta} = -\dfrac{x}{2t\sqrt{\nu t}}$}
\item
$v(x, t) = U_1 f(\eta)$ d'où \dotfill
$\dpdt{v} = U_1 f'(\eta) \dpdt{\eta} 
=  -\dfrac{x U_1}{2t\sqrt{\nu t}} \, f'(\eta) $

\smallskip
et $\dpdx{v} = U_1 f'(\eta) \dpdx{\eta}$ donc \dotfill
$\ddpdx{v} = U_1 f''(\eta) \left (\dpdx{\eta}\right)^2 + U_1 f'(\eta) \ddpdx{\eta}
= \dfrac{U_1}{\nu t} \, f''(\eta) $

\smallskip 
On en déduit donc pour l'équation (1) : 
\dotfill
$\dpdt{v} = \nu \, \ddpdx{v} \Leftrightarrow -\dfrac{x U_1}{2t\sqrt{\nu t}} \, f'(\eta) = \nu \dfrac{U_1}{\nu t} \, f''(\eta)$ 

\smallskip
soit, après simplification et en remarquant que $\dfrac{x}{\sqrt{\nu t}} = \eta$ : 
\dotfill 
\fbox{$f''(\eta) + \dfrac{1}{2}\eta f'(\eta) = 0$}

\smallskip
Considérant les conditions limite et initiale, compte tenu de $\eta = x/\sqrt{\nu t}$ on remarque que 
\dotfill $\displaystyle \lim_{t\rightarrow 0} \eta = +\infty$ pour $x>0$

et que \dotfill $\displaystyle \lim_{x\rightarrow 0} \eta = 0$ pour $t>0$

\smallskip
On en déduit que la condition initiale $v(x>0, t=0) = 0$ s'écrit \dotfill
\fbox{$f(\eta \rightarrow \infty) = 0$}

\smallskip
et la condition limite $v(x=0, t) = U_1$ \dotfill
\fbox{$f(0) = 1$}

\item
En posant $f'(\eta) = g(\eta)$, l'équation à résoudre devient 
\dotfill
$g'(\eta) + \dfrac{1}{2}\eta \, g(\eta) = 0$

\smallskip
soit 
\dotfill
$\dfrac{dg}{d\eta} = -\dfrac{1}{2}\eta \, g 
\Leftrightarrow
\dfrac{1}{g}\, dg = -\dfrac{1}{2}\eta \, d\eta
\Leftrightarrow
\int_{g(0)}^{g(\eta)} \dfrac{1}{g^*} \, dg^* = \int_{0}^{\eta}-\dfrac{1}{2}\eta^* \, d\eta^*
$

\dotfill
$ 
\Leftrightarrow
\left [ {\color{white}-\dfrac{1}{4} {\eta^*}^2}  \hspace{-10mm}  \ln g^* \right]_{g(0)}^{g(\eta)} 
= \left[ -\dfrac{1}{4} {\eta^*}^2 \right]_{0}^{\eta}
\Leftrightarrow
\ln[g(\eta)] - \ln[g(0)] = \ln\left [ \dfrac{g(\eta)}{g(0)}\right] = -\dfrac{1}{4} \eta^2
$

On obtient donc
$\dfrac{g(\eta)}{g(0)} = e^{-\eta^2/4}$, soit \dotfill \fbox{$g(\eta) = g(0) e^{-\eta^2/4}$}
où $g(0)$ est une constante à définir.

\smallskip
Il reste à résoudre
\dotfill
$f'(\eta) = g(\eta) = g(0) e^{-\eta^2/4}$

soit, en intégrant par rapport à $\eta$ :\dotfill
$
\displaystyle \int_0^\eta f'(\eta^*) d\eta^* = \int_0^\eta g(0) e^{-{\eta^*}^2/4} d\eta^*
= g(0) \int_0^\eta  e^{-{\eta^*}^2/4} d\eta^*$

D'après la définition de la fonction erreur donnée dans l'énoncé,
$\mbox{erf}(x) = \dfrac{2}{\sqrt{\pi}} \int_0^x e^{-s^2} ds$,

on a donc \dotfill
$
\displaystyle \int_0^\eta f'(\eta^*) d\eta^* = g(0) \sqrt{\pi}\, \mbox{erf}\left(\dfrac{\eta}{2}\right)
\Leftrightarrow
f(\eta) - f(0) = g(0) \sqrt{\pi}\, \mbox{erf}\left(\dfrac{\eta}{2}\right)
$

La solution générale de l'équation (1) a donc pour expression 
\dotfill
$f(\eta) = f(0) + g(0) \sqrt{\pi}\, \mbox{erf}\left(\dfrac{\eta}{2}\right)$

où $f(0)$ et $g(0)$ sont deux constantes à définir avec les conditions déduites 
des conditions limites et intiales du problème d'origine, à savoir
\dotfill
\fbox{$f(0) = 1$} 

et \dotfill 
$f(\eta\rightarrow +\infty) = 0 \Leftrightarrow 0 = f(0) + g(0) \sqrt{\pi}\, \mbox{erf}(+\infty) = 1 + g(0) \sqrt{\pi}$

On en déduit \dotfill
\fbox{$g(0) = -\dfrac{1}{\sqrt{\pi}}$} et donc \fbox{$f(\eta) 
= 1 - \mbox{erf}\left ( \dfrac{\eta}{2}\right ) = \mbox{erfc}\left ( \dfrac{\eta}{2}\right )$}

\item
On en déduit la solution $v(x, t) = U_1 \, f(\eta) = U_1 \, \mbox{erfc}(\eta/2)$
avec $\eta = x/\sqrt{\nu t}$,

\smallskip
soit \dotfill
\fbox{$v(x, t) = U_1 \, \mbox{erfc}\left ( \dfrac{x}{\sqrt{4\nu t}} \right )
= U_1 \, \left [ 1 - \mbox{erf}\left( \dfrac{x}{\sqrt{4\nu t}}\right )\right ]$}

\medskip
Le profil adimensionné $v(x, t)/U_1$ pour différents instants est représenté sur la figure ci-dessous 

\begin{center}
	\includegraphics[width=120mm]{profils.pdf}
\end{center}

\medskip
Cette figure a été obtenue à l'aide du programme MATLAB suivant (fichier \texttt{trace\_profils.m}, disponible sur la page Moodle du cours) :

\underline{\hspace{150mm}}

\begin{verbatim}
	clear all; close all;
	
	% Valeur de la viscosite cinematique :
	nu = 1e-3;
	
	% Discretisation de l'espace avec 100 points entre 0 et 2 :
	x = linspace(0, 1, 100);
	
	% Definition des instants auxquels on souhaite tracer le profil :
	t = [0.5, 2 5, 10, 20, 50, 100];
	
	% Trace le profil pour les differents instants :
	figure
	hold on
	for n=1:length(t)
	  plot(x, erfc(x/sqrt(4*nu*t(n))), 'b');
	end
	xlabel('x'); ylabel('v(x, t) / U_1');
	
	% Trace la fleche du temps :
	arrowline([0.05,0.5],[0.05,0.5], 'color', 'k');
	text(0.5, 0.55, 'temps t :', 'Color', 'k');
	
	% On affiche les instants sur la figure :
	text(0.65, 0.55, num2str(t'), 'color', 'k');
	line([0.63 0.63], [0.4, 0.7], 'color', 'k');
	
	% On sauvegarde la figure sur le disque :
	print(gcf, 'profils.pdf', '-dpdf');  % format PDF
	print(gcf, 'profils.eps', '-deps');  % format Postscript encapsule
	print(gcf, 'profils.jpg', '-djpeg'); % format JPEG
\end{verbatim}

\underline{\hspace{150mm}}

\bigskip

\item
	$\delta(t) = \sqrt{\nu t}$ avec les dimensions suivantes : $[\nu] = L^2 T^{-1}$ et $[t] = T$
	
	d'où $[\delta(t)] = \left ( [\nu] \times [t]\right)^{1/2} = \left ( L^2 T^{-1}\times T\right)^{1/2}$ 
	donc \dotfill \fbox{$[\delta(t)] = L$}

\item
	Le tenseur des contraintes visqueuses a pour expression en repérage cartésien (cf. cours de MMC) :
	\[
		\mytensor{\tau} = 
		\mu
		\left (
		\begin{array}{ccccc}
			2 \; \dpdx{u_x} & & \dpdx{u_y} + \dpdy{u_x} & & \dpdx{u_z} + \dpdz{u_x} 
			\\ & & & & \\
			\dpdx{u_y} + \dpdy{u_x} & & 2 \; \dpdx{u_y} & & \dpdx{u_z} + \dpdz{u_y} 
			\\ & & & & \\
			\dpdx{u_z} + \dpdz{u_x} & & \dpdx{u_z} + \dpdz{u_x} & & 2 \; \dpdx{u_z}
		\end{array}
		\right )
	\]
	avec ici $u_x = u_z = 0$ et $u_y = v(x, t)$, d'où
	\[
		\mytensor{\tau} = 
		\mu
		\left (
		\begin{array}{ccccc}
			0 & & \dpdx{v}  & & 0 
			\\ & & & & \\
			\dpdx{v} & & 0 & & 0
			\\ & & & & \\
			0 & & 0 & & 0
		\end{array}
		\right )
	\]
	La paroi en $x=0$ est verticale et sa normale sortante est $\myvec{n} = \myvec{e}_x$.
	La contrainte pariétale est donc :
	\[
		\myvec{\tau}_p = \mytensor{\tau}(x=0) \cdot \myvec{n} = 
				\mu
		\left (
		\begin{array}{ccccc}
			0 & & \dpdx{v}(x=0, t)  & & 0 
			\\ & & & & \\
			\dpdx{v}(x=0, t) & & 0 & & 0
			\\ & & & & \\ 
			0 & & 0 & & 0
		\end{array}
		\right )
		\left (
		\begin{array}{c}
			1 \\ \\ 0 \\ \\ 0
		\end{array}
		\right )
		=
				\left (
		\begin{array}{c}
			 0 \\ \\ \mu \dpdx{v}(x=0, t) \\ \\ 0
		\end{array}
		\right )
	\]
	
	c'est-à-dire
	$\myvec{\tau}_p = 	\mu \dpdx{v}(x=0, t) \, \myvec{e}_y$ 
	avec $v(x,t) = U_1 \, f(\eta) 
	\; \Rightarrow \; 
	\dpdx{v} = U_1 \, f'(\eta) \, \dpdx{\eta} = U_1 \, g(\eta) \, \dfrac{1}{\sqrt{\nu t}}$
	
	D'où $\dpdx{v}(x=0, t) = g(0) \, \dfrac{U_1}{\sqrt{\nu t}}$ avec $g(0) = -\dfrac{2}{\sqrt{\pi}}$
	d'après la question 3.
	
	On en déduit 
	$\dpdx{v}(x=0, t) = \dfrac{-2U_1}{\sqrt{\pi \nu t}}$
	et donc 
	\dotfill
	\fbox{$\myvec{\tau}_p = \dfrac{-2\mu U_1}{\sqrt{\pi \nu t}} \, \myvec{e}_y$}

	On remarque que la contrainte pariétale est négative 
	(le frottement exercé par le fluide sur la paroi est bien dirigé vers le bas)
	et qu'elle décroît au cours du temps comme $1/\sqrt{\nu t}$.
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

