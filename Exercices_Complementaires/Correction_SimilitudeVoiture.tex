%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[10pt, a4paper]{article}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%--------------------------------------------------------------------------------------------------
% Dimensions :
%--------------------------------------------------------------------------------------------------

\setlength{\textheight}{26cm}
\setlength{\textwidth}{16cm}

\setlength{\topmargin}{-25mm}
\setlength{\oddsidemargin}{0mm}
\setlength{\evensidemargin}{0mm}

% \setlength{\columnsep}{20mm}

\setlength{\fboxsep}{1mm}
\setlength{\unitlength}{1mm}

%--------------------------------------------------------------------------------------------------
% Packages :
%--------------------------------------------------------------------------------------------------

\usepackage{latexsym}
\usepackage{graphicx}
\usepackage{pifont}
\usepackage{color}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{enumerate}

\usepackage[french]{babel}    % pour franciser le document

%\usepackage[latin1]{inputenc} % pour utiliser les caracteres accentues du claviers
\usepackage[utf8]{inputenc} 

\usepackage{accents}

\input{mymacros.tex}

%--------------------------------------------------------------------------------------------------
% Divers :
%--------------------------------------------------------------------------------------------------

\definecolor{rougefonce}{rgb}{0.7, 0.2, 0.2}

\renewcommand{\thickline}[2]{\linethickness{#1} \line(1, 0){#2}}
\renewcommand{\mycaption}[1]{\caption{\sl #1}}
\renewcommand{\myvec}[1]{\vec{#1}}

\newcommand{\footnoteremember}[2]{
\footnote{#2}
\newcounter{#1}
\setcounter{#1}{\value{footnote}}% \!\!\!
}
\newcommand{\footnoterecall}[1]{
\footnotemark[\value{#1}]
}

\newcommand{\rep}[1]{{\color{blue}{ #1}}}

% \pagestyle{empty}

\graphicspath{{Figures/}} % chemin d'acces au repertoire des figures (par ex.)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{center}

  \textsc{Université Toulouse 3 -- Paul Sabatier \hfill Année universitaire 2022-2023}
  
  \textsc{Mécanique des fluides \hfill L3 Mécanique}
  
  \vspace{0mm}
  
  \begin{center}
    \thickline{0.4mm}{160}
    \\ \vspace{3mm}
  \textbf{\large Exercice complémentaire 1.0 : Force de traînée sur une automobile}
    \\ %\vspace{1mm}
    \thickline{0.4mm}{160}
  \end{center}

  %\vspace{5mm}
  
\end{center}
On cherche à estimer la force de traînée $D$ s'exerçant sur une automobile de longueur $L$ et surface frontale $S$ roulant à la vitesse $V$ dans de l'air, à l'aide d'une expérience utilisant une maquette à échelle réduite dans un tunnel hydrodynamique.

\begin{enumerate}
\item Listez et discutez les paramètres physiques pertinents et indépendants dans ce problème.  

\rep{\em Réponse : 

On gardera comme paramètres la longueur $L$, la vitesse $V$, la masse volumique $\rho$ et la viscosité dynamique $\mu$ du fuide.

\begin{itemize}

\item $g$ n'est pas pertinent (contribue (faiblement) à la force verticale via la poussée d'Archimède mais pas à la force horizontale), 

\item $S$ non plus (proportionnel à $L^2$ pour une géométrie de voiture donnée).


\item La pression de référence (pression atmosphérique $p_{atm}$) non plus. C'est justifié lorsque le nombre de Mach est faible. C'est toujours le cas pour une automobile, la vitesse max étant de l'ordre de $140 km/h = 38.9 m/s$, le nombre de Mach vaut au maximum $M = 0.11$ compte tenu de la vitesse du son $c_0 = 340 m/s$.

Ce dernier argument est le plus difficile à justifier rigoureusement ; il a été énoncé sans démonstration au chapitre 1, et sera réexpliqué au chapitre 7 du cours.

\item Enfin, la température $T$ est un paramètre pertinent mais non indépendant (la masse volumique étant elle-même fonction de $T$).

\end{itemize}


 Donc $ D= { \cal F} ( V, L, \rho, \mu)$.
  
}

\item En utilisant les principes de l'analyse dimensionnelle, montrez que la traînée s'exprime simplement sous la forme suivante :

$$ 
D = \frac{1}{2}\rho S V^2 C_x( Re),  \quad \mbox{ avec } Re = \frac{\rho V L}{\mu}
$$

\rep{\em Réponse : 

On applique la démarche du cours :

$$\frac{D}{ M^* L^* {T^*}^{-2}} = \bar{\cal F}\left(\frac{V}{L^* {T^*}^{-1}},\frac{L}{L^*},\frac{\mu}{M^* {L^*}^{-1}  {T^*}^{-1}}\right).$$

Le choix des unités intrinsèques au problème le plus simple est $L^*=L$, $T^* = L/V$, $M^* = \rho L^3$. Ceci conduit à

$$D/(\rho L^2 V^2) = \bar{\cal F}(1,1,1,\frac{\mu}{\rho V L}).$$

La trainée adimensionnelle $D/(\rho L^2 V^2)$ est donc une fonction uniquement du paramètre $\frac{\mu}{\rho V L}$, et donc de son inverse dans lequel on reconnait le nombre de Reynolds. On réécrit donc cette dépendance sous la forme:

$$
D/(\rho L^2 V^2) = f(Re)  \quad \mbox{ avec } Re = \frac{\rho V L}{\mu}
$$

Par convention on préfère utiliser la surface de référence $S$. Celle-ci est proportionnelle à $L^2$ pour une géométrie donnée, c'est-à-dire que $S = \alpha L^2$ où $\alpha$ est un coefficient géométrique sans dimensions. On peut introduire une fonction $C_x(Re)$ définie simplement  par  $C_x(Re) =  2 f(Re)/\alpha$ ce qui conduit au résultat sous la forme recherchée. 

}


\item Calculez la valeur du nombre de Reynolds, pour une automobile de longueur $L = 4m$ (et surface frontale $S = 1.74 m^2$), roulant à la vitesse $V= 90 km/h$ dans de l'air (de propriétés $\rho_a$ $\mu_a$ données dans le formulaire).

\rep{\em Réponse : 

En unités internationales la vitesse vaut 
$V = 90 km/h = (90 \times 10^3 m) / (60\times 60 s) = (90/3.6) m/s = 25 m/s$.

%La valeur de $\nu$ pour l'air est donnée dans le formulaire : $\nu = 1.5 \times 10^{-5} m^2/s$.

Les propriétés de l'air sont données dans le formulaire : $\rho_a = 1.205 kg/m^3$, $\mu_a = 1.81 \cdot 10^{-5} Pa \cdot s$.

Le nombre de Reynolds vaut donc 

$$Re = 25 \times 4 / (1.5 \times 10^{-5}) =  {\color{red} 6.66 \cdot 10^6}.$$
}

\item Dans l'expérience, la maquette est à l'échelle 1/10 (c.a.d. $L_m = L/10$). La vitesse dans le tunnel hydrodynamique est notée $V_m$.  Le fluide est de l'eau (de propriétés $\rho_m$ $\mu_m$ données dans le formulaire).

Quelle doit être la valeur de $V_m$ pour assurer la similitude de Reynolds ?

\rep{\em Réponse : 

Si l'on veut être en similitude de Reynolds il faut que $Re_m = \rho_m V_m L_m / \mu_m = Re = 6.67 \times 10^6$.

%La valeur de $\nu_m$ est celle de l'eau donnée dans le formulaire : $\nu_m = 1 \times 10^{-6} m^2/s$.
Les propriétés de l'eau sont données dans le formulaire : $\rho_m \approx 10^3 kg/m^3$;
 $\mu_m \approx  10^{-3} Pa \cdot s$.

La longueur de la maquette est $L_m = L/10 = 0.4 m$. Donc :

$$V_m = Re_m \times \mu_m / (\rho_m L_m) = (6.67 \times 10^6) \times 10^{-3} / (0.4 \times 10^3) =  {\color{red} 16.7 m/s}$$
}

\item 
Le tunnel hydrodynamique étant réglé à la vitesse $V_m$ précédemment déterminée, 
on mesure à l'aide d'une balance de forces une traînée sur la maquette $D_m = 911 N$.
En déduire le $C_x$, puis la traînée $D$ sur la voiture.

\rep{\em Réponse : 

La surface de la maquette vaut $S_m = S/100 = 0.0174m^2$ ; en effet les longueurs sont diminuées d'un facteur 10, les surfaces d'un facteur $10^2 = 100$. Pour la maquette on a $D_m = (1/2) \rho_m S_m V_m^2 C_{x,m} = C_x(Re_m)$. 
Le coefficient de traînée de la maquette vaut donc :

$$ C_x(Re_m) = (2 D_m)/(\rho_m S_m V_m^2) = (2 \times 911)/(1000 \times 0.0174 \times 16.6^2) =  0.38$$

Puisque le coefficient de traînée ne dépend que du nombre de Reynolds, il est le même pour la maquette et la voiture réelle : $C_x(Re) = C_x(Re_m)$. C'est le principe même de la similitude !

On peut donc déduire la traînée exercée sur la voiture : 
 
 $$D = (1/2) \rho S V^2 C_x(Re) = .5 \times 1.2 \times1.67 \times 25^2 \times 0.38 =  {\color{red} 245 N}$$


}

\item En déduire la puissance dissipée par les forces aérodynamiques, puis la puissance qui doit être fournie par le moteur, en supposant que le rendement énergétique global de celui-ci est $\eta = {\color{red} 20 \%}$.

\rep{\em Réponse : 

La puissance des forces aérodynamiques vaut 
$$  { \cal P}_{aero} = D \times V =  {\color{red} 6.12 kW}$$

Le rendement du moteur peut être défini par $\eta =  { \cal P}_{aero} /  { \cal P}_{moteur}$. 

Avec les données du problème on a une puissance fournie par le moteur : 

$$ { \cal P}_{moteur} = {\color{red} 30.6 kW = 41 hp}$$

($1 hp$ = 1 cheval-vapeur = $746 W$; unité différente du cheval-fiscal apparaissant sur la carte grise !)
}

\end{enumerate}
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

