%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[10pt, a4paper]{article}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%--------------------------------------------------------------------------------------------------
% Dimensions :
%--------------------------------------------------------------------------------------------------

\setlength{\textheight}{26cm}
\setlength{\textwidth}{16cm}

\setlength{\topmargin}{-25mm}
\setlength{\oddsidemargin}{0mm}
\setlength{\evensidemargin}{0mm}

% \setlength{\columnsep}{20mm}

\setlength{\fboxsep}{1mm}
\setlength{\unitlength}{1mm}

%\newcommand{\rep}[1]{{\color{blue}{ #1}}}
\newcommand{\rep}[1]{}


%--------------------------------------------------------------------------------------------------
% Packages :
%--------------------------------------------------------------------------------------------------

\usepackage{latexsym}
\usepackage{graphicx}
\usepackage{pifont}
\usepackage{color}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{enumerate}

\usepackage[french]{babel}    % pour franciser le document

%\usepackage[latin1]{inputenc} % pour utiliser les caracteres accentues du claviers
\usepackage[utf8]{inputenc} 

\usepackage{accents}

\input{mymacros.tex}

%--------------------------------------------------------------------------------------------------
% Divers :
%--------------------------------------------------------------------------------------------------

\definecolor{rougefonce}{rgb}{0.7, 0.2, 0.2}

\renewcommand{\thickline}[2]{\linethickness{#1} \line(1, 0){#2}}
\renewcommand{\mycaption}[1]{\caption{\sl #1}}
\renewcommand{\myvec}[1]{\vec{#1}}

\newcommand{\footnoteremember}[2]{
\footnote{#2}
\newcounter{#1}
\setcounter{#1}{\value{footnote}}% \!\!\!
}
\newcommand{\footnoterecall}[1]{
\footnotemark[\value{#1}]
}

\newcommand{\dpa}[2]{\frac{ \partial #1}{\partial #2}}

% \pagestyle{empty}

\graphicspath{{Figures/}} % chemin d'acces au repertoire des figures (par ex.)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{center}

  \textsc{Université Toulouse 3 -- Paul Sabatier \hfill Année universitaire 2022-2023}
  
  \textsc{Mécanique des fluides \hfill L3 Mécanique}
  
  \vspace{0mm}
  
  \begin{center}
    \thickline{0.4mm}{160}
    \\ \vspace{3mm}
  \textbf{\large Exercice complémentaire 5.00 : Ecoulement de Stokes autour d'une sphère }
    \\ %\vspace{1mm}
    \thickline{0.4mm}{160}
  \end{center}

  %\vspace{5mm}
  
\end{center}

Dans un article majeur paru en 1846, G.G. Stokes a établi (entre autres) la solution de l'écoulement d'un fluide autour d'une sphère,
 dans le régime dominé par la viscosité (maintenant appelé régime de Stokes).
En décrivant les champs de vitesse et de pression en coordonnées sphériques, c.a.d. 
$\vec{u} = u_r(r,\theta) \vec{e}_r + u_\theta(r,\theta) \vec{e}_\theta$  et $p = p(r,\theta)$, la Solution de Stokes s'écrit :


\begin{eqnarray*}
  u_{r}(r,\theta) & = & U \cos \theta \left [ 1 - \frac{3a}{2r} + \frac{a^3}{2r^3} \right ]
  \\
  u_{\theta}(r,\theta) & = & -U\sin \theta \left [1 - \frac{3a}{4r} - \frac{a^3}{4r^3} \right ]
  \\
  p(r,\theta) & = & p_{\infty} - \frac{3}{2} \mu U a \frac{\cos \theta}{r^2}
\end{eqnarray*}

Le but de cet exercice est de vérifier que cette expression est bien solution des équations du mouvement, et de calculer la force exercée sur la sphère.

(On néglige la gravité pour simplifier).

\begin{enumerate}

\item A partir du formulaire (section D.3), écrivez l'équation de continuité sous sa forme la plus simple.



\rep{

On part de l'expression de $div(\vec{u})$ donnée dans le formulaire et on supprime le terme contenant la composante $u_\varphi$ qui est nulle dans le cas présent. On arrive à :

\begin{equation}
div(\vec{u}) =\frac{\partial u_r}{\partial r} + \frac{2 u_r}{r} + \frac{1}{r} \frac{\partial u_\theta}{\partial \theta} + \frac{u_\theta}{r \tan \theta}  = 0.
\end{equation}
}

\item Vérifiez que la solution de Stokes est bien solution de cette équation de continuité.

\rep{

Il y a 4 termes dans cette expression que nous allons évaluer un par un :

$$
 \frac{\partial u_r}{\partial r} = U \cos \theta \left[ \frac{3a}{2r^2} - \frac{3 a^3}{2 r^4} \right], 
\qquad 
\frac{2 u_r}{r} =
U \cos \theta \left[ \frac{2}{r} - \frac{3a}{r^2} + \frac{a^3}{r^4} \right],  
$$
$$
 \frac{1}{r} \frac{\partial u_\theta}{\partial \theta} =  -U \cos \theta \left[ \frac{1}{r} - \frac{3a}{4r^2} - \frac{a^3}{4r^4} \right ], 
 \qquad 
 \frac{u_\theta}{r \tan \theta} 
 =  -U \cos \theta \left[ \frac{1}{r} - \frac{3a}{4r^2} - \frac{a^3}{4r^4} \right].
 $$

En rassemblant les termes du même type, on arrive à 
$$
div(\vec{u}) = U \cos \theta \left( \frac{1}{r} \left[ 2 - 1 -1 \right] 
+ \frac{a}{r^2} \left[ \frac{3}{2} - 3 + \frac{3}{4} + \frac{3}{4}  \right] 
+\frac{a^3}{r^4} \left[-\frac{3}{2} +1 + \frac{1}{4} + \frac{1}{4}  \right] \right) 
= 0.
$$

}



\item Toujours a partir du formulaire, écrivez sous leur forme la plus simple les projections dans les directions $r$ et $\theta$ de l'équation de Stokes.

\rep{
L'équation de Stokes est obtenue a partir de l'équation de Navier-Stokes en supprimant les termes "instationnaire" et "convectif" qui sont les termes de gauche de l'équation. On va de plus supprimer tous les termes faisant intervenir $u_\varphi$ et toutes les dérivées par rapport à $\varphi$ qui ne sont pas pertinents ici.
En introduisant ces simplifications dans les deux premières composantes de l'équation donnée dans le formulaire (la troisième composante n'étant pas pertinente ici), et en divisant par $\nu$ pour mettre sous la forme la plus simple, on arrive à :

\begin{equation}
0 = - \frac{1}{\mu} \dpa{p}{r} +  \Delta_r (\vec{u}), 
\mbox{ avec } 
\Delta_r ( \vec{u} ) = 
 \frac{1}{r} \dpa{^2}{r^2} \left( r u_r \right) 
+ \frac{1}{r^2} \frac{\partial^2 u_r}{\partial \theta^2} + \frac{1}{r^2 \tan \theta} \frac{\partial u_r}{\partial \theta} 
- \frac{2}{r^2} \left[ u_r + \dpa{u_\theta}{\theta} + \frac{u_\theta}{\tan \theta} \right].
\end{equation}

\begin{equation}
0 = - \frac{1}{r \mu} \dpa{p}{\theta} +  \Delta_\theta (\vec{u}), 
\mbox{ avec } 
\Delta_\theta ( \vec{u} ) = 
 \frac{1}{r} \dpa{^2}{r^2} \left( r u_\theta \right) 
+ \frac{1}{r^2} \frac{\partial^2 u_\theta}{\partial \theta^2} + \frac{1}{r^2 \tan \theta} \frac{\partial u_\theta}{\partial \theta} 
+ \frac{1}{r^2} \left[ 2 \dpa{u_r}{\theta} - \frac{u_\theta}{\sin^2 \theta} \right].
\end{equation}

}

\item Montrez que la solution de Stokes est bien solution de ces équations.

\rep{
On va faire ce travail pour l'équation (2) correspondant à la composante selon $r$ seulement (la vérification de l'équation (3) se fait de la même manière).
Il y a 7 termes à évaluer. Les 4 premiers sont :
$$
 - \frac{1}{r \mu} \dpa{p}{\theta} =  3 U a \frac{\cos \theta}{r^3},  
   \qquad
 \frac{1}{r} \dpa{^2}{r^2} \left( r u_\theta \right) = \frac{3 U a^3 \cos \theta  }{r^5},
$$
$$
 \frac{1}{r^2} \frac{\partial^2 u_r}{\partial \theta^2} 
 = - U \cos \theta \left [ \frac{1}{r^2} - \frac{3a}{2r^3} + \frac{a^3}{2r^5} \right ],
 \qquad 
  \frac{1}{r^2 \tan \theta} \frac{\partial u_r}{\partial \theta} 
  =  - U \cos \theta \left [ \frac{1}{r^2} - \frac{3a}{2r^3} + \frac{a^3}{2r^5} \right ].
$$
Les trois derniers termes contiennent des ingrédients qui ont déjà été évalués dans le calcul de la divergence, et peuvent être regroupés pour s'écrire 
$$
- \frac{2}{r^2} \left[ u_r + \dpa{u_\theta}{\theta} + \frac{u_\theta}{\tan \theta} \right]
= U \cos \theta \left [ \frac{2}{r^2}  - \frac{2 a^3}{r^5} \right ].
$$
En regroupant de nouveau les termes de même forme on obtient :
$$ U \cos \theta \left( \frac{1}{r^2} \left[-1 - 1 +2 \right] 
+ \frac{a}{r^3} \left[ 3 - \frac{3}{2} - \frac{3}{2} \right] 
+\frac{a^3}{r^5} \left[3 -\frac{1}{2}-\frac{1}{2} -2   \right] \right) 
= 0.
$$
}

\item Donnez l'expression des contraintes visqueuses à la surface de la sphère $\tau_{rr}(r=a,\theta)$ et $\tau_{r\theta}(r=a,\theta)$.


\rep{
D'après le formulaire :
$$
{\tau_{rr} = 2 \mu \left( \frac{\partial u_r}{\partial r} \right) ;
\quad   
\tau_{r\theta} = \mu  \left( \frac{1}{r} \frac{\partial u_r}{\partial \theta} 
+ \frac{\partial u_\theta}{\partial r} - \frac{u_\theta}{r} \right) 
}$$

Toutes les dérivées du champ de vitesse nécessaire ont déjà été évaluées dans les questions précédentes. En rassemblant les morceaux on arrive à :
$$
{\tau_{rr} = 2 \mu U \cos \theta \left[ \frac{3a}{2r^2} - \frac{3 a^3}{2 r^4} \right] ;
\quad   
\tau_{r\theta} = \mu U \sin \theta \left[ - \frac{3 a^3}{2 r^4} \right]
}$$

En évaluant ces quantités ainsi que la pression à la surface (en $r=a$) on trouve :
$$
{\tau_{rr}(a,\theta) = 0;
\quad   
\tau_{r\theta}(a,\theta) = \frac{-3 \mu U \sin \theta}{2a}; \quad 
p(a,\theta) = p_{\infty} - \frac{3 \mu U \cos \theta }{2 a}.
}$$

}

\item En déduire la force exercée par le fluide sur la sphère.

\rep{ Le vecteur contrainte est donné par 

\begin{equation*}
  \vec{F}_{ \mbox{fluide} \rightarrow \mbox{sphère}} 
  \equiv 
  \oint_{\mbox{\rm \scriptsize \sl sphère}}
 \mytensor{\sigma} \cdot \myvec{n}_{ \mbox{sphère} \rightarrow \mbox{fluide} } 
 \; dS
\end{equation*}

On utilise ici la convention MMC : la normale est sortante par rapport au domaine qui reçoit la force qu'on cherche à évaluer, ici la sphère, elle est donc orientée de la sphère vers le fluide.  Celle-ci vaut  
$\myvec{n}_{ \mbox{sphère} \rightarrow \mbox{fluide}}  = \vec{e}_r$.

La contrainte vaut donc :
$$
\mytensor{\sigma} \cdot \myvec{n}_{ \mbox{sphère} \rightarrow \mbox{fluide}} = - p \vec{e}_r + \mytensor{\tau} \cdot \vec{e}_r 
=
- p \vec{e}_r + \left( \tau_{rr} \vec{e}_r  + \tau_{r\theta} \vec{e}_\theta \right)
$$

En reportant dans l'intégrale et en explicitant les bornes d'intégration et l'élément de surface sphérique, on a 

$$
 \vec{F}_{ \mbox{fluide} \rightarrow \mbox{sphère}} 
\int_{\theta=0}^{\pi} \int_{\varphi=0}^{2\pi} 
  \left[ (- p + \tau_{rr} ) \vec{e}_{r} + \tau_{r\theta} \vec{e}_{\theta} \right] \; 
  a^2 \sin \theta d \theta d\varphi
  $$

Pour évaluer l'intégrale il va maintenant falloir exprimer $\vec{e}_r$ et $\vec{e}_\theta$ dans une base fixe 
$(\vec{e}_x, \vec{e}_y, \vec{e}_z)$. Ce changement de base s'écrit :

$$
\vec{e}_r = \cos \theta \vec{e}_x + \sin \theta \cos \varphi  \vec{e}_y + \sin \theta \sin\varphi  \vec{e}_z ; 
\quad  
\vec{e}_\theta = - \sin \theta \vec{e}_x + \cos \theta \cos \varphi  \vec{e}_y + \cos \theta \sin\varphi  \vec{e}_z.
$$

Il faut maintenant remplacer $p, \tau_{rr}$ et $\tau_{r\theta} $ par leur expression en $r=a$ calculée plus haut.
Par raison de symétrie les projections selon $\vec{e}_y$ et $\vec{e}_z$ sont nulles, il reste donc :

$$
 \vec{F}_{ \mbox{fluide} \rightarrow \mbox{sphère}} = 
2 \pi a^2 \int_{\theta=0}^{\pi} \left[ - p_\infty \sin \theta \cos \theta + \frac{3\mu U}{2 a} \sin \theta \cos^2 \theta + \frac{3\mu U}{2 a} \sin^3 \theta \right] \; d \theta  \times \vec{e}_x. 
  $$
Il reste à évaluer les petites intégrales suivantes :

$$\int_0^\pi \sin \theta \cos \theta d \theta = {\left[ \frac{\sin 2 \theta}{2} \right]}_0^\pi = 0; \quad 
\int_0^\pi \cos^2 \theta \sin \theta d \theta = {\left[ - \frac{\cos^3 \theta}{3} \right]}_0^\pi = \frac{2}{3};
 $$
 $$
 \int_0^\pi \sin^3  \theta d \theta = \int_0^\pi \frac{\ 3 \sin \theta- \sin 3 \theta }{4} d \theta 
 = {\left[ - \frac{3 \cos  \theta}{4} +  \frac{\cos 3 \theta}{12} \right]}_0^\pi = \frac{4}{3}.
 $$

En rassemblant toutes les parties on arrive finalement au résultat attendu :
$$
 \vec{F}_{ \mbox{fluide} \rightarrow \mbox{sphère}} = 6 \pi \mu U a \vec{e}_x.
 $$


}
\end{enumerate}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

