%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[10pt, a4paper]{article}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%--------------------------------------------------------------------------------------------------
% Dimensions :
%--------------------------------------------------------------------------------------------------

\setlength{\textheight}{25cm}
\setlength{\textwidth}{16cm}

\setlength{\topmargin}{-20mm}
\setlength{\oddsidemargin}{0mm}
\setlength{\evensidemargin}{0mm}

% \setlength{\columnsep}{20mm}

\setlength{\fboxsep}{1mm}
\setlength{\unitlength}{1mm}

%--------------------------------------------------------------------------------------------------
% Packages :
%--------------------------------------------------------------------------------------------------

\usepackage{latexsym}
\usepackage{graphicx}
\usepackage{pifont}
\usepackage{color}
\usepackage{amsmath}
\usepackage{amssymb}

\usepackage[french]{babel}    % pour franciser le document

\usepackage[latin1]{inputenc} % pour utiliser les caracteres accentues du claviers


%--------------------------------------------------------------------------------------------------
% Divers :
%--------------------------------------------------------------------------------------------------

\definecolor{rougefonce}{rgb}{0.7, 0.2, 0.2}

\newcommand{\thickline}[2]{\linethickness{#1} \line(1, 0){#2}}
\newcommand{\mycaption}[1]{\caption{\sl #1}}
\newcommand{\myvec}[1]{\vec{#1}}

\pagestyle{empty}

\graphicspath{{Figures/}} % chemin d'acces au repertoire des figures (par ex.)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{center}

  \textsc{Universit� Paul Sabatier Toulouse 3\hfill Ann�e universitaire 2022-2023}
  
  \textsc{M�canique des fluides \hfill L3 M�canique}
  
  \vspace{5mm}
  
  \begin{center}
    \thickline{0.4mm}{160}
    \\ \vspace{3mm}
  \textbf{\large Exercice compl�mentaire 7 : vidange de r�servoir}
    \\ %\vspace{1mm}
    \thickline{0.4mm}{160}
  \end{center}

  \vspace{5mm}
  
\end{center}

On consid�re la vidange sous l'effet de la pesanteur $\myvec{g}$
d'un r�servoir rempli de liquide � travers un petit orifice localis� sur le fond. 
On notera $P_0$ la pression atmosph�rique.
On suppose que la section $S$ du r�servoir est grande devant la section du trou de vidange $S_0$ 
de telle sorte 
que $\varepsilon= S_0/S \ll 1$. 
On notera $h$ la diff�rence de niveau entre la surface libre et l'orifice de vidange.
On supposera dans tout le probl�me que l'�coulement v�rifie l'approximation d'�coulement inertiel.

\begin{figure}[htb]
  \begin{center}
    \begin{picture}(160, 55)(-5, 0)
      \put(0, 0){\includegraphics[height=50mm]{reservoir.pdf}}
      \put(90, 0){\includegraphics[height=50mm]{clepsydre.pdf}}
      \put(30, 0){(a)}
      \put(110, 0){(b)}
      \put(72, 27){\colorbox{white}{$h(t)$}}
      \put(149, 27){\colorbox{white}{$h(t)$}}
      \put(50, 46){$P_0$}
      \put(130, 46){$P_0$}
      \put(0, 10){$0$}
      \put(91, 11){$0$}
      \put(2, 39){$z$}
      \put(92.5, 39){$z$}
      \put(49, 5){$V_0$}
      \put(126.5, 5){$V_0$}
      \put(34, 45){$A$}
      \put(47, 14){$B$}
      \put(15, 45){\vector(0, -1){10}}
      \put(16, 40){$\myvec{g}$}
    \end{picture}
  \end{center}
  \mycaption{Vidange d'un r�servoir de section constante (a) et d'une clepsydre (b).}
  \label{fig:vidange}
\end{figure}

\begin{enumerate}
\item
	En notant $V$ la vitesse de descente de la surface libre
	et $V_0$ la vitesse au niveau du trou de vidange, �crire la conservation de la masse
	pour obtenir une relation entre $V$, $V_0$ et $\varepsilon$.
\item
	En d�duire que l'on peut n�gliger la vitesse de descente de la surface libre par rapport
	� la vitesse du jet en sortie, et donc supposer l'�coulement quasi-stationnaire.
\item
	Ecrire l'�quation pour la pression motrice � travers le jet de sortie pour d�terminer
	la pression en sortie de vidange.
	On n�gligera les effets capillaires.
\item
	Ecrire la conservation de l'�nergie m�canique (ou th�or�me de Bernoulli)
	sur la ligne de courant entre les points $A$ et $B$ (fig.~\ref{fig:vidange}a)
	pour d�terminer la vitesse du jet en sortie (th�or�me de Torricelli).
\item
	En d�duire une �quation d'�volution pour la hauteur de liquide $h(t)$ dans le r�servoir.
\item
	Int�grer cette �quation pour obtenir le temps de vidange $T$ du r�servoir de section $S$ constante 
	(fig.~\ref{fig:vidange}a) initialement rempli d'une hauteur $h_0$ de liquide.
\item
	On v�rifiera que dans ce cas le niveau de liquide descend � une vitesse $V$ 
	qui varie au cours du temps.
\item
	Pour mesurer le temps en graduant lin�airement le r�servoir, il faut avoir un niveau qui descend
	� vitesse constante.
	C'est le principe de la clepsydre (fig.~\ref{fig:vidange}b).
	D�duire des r�sultats pr�c�dents la variation $S(z)$ de la section du r�servoir 
	qui permet une vitesse de descente constante de la surface libre.
	On notera $S_{\mbox{\sl \scriptsize max}}$ la section au sommet $z_{\mbox{\sl \scriptsize max}}$
	du r�servoir.
\item
	Donner la relation liant $S_{\mbox{\sl \scriptsize max}}$ et $z_{\mbox{\sl \scriptsize max}}$
	� la vitesse de descente constante $V$ que l'on souhaite obtenir. 
\item
	En supposant la clepsydre axisym�trique, en d�duire la loi de rayon $R(z)$ du r�servoir.
	On notera $R_{\mbox{\sl \scriptsize max}}$ le rayon maximum du r�servoir au sommet.
\item
	Tracer la forme de la clepsydre sous Matlab.
\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

