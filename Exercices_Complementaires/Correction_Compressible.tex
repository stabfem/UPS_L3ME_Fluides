%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[10pt, a4paper]{article}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%--------------------------------------------------------------------------------------------------
% Dimensions :
%--------------------------------------------------------------------------------------------------

\setlength{\textheight}{26cm}
\setlength{\textwidth}{16cm}

\setlength{\topmargin}{-25mm}
\setlength{\oddsidemargin}{0mm}
\setlength{\evensidemargin}{0mm}

% \setlength{\columnsep}{20mm}

\setlength{\fboxsep}{1mm}
\setlength{\unitlength}{1mm}

%--------------------------------------------------------------------------------------------------
% Packages :
%--------------------------------------------------------------------------------------------------

\usepackage{latexsym}
\usepackage{graphicx}
\usepackage{pifont}
\usepackage{color}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{enumerate}

\usepackage[french]{babel}    % pour franciser le document

%\usepackage[latin1]{inputenc} % pour utiliser les caracteres accentues du claviers
\usepackage[utf8]{inputenc} 

\usepackage{accents}

\input{mymacros.tex}

%--------------------------------------------------------------------------------------------------
% Divers :
%--------------------------------------------------------------------------------------------------

\definecolor{rougefonce}{rgb}{0.7, 0.2, 0.2}

\renewcommand{\thickline}[2]{\linethickness{#1} \line(1, 0){#2}}
\renewcommand{\mycaption}[1]{\caption{\sl #1}}
\renewcommand{\myvec}[1]{\vec{#1}}

\newcommand{\footnoteremember}[2]{
\footnote{#2}
\newcounter{#1}
\setcounter{#1}{\value{footnote}}% \!\!\!
}
\newcommand{\footnoterecall}[1]{
\footnotemark[\value{#1}]
}

\newcommand{\question}[1]{}
\newcommand{\answer}[1]{#1}

% \pagestyle{empty}

\graphicspath{{../FIGURES/}{Figures/}} % chemin d'acces au repertoire des figures (par ex.)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{center}

  \textsc{Université Toulouse 3 -- Paul Sabatier \hfill Année universitaire 2023-2024}
  
  \textsc{Mécanique des fluides \hfill L3 Mécanique}
  
  \vspace{0mm}
  
  \begin{center}
    \thickline{0.4mm}{160}
    \\ \vspace{3mm} 
  \textbf{\large Exercice complémentaire 12.0: 
  \\
 Régimes isentropiques dans une tuyère.
\\
\,
\\
  {\em Eléments de correction}
  }
    \\ %\vspace{1mm}
    \thickline{0.4mm}{160}
  \end{center}

  %\vspace{5mm}
  
\end{center}


\begin{figure}[h]
$$\includegraphics[width=.5\linewidth]{../TD_Fluides/Figures/Exo12_0_1.png}
$$\caption{fonction $u_c = f(M)$ de la question 1.}
\end{figure}


{\bf Régime 1}
 
\begin{enumerate}



\item Cherchons une relation entre $T_i$ et la vitesse $u$ en un point donné de la tuyère, permettant de déterminer 
le nombre de mach local $M$ = $u/c$.
On a d'une part la 1ère relation de St Venant :
$
\frac{T}{T_i} = \frac{c^2}{c_i^2} = \left( 1+ \frac{\gamma-1}2 M^2 \right)^{-1}
$
%où $T$ est la température locale et $c = \sqrt{\gamma r T}$ la vitesse du son locale.





On a donc : 
$$
u =  M c =  \frac{M c_i}{\left( 1+ \frac{\gamma-1}2 M^2 \right)^{1/2}} \equiv f(M).
$$
avec $c_i = \sqrt{\gamma r T_i} = 375 m/s$ la vitesse du son dans les conditions génératrices.

On cherche la valeur $M_c$ telle que $f(M_c) =  250 m/s$.
On peut chercher la solution en traçant la fonction $f(M)$. Le programme Matlab donne cette solution (voir figure).
On trouve finalement :  

$M_c = 0.831$.

On en déduit la température au col : $T_c = \sqrt{\frac{c_c}{\gamma r} } = \sqrt{\frac{u_c}{M_c \gamma r} } = 268 K$.


{\bf Remarque : } On peut aussi utiliser le tableau de valeur du formulaire, donnant $U^* = u/c_i$ en fonction de Mach, mais celui-!ci n'est pas précis car le tableau de valeur a été établi pour $T_i = 288 K$. 

%Autre détermination : $T = \frac{T_i}{\left( 1+ \frac{\gamma-1}2 M_c^2 \right)} = 313 K$ ????

\item L'écoulement est forcément subsonique dans le convergent (car accéléré à partir d'une vitesse initiale supposée négligeable).
Au col il est également subsonique. Donc la tuyère n'est pas amorcée : dans le divergent l'écoulement ralentit et reste subsonique.


{\color{red} {\bf REMARQUE :} les questions 3 et 4 sont inversées par rapport à l'énoncé distribué (voir nouvelle versions sur la version en ligne du fascicule de TD).}


\item Exploitons la première relation de St Venant :
$$
\frac{T_s}{T_i} =  \left( 1+ \frac{\gamma-1}2 M_s^2 \right)^{-1}
$$
qui s'inverse en :
$$
M_s = \sqrt{\frac{2}{\gamma -1} \left( \frac{T_i}{T_s} -1 \right)}
$$
Avec les données de l'énoncé on trouve $M_c = 0.18$. 

\item On va exploiter maintenant la seconde relation de St Venant :

$$
\frac{P_s}{P_i} =  \left( 1+ \frac{\gamma-1}2 M_s^2 \right)^{\frac{-\gamma}{\gamma-1}}
$$

Avec les données de l'énoncé ($P_a = 1 atm = 1013 hPa$), on trouve $P_i = 1036 hPa$.

On déduit finalement la masse volumique a partir de l'équation d'état :

$\rho_i = P_i/(r T_i) = 1.203 kg/m^3$.

\item A partir de la relation établie à la question 1 et de la figure : $u_s = 60 m/s$.

La masse volumique en sortie peut être déterminée par la 3e relation de St Venant, ou par l'équation d'état : $\rho_s = 1.184 kg/m^3$.

La section de sortie vaut $A_s = \pi D_s^2/4 = 19.6 cm^2$/.

On en déduit finalement le débit : $\dot m = A_s \rho_s u_s = 0.139 kg/s$.

{\bf Régime 2}
 
\item Puisque l'écoulement en sortie est supersonique, c'est qu'il est supersonique dans tout le divergent. Il ne peut être sonique qu'au niveau du col, et est forcément subsonique dans le convergent.

\item Exploitons les relations de St Venant, en appliquant les formules, ou en lisant les valeurs correspondant à la ligne $M = 1.5$ dans le tableau du formulaire.
On lit : 

$P_s /P_i = 0.2719 \qquad \Longrightarrow P_i = 3726 hPa$.

$T_s /T_i = 0.2719 \qquad \Longrightarrow T_i = 431 K$.

$\rho_i = \frac{P_i}{r T_i} = 3.01 kg/m^3$.


\item 
D'après la relation de St Venant : $T_c = \left( 1+ \frac{\gamma-1}{2} M_c^2 \right)^{-1} T_i$ avec ici $M_c= 1$.
c.a.d. $T_c = 358 K$.

\item La relation (4) du formulaire donne $A/A_c$ en fonction de $M$. On peut de nouveau utiliser le tableau de valeurs pour lire directement dans la ligne $M=1.5$ : $A/A_c = 1.1756$.

On en déduit alors $A_c = 16.68 cm^2$, soit un diamètre de $D_c = \sqrt{4 A_c/\pi} =  4.6cm$.

\item Le débit peut être calculé en fonction des données au col, $\dot m = A_c \rho_c u_c$. 
On a $u_c = c_c = \sqrt{\gamma r T_c} = 380 m/s$, et $\rho_c = 0.643 \rho_i$ (d'après le tableau de valeurs pour la ligne 
$M=M_c = 1$, soit $\rho_c = 1.937 kg/m^3$.

L'application numérique donne finalement : $\dot m = 5.815 kg/s$.

\textbf{R\'egime 3 :}


\item {\color{red} Question supplémentaire (voir énoncé sur la version en ligne du fascicule de TD):}

 
 $P_2 /P_1 = 1.4601 \qquad \Longrightarrow P_2 = 1478 hPa$.

$T_2 /T_1 = 1.3237 \qquad \Longrightarrow T_2 =  570 K$.
 
 $M_2 = 0.701$.
 
 
 NB : ce régime de fonctionnement n'est pas possible si la pression ambiante est la pression atmosphérique, car le choc conduirait à une remontée de pression à une pression supérieure à la pression ambiante.
 En revanche il est possible si le moteur fusée fonctionne dans une atmosphère à plus haute pression (sur Vénus ?)
 
 
\end{enumerate}





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

