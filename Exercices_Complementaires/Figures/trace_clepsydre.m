clear all; close all;
R = linspace(-1, 1, 100);
Z = R.^4;
plot(R, Z, 'k', 'LineWidth', 2);
hold on
plot([-1 1], [1 1], 'k')
plot([0 0], [-0.1 1.1], 'k:')
axis equal
axis([-1.1, 1.1 -0.1, 1.1])
print(gcf, 'cleps.jpg', '-djpeg')
print(gcf, 'cleps.eps', '-deps')