%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[10pt, a4paper]{article}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%--------------------------------------------------------------------------------------------------
% Dimensions :
%--------------------------------------------------------------------------------------------------

\setlength{\textheight}{25.7cm}
\setlength{\textwidth}{16cm}

\setlength{\topmargin}{-22mm}
\setlength{\oddsidemargin}{0mm}
\setlength{\evensidemargin}{0mm}

% \setlength{\columnsep}{20mm}

\setlength{\fboxsep}{1mm}
\setlength{\unitlength}{1mm}

%--------------------------------------------------------------------------------------------------
% Packages :
%--------------------------------------------------------------------------------------------------

\usepackage{latexsym}
\usepackage{graphicx}
\usepackage{pifont}
\usepackage{color}
\usepackage{amsmath}
\usepackage{amssymb}

\usepackage[french]{babel}    % pour franciser le document

\usepackage[latin1]{inputenc} % pour utiliser les caracteres accentues du claviers


%--------------------------------------------------------------------------------------------------
% Divers :
%--------------------------------------------------------------------------------------------------

\definecolor{rougefonce}{rgb}{0.7, 0.2, 0.2}

\newcommand{\thickline}[2]{\linethickness{#1} \line(1, 0){#2}}
\newcommand{\mycaption}[1]{\caption{\sl #1}}
\newcommand{\myvec}[1]{\vec{#1}}

%\pagestyle{empty}

\graphicspath{{Figures/}} % chemin d'acces au repertoire des figures (par ex.)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{center}

  \textsc{Universit� Paul Sabatier Toulouse 3\hfill Ann�e universitaire 2022-2023}
  
  \textsc{M�canique des fluides \hfill L3 M�canique}
  
  \vspace{5mm}
  
  \begin{center}
    \thickline{0.4mm}{160}
    \\ \vspace{3mm}
  \textbf{\large Exercice compl�mentaire 8 : transvasement � travers une conduite}
    \\ %\vspace{1mm}
    \thickline{0.4mm}{160}
  \end{center}

  \vspace{5mm}
  
\end{center}

On s'int�resse dans ce probl�me au transvasement d'un liquide de masse volumique $\rho$
et de viscosit� cin�matique $\nu$ entre un premier
r�servoir ${\cal R}_1$, de section $S_1$, et un second, ${\cal R}_2$, de section $S_2$.
Le transvasement s'effectue par l'interm�diaire d'une conduite cylindrique $\cal C$ 
de diam�tre $d$ et de longueur $L$ reliant le fond des deux r�servoirs
(fig.~\ref{fig:transvasement}).
On suppose que la section $S$ de la conduite est tr�s petite devant celles des deux
r�servoirs~: on note $\varepsilon_1 = S/S_1 \ll 1$ et $\varepsilon_2 = S/S_2 \ll 1$.
Le liquide est en contact avec l'air � pression atmosph�rique $p_0$.
Il est en outre soumis au champ de pesanteur $\myvec{g}$.

Initialement, les r�servoirs ${\cal R}_1$ et ${\cal R}_2$ contiennent 
respectivement les hauteurs $h_1^0$ et $h_2^0 < h_1^0$ de liquide.
A $t=0$ la conduite $\cal C$ est ouverte et le liquide s'�coule entre les deux
r�servoirs.
L'objectif de ce probl�me est de mod�liser cet �coulement et plus pr�cis�ment de
d�river une pr�diction th�orique du temps de transvasement entre les deux r�servoirs.

\begin{figure}[htb]
  \begin{center}
    \setlength{\unitlength}{1mm}
    \begin{picture}(140, 99)(0, 3)
      \put(0, 0){\includegraphics[width=140mm]{transvasement.pdf}}
      \put(108, 23){$u \! \approx \! 0$}
      \put(127, 23){$u \! \approx \! 0$}
      \put(88, 38){$\myvec{g}$}
      \put(76.5, 99){$z$}
      \put(79, 15.5){$0$}
      \put(61, 18){$d$}
      \put(79, 81){$h_1(t)$}
      \put(68, 59){$h_2(t)$}
      \put(5, 85){${\cal R}_1$}
      \put(107, 77){${\cal R}_2$}
      \put(25, 83){$A$}
      \put(19, 11){$B$}
      \put(126, 11){$C$}
      \put(123, 61){$D$}
      \put(29, 69){$U_1$}
      \put(123, 71){$U_2$}
      \put(91, 5.5){$U$}
      \put(73, 0){\colorbox{white}{$L$}}
    \end{picture}
  \end{center}
  \mycaption{G�om�trie du dispositif de transvasement (N.B.~: les lignes de courant
    de l'�coulement sont trac�es qualitativement et
    n'ont qu'une valeur purement indicative.)}
  \label{fig:transvasement}
\end{figure}

On note $U$ la vitesse moyenne dans la conduite, $U_1$ et $U_2$
les vitesses de variation des hauteurs de liquide $h_1(t)$ et $h_2(t)$ 
dans les deux r�servoirs (fig.~\ref{fig:transvasement}).
$\delta_1$, $\delta_2$ et $\delta$ d�signent respectivement les pertes de charge
(c'est-�-dire les pertes d'�nergie m�canique par unit� de volume) dans les
r�servoirs  ${\cal R}_1$ et ${\cal R}_2$ et dans la conduite.
Pour mat�rialiser les diff�rentes composantes de ce dispositif, on note $A$
un point de la surface libre dans le premier r�servoir, $B$ un point de l'entr�e
de la conduite au fond du premier r�servoir, $C$ un point de la sortie 
de la conduite au fond du second r�servoir, et finalement $D$
un point de la surface libre dans le second r�servoir.
On n�glige les variations d'altitude $z$ de l'�coulement dans la conduite
entre les points $B$ et $C$ par rapport aux variations dans les deux r�servoirs~: 
on consid�re ainsi que l'�coulement dans la conduite se fait approximativement
� l'altitude $z=0$.

\textsl{On traitera l'ensemble du probl�me dans l'approximation d'un �coulement
quasi-stationnaire.}

\pagebreak

\begin{enumerate}
\item
Mod�lisation de l'�coulement dans le r�servoir ${\cal R}_1$, entre les points $A$ et $B$~:
  \begin{enumerate}
  \item
    Ecrire la relation liant $U_1$ et $U$.
  \item
    D�duire de la conservation de la masse dans le r�servoir ${\cal R}_1$
    une �quation pour $dh_1/dt$.
  \item
    L'�coulement dans le r�servoir converge vers l'entr�e de la conduite de mani�re
    quasi-potentielle et sans zone de
    recirculation majeure. On peut donc supposer en premi�re approximation que les pertes
    de charge dans ce r�servoir sont n�gligeables~: $\delta_1 \approx 0$.
    Utiliser la conservation de l'�nergie m�canique pour obtenir une relation liant $U$,
    $p_0$, $h_1$ et la pression $p_1$ � l'entr�e de la conduite.
    On utilisera l'hypoth�se $\varepsilon_1 \ll 1$ pour simplifier les calculs. 
  \end{enumerate}
\item
Mod�lisation de l'�coulement dans la conduite $\cal C$, entre les points $B$ et $C$~:
  \begin{enumerate}
  \item
    Quelles sont les sources possibles de pertes de charge dans cette partie du dispositif~?
    On pourra s'inspirer du sch�ma descriptif de la figure~\ref{fig:transvasement}
    pour r�pondre � cette question.
  \item
    On suppose dans la suite que les pertes de charge r�guli�res sont pr�pond�rantes.
    On a alors $\delta = 1/2 \; \rho U^2 \, (L/d) \; \lambda$ o� $\lambda$ d�signe le coefficient
    de pertes de charge r�guli�res.
    De quoi d�pend ce coefficient et quelle est sa dimension ?
  \item
    Donner la relation liant les pressions � l'entr�e $p_1$ et � la sortie $p_2$ de la conduite.
  \end{enumerate}
\item
Mod�lisation de l'�coulement dans le r�servoir ${\cal R}_2$, entre les points $C$ et $D$~:
  \begin{enumerate}
  \item
    Ecrire la relation liant $U_2$ et $U$.
  \item
    D�duire de la conservation de la masse dans le r�servoir ${\cal R}_2$
    une �quation pour $dh_2/dt$.
  \item
    L'�coulement �merge de la conduite sous la forme d'un jet entour� d'une zone de recirculation
    dans laquelle la vitesse de l'�coulement est quasi-nulle (fig.~\ref{fig:transvasement}).
    Cette variation brusque de section pour l'�coulement g�n�re une perte de charge singuli�re
    qui constitue la principale source de la chute d'�nergie m�canique 
    $\delta_2$ dans ce r�servoir.
    Ecrire dans ces conditions la relation liant $U$, $p_2$, $p_0$, $h_2$ et $\delta_2$. 
    On utilisera l'hypoth�se $\varepsilon_2 \ll 1$ pour simplifier les calculs.
  \item
    On souhaite estimer ces pertes de charge $\delta_2$.
    Ecrire le bilan int�gral de quantit� de
    mouvement dans le volume de contr�le d�fini par le volume occup� par le liquide dans le
    r�servoir ${\cal R}_2$ � l'instant $t$ dans l'approximation quasi-stationnaire
    (on n'oubliera pas l'apport de quantit� de mouvement par la pesanteur).
    On utilisera l'hypoth�se $\varepsilon_2 \ll 1$.
    Les frottements visqueux ayant principalement lieu dans le jet, on pourra les n�gliger
	au niveau de la fronti�re du domaine.    
  \item
    D�duire des relations obtenues dans les deux derni�res questions l'expression
    des pertes de charge $\delta_2$ en fonction de $\rho$ et $U$. 
    On utilisera une nouvelle fois l'hypoth�se $\varepsilon_2 \ll 1$.
  \item
    Montrer alors que la pression $p_2$ est �gale � la pression hydrostatique r�gnant sous
    une hauteur $h_2$ de liquide.
  \item
    Sans faire de calcul, justifier que ce dernier r�sultat reste valable dans le cas o� la zone de
    recirculation s'�tend jusqu'� la surface libre.
  \end{enumerate}
\item
Un premier bilan~: on supposera dans la suite du probl�me que les inconnues du probl�me sont 
reli�es en particulier par les �quations 
\begin{eqnarray}
p_0 + \rho g h_1 & = & p_1 + \frac{1}{2} \rho U^2 \\ 
p_1 & = & p_2 + \frac{1}{2} \rho U^2 \frac{L}{d} \, \lambda \\
p_2 & = & p_0 + \rho g h_2 
\end{eqnarray}
  \begin{enumerate}
  \item
    En d�duire une relation entre $U^2$ et $h_1-h_2$.
  \item
    D�crire l'arr�t du transvasement.
  \end{enumerate}
\item
  Cas d'un �coulement hydrauliquement rugueux dans la conduite $\cal C$~:
  on suppose que le nombre de Reynolds dans la conduite est suffisamment grand pour approximer 
  le coefficient de pertes de charge par une constante $\lambda \approx c$.
  On notera par la suite $\alpha = 1 + Lc/d$ 
  et $\bar{\varepsilon} = (\varepsilon_1+\varepsilon_2)/2$,
  \begin{enumerate}
  \item
    Pr�ciser la valeur de la vitesse initiale $U_0$ dans la conduite
    en fonction de $h_1^0$ et $h_2^0$ en particulier.
  \item
    D�river par rapport au temps la relation entre $U^2$ et $h_1-h_2$ obtenue
    pr�c�demment puis r�soudre cette �quation diff�rentielle pour en d�duire $U$
    en fonction de $t$.
  \item
    En d�duire le temps de transvasement $t_1$ en fonction de $\bar{\varepsilon}$,
    $h_1^0$, $h_2^0$, $\alpha$ et $g$.
  \item
    Donner l'expression de $h_1(t)$.
  \item
    En d�duire la hauteur de liquide finale $h_1^1$.
  \item
    Donner l'expression de $h_2(t)$.
  \item
    En d�duire la hauteur de liquide finale $h_2^1$.
  \end{enumerate}
\item
  Cas d'un �coulement laminaire dans la conduite $\cal C$~:
  on suppose que le nombre de Reynolds dans la conduite est suffisamment faible 
  pour que le coefficient de perte de charge soit donn� par son expression dans le cas
  laminaire $\lambda = 64/Re$ o� $Re$ d�signe le nombre de Reynolds dans la conduite.
  \begin{enumerate}
  \item
    Donner l'expression du nombre de Reynolds.
  \item
    Quel est l'�coulement dans la conduite dans le cas pr�sent ?
  \item
    Donner l'�quation diff�rentielle en temps v�rifi�e par $U(t)$.
    On notera $\beta = 64 L \nu / d^2$.
  \item
    R�soudre cette �quation diff�rentielle pour obtenir une relation entre $U$,
    $U_0$, $\beta$ et $\bar{\varepsilon}$.
  \item
    En d�duire que $U$ tend vers 0 lorsque $t$ tend vers l'infini.
  \item
    Donner l'expression du temps caract�ristique de d�croissance $t_2$ correspondant
    � un niveau de vitesse dans la conduite $U = \varepsilon U_0$ o� $\varepsilon \ll 1$
    est un param�tre arbitraire fix�.
  \item
    Exprimer le rapport $t_2/t_1$ en fonction de $\varepsilon$, $c$ et $Re$.
  \end{enumerate}
\item
Faire une synth�se critique des r�sultats obtenus.
En particulier, quel am�lioration pourrait-on apporter � la mod�lisation pour se rapprocher
de la r�alit� physique de ce proc�d�~?
Quelle condition doivent v�rifier les param�tres g�om�triques du probl�me pour que
l'approximation quasi-stationnaire soit valide ?
\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

