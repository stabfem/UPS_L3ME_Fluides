%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[10pt, a4paper]{article}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%--------------------------------------------------------------------------------------------------
% Dimensions :
%--------------------------------------------------------------------------------------------------

\setlength{\textheight}{25cm}
\setlength{\textwidth}{16cm}

\setlength{\topmargin}{-20mm}
\setlength{\oddsidemargin}{0mm}
\setlength{\evensidemargin}{0mm}

% \setlength{\columnsep}{20mm}

\setlength{\fboxsep}{1mm}
\setlength{\unitlength}{1mm}

%--------------------------------------------------------------------------------------------------
% Packages :
%--------------------------------------------------------------------------------------------------

\usepackage{latexsym}
\usepackage{graphicx}
\usepackage{pifont}
\usepackage{color}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{enumerate}

\usepackage[french]{babel}    % pour franciser le document

\usepackage[latin1]{inputenc} % pour utiliser les caracteres accentues du claviers

\usepackage{accents}

\input{mymacros.tex}

%--------------------------------------------------------------------------------------------------
% Divers :
%--------------------------------------------------------------------------------------------------

\definecolor{rougefonce}{rgb}{0.7, 0.2, 0.2}

\renewcommand{\thickline}[2]{\linethickness{#1} \line(1, 0){#2}}
\renewcommand{\mycaption}[1]{\caption{\sl #1}}
\renewcommand{\myvec}[1]{\vec{#1}}

\newcommand{\footnoteremember}[2]{
\footnote{#2}
\newcounter{#1}
\setcounter{#1}{\value{footnote}}% \!\!\!
}
\newcommand{\footnoterecall}[1]{
\footnotemark[\value{#1}]
}

% \pagestyle{empty}

\graphicspath{{Figures/}} % chemin d'acces au repertoire des figures (par ex.)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{center}

  \textsc{Universit� Toulouse 3 -- Paul Sabatier \hfill Ann�e universitaire 2022-2023}
  
  \textsc{M�canique des fluides \hfill L3 M�canique}
  
  \vspace{3mm}
  
  \begin{center}
    \thickline{0.4mm}{160}
    \\ \vspace{3mm}
  \textbf{\large Exercice compl�mentaire 9 : force exerc�e sur une conduite}
    \\ %\vspace{1mm}
    \thickline{0.4mm}{160}
  \end{center}

  \vspace{5mm}
  
\end{center}

\bigskip

\noindent
On consid�re l'�coulement incompressible d'un fluide newtonien homog�ne 
de masse volumique $\rho$ dans une conduite de section circulaire de rayon $R$ 
et de grande longueur $L$ (fig.~\ref{fig:conduite}).
Le fluide dans la conduite est mis en �coulement sous l'action d'une diff�rence 
de pression entre l'entr�e et la sortie de la conduite.
Sans perte de g�n�ralit� on supposera $p_1>p_2$.
On n�gligera les forces massiques (dont la pesanteur).
On notera $\nu$ et $\mu = \rho \nu$ les viscosit�s cin�matique et dynamique.
L'objectif de cet exercice est de d�terminer de diff�rentes fa�ons la force exerc�e
par un �coulement laminaire ou turbulent sur une conduite rectiligne ou coud�e.
On notera $Re=UD/\nu$ le nombre de Reynolds de l'�coulement dans la conduite, o�
$U$ d�signe la vitesse moyenne et $D$ le diam�tre.

\begin{figure}[hbt]
  \begin{center}
    \begin{picture}(160, 25)(0, 0)
      \put(0, 0){\includegraphics[width=160mm]{conduite.pdf}}
      \put(152, 10){\footnotesize $\alpha$}
      \put(24, 0.5){\footnotesize $2R$}
      \put(86, 6){\footnotesize $x$}
      \put(71.5, 17){\footnotesize $y$}
      \put(30, 10){\footnotesize $L\gg R$}
      \put(117, 10){\footnotesize $L\gg R$}
      \put(0, 6){\footnotesize$U$}
      \put(0, 2){\footnotesize$p_1$}
      \put(71, 2){\footnotesize$p_2$}
      \put(91, 6){\footnotesize$U$}
      \put(91, 2){\footnotesize$p_1$}
      \put(158, 20){\footnotesize$p_2$}
      \put(30, 20){(a)}
      \put(110, 20){(b)}
    \end{picture}
  \end{center}
  \mycaption{Ecoulement en conduite rectiligne (a) ou coud�e (b).}
  \label{fig:conduite}
\end{figure}

%==================================================================================================
\section{Conduite rectiligne}
%==================================================================================================

\noindent
On s'int�resse dans cette partie au cas d'une conduite rectiligne (fig.~\ref{fig:conduite}a).

\medskip

\noindent
En r�gime \textbf{laminaire}, soit $Re<2000$ environ, l'�coulement dans la conduite est exactement 
d�crit par la solution de Poiseuille donn�e par le profil parabolique (cf. exercice compl�mentaire 4) :
\[
	u(r) 
	= - \dfrac{R^2}{4\mu} \, \ddx{p} \left [ 1 - \left (  \dfrac{r}{R} \right)^2 \right ]
	= 2U \left [ 1 - \left (  \dfrac{r}{R} \right)^2 \right ]
\] 
o� $U = - \dfrac{R^2}{8\mu} \, \ddx{p}$
d�signe la vitesse moyenne dans ce cas.

\medskip

\begin{myenumerate}
\item \textbf{D�termination directe}
\begin{enumerate}[a)]
\item 
  	En vous basant sur les r�sutats obtenus dans l'exercice compl�mentaire 4,
  	d�terminer l'expression du champ de pression $p$ dans la conduite. 
\item
	D�terminer les composantes\footnote{%
	L'expression du tenseur des contraintes en cylindriques 
	est disponible en annexe du fascicule de TD (partie 1).} 
	du tenseur des contraintes visqueuses.
\item
	D�terminer l'expression de la force �l�mentaire $d\myvec{F}$ exerc�e par l'�coulement
	sur la surface infinit�simale $dS = Rd\theta dx$ de la paroi.
\item
	En d�duire la force totale exerc�e par l'�coulement sur la portion de conduite de longueur $L$.
	On pourra exprimer cette force en fonction de la diff�rence de pression $p_1-p_2$
	ou en fonction de la vitesse moyenne $U$. On notera $S$ la section de la conduite.
\end{enumerate}

\item \textbf{D�termination indirecte}
\item[]
	Retrouver l'expression de la force pr�c�dente en passant par un bilan int�gral 
	de quantit� de mouvement pour le volume de contr�le d�limit� par les sections d'entr�e $S_1$
	et de sortie $S_2$ et la paroi $S_p$ de la conduite de longueur $L$.
\end{myenumerate}

\pagebreak

\noindent
Passons maintenant au r�gime \textbf{turbulent}, pour $Re>4000$ environ : dans ce cas, on ne sait pas
�crire analytiquement la solution correspondant � l'�coulement observ�.
On peut n�anmoins utiliser la mod�lisation pr�sent�e au chapitre 8, qui approxime l'�coulement
turbulent (3D et instationnaire) par sa moyenne quasi stationnaire et uniforme dans chaque section.
On ne conna�t pas le d�tail de l'�coulement entre les sections d'entr�e et de sortie, aussi
il faut proc�der par d�termination indirecte.

\begin{myenumerate}
\item 
	En n�gligeant les frottements visqueux dans les sections d'entr�e et de sortie, de rayon $R$,
	par rapport aux frottements qui s'exercent le long de la paroi de longueur $L\gg R$, 
	montrer que la force exerc�e par l'�coulement sur la conduite a la m�me expression 
	qu'en r�gime laminaire.
\end{myenumerate}

%==================================================================================================
\section{Conduite coud�e}
%==================================================================================================

\noindent
On s'int�resse dans cette partie au cas d'une conduite coud�e conduisant � une r�orientation
de l'�coulement d'un angle $\alpha$ (fig.~\ref{fig:conduite}b).

\medskip

\noindent
En r�gime \textbf{laminaire}, on supposera que l'�coulement en entr�e est  
d�crit par la solution de Poiseuille.
Apr�s r�orientation dans le coude, et si la section de sortie est suffisamment loin du coude,
on retrouve l'�coulement de Poiseuille en sortie.
On ne conna�t donc pas l'expression du champ de vitesse et de pression tout le long de la conduite :
il faut donc passer par une d�termination indirecte.

\begin{myenumerate}
\item 
	En n�gligeant les frottements visqueux dans les sections d'entr�e et de sortie
	par rapport aux frottements qui s'exercent le long de la paroi, 
	d�terminer la force exerc�e par l'�coulement sur la conduite, en fonction de l'angle $\alpha$
	et de la vitesse moyenne $U$.
	On portera une attention particuli�re au calcul des termes de flux de quantit� de mouvement
	en entr�e et en sortie.
	On pourra noter $\myvec{e}_x{\!}^\prime$ le vecteur unitaire sortant de la section de sortie.
\end{myenumerate}

\begin{figure}[hbt]
  \begin{center}
    \begin{picture}(60, 50)(0, 0)
      \put(0, 0){\includegraphics[width=60mm]{turbulence_coude.png}}
    \end{picture}
  \end{center}
  \mycaption{Simulation num�rique de l'�coulement � grand nombre de Reynolds
  	au niveau d'un coude �~90� (calculs effectu�s sous Comsol).}
  \label{fig:turbulence_coude}
\end{figure}

\noindent
En r�gime \textbf{turbulent}, l'�coulement au niveau du coude peut �tre extr�mement complexe,
m�me pour l'�coulement moyen, avec g�n�ration de zones de d�collement (cf. fig.~\ref{fig:turbulence_coude}).
En supposant que la section de sortie est suffisamment �loign�e du coude, il est raisonnable
de supposer que l'on retrouve un �coulement turbulent �tabli comme en entr�e, avec un �coulement moyen 
quasi stationnaire et uniforme dans la section.

\begin{myenumerate}
\item 
	Toujours en n�gligeant les frottements visqueux dans les sections d'entr�e et de sortie
	par rapport aux frottements qui s'exercent le long de la paroi, 
	d�terminer la force exerc�e par l'�coulement sur la conduite, en fonction de l'angle $\alpha$
	et de la vitesse moyenne $U$.
	Comparer au cas laminaire.
\end{myenumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

