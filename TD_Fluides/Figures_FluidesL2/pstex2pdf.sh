#!/bin/sh
for psfile in `ls -1 *.pstex`
do
  new="$(basename $psfile .pstex).pdf"
  convert $psfile $new
done