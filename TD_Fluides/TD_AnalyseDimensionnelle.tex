% !TEX root = TD_fluides_part1.tex


%==================================================================================================
\section{Analyse dimensionnelle et similitude}
%==================================================================================================


\setcounter{subsection}{-1}

\subsection{Force de traînée sur une automobile}

{\em (Exercice préparatoire ; correction sur le site web du cours)}

On cherche à estimer la force de traînée $D$ s'exerçant sur une automobile de longueur $L$ et surface frontale $S$ roulant à la vitesse $V$ dans de l'air, à l'aide d'une expérience utilisant une maquette à échelle réduite dans un tunnel hydrodynamique.

\begin{enumerate}
\item Listez et discutez les paramètres physiques pertinents et indépendants dans ce problème.  

\rep{\em Réponse : 

On gardera comme paramètres la longueur $L$, la vitesse $V$, la masse volumique $\rho$ et la viscosité dynamique $\mu$ du fuide.

\begin{itemize}

\item $g$ n'est pas pertinent (contribue (faiblement) à la force verticale via la poussée d'Archimède mais pas à la force horizontale), 

\item $S$ non plus (proportionnel à $L^2$ pour une géométrie de voiture donnée).


\item La pression de référence (pression atmosphérique $p_{atm}$) non plus. C'est justifié lorsque le nombre de Mach est faible. C'est toujours le cas pour une automobile, la vitesse max étant de l'ordre de $140 km/h = 38.9 m/s$, le nombre de Mach vaut au maximum $M = 0.11$ compte tenu de la vitesse du son $c_0 = 340 m/s$.

Ce dernier argument est le plus difficile à justifier rigoureusement ; il a été énoncé sans démonstration au chapitre 1, et sera réexpliqué au chapitre 7 du cours.

\item Enfin, la température $T$ est un paramètre pertinent mais non indépendant (la masse volumique étant elle-même fonction de $T$).

\end{itemize}


 Donc $ D= { \cal F} ( V, L, \rho, \mu)$.
  
}

\item En utilisant les principes de l'analyse dimensionnelle, montrez que la traînée s'exprime simplement sous la forme suivante :

$$ 
D = \frac{1}{2}\rho S V^2 C_x( Re),  \quad \mbox{ avec } Re = \frac{\rho V L}{\mu}
$$

\rep{\em Réponse : 

On applique la démarche du cours :

$$\frac{D}{ M^* L^* {T^*}^{-2}} = \bar{\cal F}\left(\frac{V}{L^* {T^*}^{-1}},\frac{L}{L^*},\frac{\mu}{M^* {L^*}^{-1}  {T^*}^{-1}}\right).$$

Le choix des unités intrinsèques au problème le plus simple est $L^*=L$, $T^* = L/V$, $M^* = \rho L^3$. Ceci conduit à

$$D/(\rho L^2 V^2) = \bar{\cal F}(1,1,1,\frac{\mu}{\rho V L}).$$

La trainée adimensionnelle $D/(\rho L^2 V^2)$ est donc une fonction uniquement du paramètre $\frac{\mu}{\rho V L}$, et donc de son inverse dans lequel on reconnait le nombre de Reynolds. On réécrit donc cette dépendance sous la forme:

$$
D/(\rho L^2 V^2) = f(Re)  \quad \mbox{ avec } Re = \frac{\rho V L}{\mu}
$$

Par convention on préfère utiliser la surface de référence $S$. Celle-ci est proportionnelle à $L^2$ pour une géométrie donnée, c'est-à-dire que $S = \alpha L^2$ où $\alpha$ est un coefficient géométrique sans dimensions. On peut introduire une fonction $C_x(Re)$ définie simplement  par  $C_x(Re) =  2 f(Re)/\alpha$ ce qui conduit au résultat sous la forme recherchée. 

}


\item Calculez la valeur du nombre de Reynolds, pour une automobile de longueur $L = 4m$ (et surface frontale $S = 1.74 m^2$), roulant à la vitesse $V= 90 km/h$ dans de l'air (de propriétés $\rho_a$ $\mu_a$ données dans le formulaire).

\rep{\em Réponse : 

En unités internationales la vitesse vaut 
$V = 90 km/h = (90 \times 10^3 m) / (60\times 60 s) = (90/3.6) m/s = 25 m/s$.

%La valeur de $\nu$ pour l'air est donnée dans le formulaire : $\nu = 1.5 \times 10^{-5} m^2/s$.

Les propriétés de l'air sont données dans le formulaire : $\rho_a = 1.205 kg/m^3$, $\mu_a = 1.81 \cdot 10^{-5} Pa \cdot s$.

Le nombre de Reynolds vaut donc 

$$Re = 25 \times 4 / (1.5 \times 10^{-5}) =  {\color{red} 6.66 \cdot 10^6}.$$
}

\item Dans l'expérience, la maquette est à l'échelle 1/10 (c.a.d. $L_m = L/10$). La vitesse dans le tunnel hydrodynamique est notée $V_m$.  Le fluide est de l'eau (de propriétés $\rho_m$ $\mu_m$ données dans le formulaire).

Quelle doit être la valeur de $V_m$ pour assurer la similitude de Reynolds ?

\rep{\em Réponse : 

Si l'on veut être en similitude de Reynolds il faut que $Re_m = \rho_m V_m L_m / \mu_m = Re = 6.67 \times 10^6$.

%La valeur de $\nu_m$ est celle de l'eau donnée dans le formulaire : $\nu_m = 1 \times 10^{-6} m^2/s$.
Les propriétés de l'eau sont données dans le formulaire : $\rho_m \approx 10^3 kg/m^3$;
 $\mu_m \approx  10^{-3} Pa \cdot s$.

La longueur de la maquette est $L_m = L/10 = 0.4 m$. Donc :

$$V_m = Re_m \times \mu_m / (\rho_m L_m) = (6.67 \times 10^6) \times 10^{-3} / (0.4 \times 10^3) =  {\color{red} 16.7 m/s}$$
}

\item 
Le tunnel hydrodynamique étant réglé à la vitesse $V_m$ précédemment déterminée, 
on mesure à l'aide d'une balance de forces une traînée sur la maquette $D_m = 911 N$.
En déduire le $C_x$, puis la traînée $D$ sur la voiture.

\rep{\em Réponse : 

La surface de la maquette vaut $S_m = S/100 = 0.0174m^2$ ; en effet les longueurs sont diminuées d'un facteur 10, les surfaces d'un facteur $10^2 = 100$. Pour la maquette on a $D_m = (1/2) \rho_m S_m V_m^2 C_{x,m} = C_x(Re_m)$. 
Le coefficient de traînée de la maquette vaut donc :

$$ C_x(Re_m) = (2 D_m)/(\rho_m S_m V_m^2) = (2 \times 911)/(1000 \times 0.0174 \times 16.6^2) =  0.38$$

Puisque le coefficient de traînée ne dépend que du nombre de Reynolds, il est le même pour la maquette et la voiture réelle : $C_x(Re) = C_x(Re_m)$. C'est le principe même de la similitude !

On peut donc déduire la traînée exercée sur la voiture : 
 
 $$D = (1/2) \rho S V^2 C_x(Re) = .5 \times 1.2 \times1.67 \times 25^2 \times 0.38 =  {\color{red} 245 N}$$


}

\item En déduire la puissance dissipée par les forces aérodynamiques, puis la puissance qui doit être fournie par le moteur, en supposant que le rendement énergétique global de celui-ci est $\eta = { 20 \%}$.

\rep{\em Réponse : 

La puissance des forces aérodynamiques vaut 
$$  { \cal P}_{aero} = D \times V =  {\color{red} 6.12 kW}$$

Le rendement du moteur peut être défini par $\eta =  { \cal P}_{aero} /  { \cal P}_{moteur}$. 

Avec les données du problème on a une puissance fournie par le moteur : 

$$ { \cal P}_{moteur} = {\color{red} 30.6 kW = 41 hp}$$

($1 hp$ = 1 cheval-vapeur = $746 W$; unité différente du cheval-fiscal apparaissant sur la carte grise !)
}

\end{enumerate}



\subsection{Force de résistance à l'avancement d'un bateau de pêche}

%\section{Résistance à l'avancement d'un pétrolier}

On cherche à déterminer la résistance à l'avancement d'un petit bateau de pêche naviguant dans de l'eau de mer à la vitesse nominale  $U = 4 m/s$.
La longueur du navire est de $L = 10 m$, et sa surface mouillée est de $S = 37,64 m^2$. 
La masse volumique $\rho$, la viscosité dynamique $\mu$ et la viscosité cinématique $\nu = \mu/\rho$ de l'eau de mer sont données dans le formulaire.


%La masse volumique de l'eau vaut $\rho = 1026 kg/m^3$ pour le navire. La viscosité cinématique de l'eau est $\nu = 1.188 10^{-6} m /s^2$.

%\subsubsection{Analyse dimensionnelle}
%{\bf A. Analyse dimensionnelle}
\,

\begin{enumerate}

%\subsubsection{Analyse dimensionnelle}

\item[] {\bf A. Analyse dimensionnelle}

\item Listez tous les paramètres physiques pertinents et indépendants ayant une influence sur la force de résistance.

\rep{ Liste des paramètres retenus : $U,L,\rho,g,\nu$ (ou $\mu$),

Paramètres non retenus :
\begin{itemize}
\item $P_0$ (pression de référence "atmosphérique). Justifications possibles : 
$(i)$ expérimentale ; on constate que la traînée ne varie pas en fonction des conditions météo. 
$(ii)$ La force due à la pression est une intégrale de $P$ sur une surface fermée, si on modifie la pression de référence (on remplace $P$ par $P+ \delta_P$ partout) 
  l'intégrale correspondante n'est pas modifiée  (cf. chap. 2 et souvenirs de L2 normalement).
$(iii)$  les équations du mouvement du fluide incompressible (NS) sont invariantes lorsque l'on change $P_0$ en $P_0'$ (cf. chap. 5).

\item $T_0$ : les transferts thermiques ne sont pas pertinents dans ce problème (et de + $T_0$ n'est pas indépendant car pour un fluide incompressible $\rho = \rho(T)$)

\item $\rho_a,\mu_a$ (densité de l'air ; car 1000 fois plus faible que celle de l'eau, la résistance exercée par l'air est donc négligeable)

\item Si on retient $\nu$, $\mu$ n'est pas indépendant (ou inversement).

\item Largeur $\ell$, hauteur $H$, autres dimensions, etc... car si l'on considère une géométrie de bateau donnée, celles-ci sont toutes proportionnelles à la longueur $L$.
(en revanche si l'on considère une famille de géométries dont on souhaite faire varier les caractéristiques, il pourra être pertinent de rajouter des paramètres géométriques comme $\ell/L$ , etc... , mais ici dans la suite on utilisera une maquette géométriquement similaire )

\item  De même la surface mouillée $S$ n'est pas indépendante car $S  \equiv L^2$, le volume non plus.

\item La masse du bateau n'est pas pertinente (elle intervient dans le bilan des forces verticales, mais n'est pas pertinente dans la force horizontale) 

\item De même pour toutes les autres propriétés du bateau (répartition de masse à l'intérieur de celui-ci, etc...) 

\item etc... (l'age du capitaine n'est pas pertinent non plus !)

\end{itemize}

NB les valeurs de $\rho,\nu$,etc... sont données dans le tableau "annexe A" du formulaire.

}



\item En déduire que la résistance à l'avancement peut se mettre sous la forme suivante :

$$ 
R_T = \frac{1}{2} \rho S U^2 \cdot  C_T \left( \frac{U L}{\nu} , \frac{U}{\sqrt{gL}} \right) 
$$  

\rep{
Méthode : on part de la relation $R_T = {\cal F}(U,L,\rho,g,\nu)$. On liste les dimensions physiques des variables retenues.
On met sous forme adimensionnelle sous la forme 
$$
\frac{R_T}{ {M^*} {L^*} {T^*}^{-2} }  = \overline{{\cal F}} \left( \frac{U}{{L^*} {T^*}^{-1}},\frac{L}{{L^*}},\frac{\rho}{{M^*} {L^*}^{-3}},\frac{g}{{L^*} {T^*}^{-2}},\frac{\nu}{{L^*}^2 {T^*}^{-1}} \right)
$$

Le choix le plus judicieux est ${L^*} = L$, ${M^*} = \rho L^3$, ${T^*} = L/U$. On arrive alors à : 
$$
\frac{R_T}{ \rho L^2 U^2 }  = \overline{{\cal F}}\left(1,1,1,\frac{gL }{U^2},\frac{\nu}{L U} \right) 
$$
ce qui est équivalent à la forme recherchée (NB par convention on préfère faire apparaître $S$ plutôt que $L^2$ , c'est équivalent dimensionnellement mais plus pratique à l'usage) .
}




\item Quels nombres adimensionnels classiques reconnaissez-vous là ? quelle est leur interprétation ?

\rep{
On reconnaît les nombres de Froude et Reynolds (s'aider du tableau "annexe B" du formulaire).

Inteprétations : $Re$ = Effets inertiels / Effets visqueux, $Fr$ = effets inertiels /effets gravitationnels). NB : ici les effets gravitationnels se manifestent par la création de vagues. Autre interprétation : $Fr$ =  vitesse du bateau / vitesse des vagues. Analogie avec le nombre de Mach (mais en réalité c'est plus compliqué car les vagues sont des ondes dispersives...).
}   


\item Calculez la valeur de ces deux nombres dans le cas du bateau de pêche en condition réelles.

\rep{  $Fr = 0.4$, $Re = 3.0326 \cdot 10^7$.}  

\,

\item[]{\bf B. Etude de similitude}


On cherche à déterminer la traînée à l'aide d'une expérience en bassin de traction (dans l'eau douce), à l'aide d'une maquette à l'échelle 1/10ème.

\item Est-il possible de faire une expérience en similitude totale (nombres de Froude et de Reynolds identiques dans l'expérience et le navire réel) ? 

\rep{ Similitude de Reynolds $=>$ $U_M/U = L/L_M$ ; similitude de Froude (en gravité terrestre, $g=g_M$) $=>$ $U_M /U = (L/L_M)^{-1/2}$, impossible d'assurer les deux a moins de faire l'expérience  sur une autre planète !}

\item Quelle doit être la vitesse $U_m$ de la maquette si l'on souhaite respecter la similitude de Reynolds ? Que vaut alors le nombre de Froude de l'expérience ? ce cas vous semble-t-il réaliste ?

	\rep{ $U_m = 40m/s$, $Fr_m = 12.77$, la structure du champ de vagues va être très différente...}


\item Quelle doit être la vitesse $U_m$ de la maquette si l'on souhaite respecter la similitude de Froude ? Que vaut alors le nombre de Reynolds $Re_m$ de l'expérience ?

\rep{ $U_m = 1.26 m/s$ ; $Re_m = 1.2624e6$. } 

\, 
\item[]{\bf C. Similitude partielle sous l'hypothèse de Froude} 



 Lorsque $Re \gg 10^{5}$ et $Fr \le 0.4$, l'expérience montre que l'on peut faire l'hypothèse de Froude, qui consiste à supposer que la  résistance à l'avancement se décompose en deux parties données par les expressions suivantes :

$$
C_T(Re, Fr) = C_v (Re) + C_{w}(Fr) 
$$ 

où $C_v$ est le coefficient de traînée visqueuse, estimé par la loi empirique suivante \footnote{ Cette loi correspond à la formule de Hugues modifiée en considérant un coefficient de forme $\kappa = 0.104$; voir chapitre 6, formulaire section H  et TD 6.4.} :
$$
C_v(Re) = \frac{0.074}{\left( \log_{10} Re - 2 \right)^2}
$$

et $C_{w}(Fr)$ est le coefficient de traînée de vagues, qui peut être mesuré en effectuant une expérience en similitude de Froude.

\item  Pour une vitesse d'avance de la maquette correspondant à celle calculée à la question 7, on mesure sur la maquette une résistance à l'avancement 
$R_{T,m}  = 2.228 N$.

En déduire la traînée totale exercée sur le navire réel $C_T$, puis la puissance qui doit être fournie par le moteur du bateau.

\rep{Rep : le coefficient de traînée de la maquette vaut 

$C_{T,m} = \frac{ R_{T,m}}{\rho_m S_m U_m^2/2} = 0.0074 $. 

La partie visqueuse donnée par la loi empirique vaut $C_{v}(Re_m) = 0.0044$.

En retranchant les deux on déduit $C_w =  0.003$, qui est le même que pour le navire réel.
 
Donc le coefficient de traînée du navire réel vaut $C_T = C_w + C_v(Re) = 0.003 + 0.0025 = 0.0055$, et la traînée totale vaut 
$R_T = 1701 N$.
La puissance correspondante vaut ${\cal P} = R_T U = 6803 W= 9.13$ chevaux-vapeur. 
}


\end{enumerate}

 
%{\em Remarque : je ne donne pas les valeurs de la masse volumique et viscosité de l'air et de l'eau, c'est intentionnel, normalement ils devraient les connaitre...}



%\subsection{Traînée d'un bateau}


\subsection{Vitesse d'un animal volant}

On cherche à estimer la vitesse $V$ d'un animal volant (insecte, oiseau ou chauve-souris) en fonction de sa masse $M$.


%\subsubsection{Analyse dimensionnelle}


\begin{enumerate}

\item 

Listez les différents paramètres physiques intervenant dans le problème, et discutez leur pertinence. Montrez qu'il est légitime de supposer que la vitesse est donnée par une loi de la forme 

$$
V = {\cal F}(M,L,\rho,g).
$$

où $\rho$ est la masse volumique de l'air, $g$ l'accélération de la gravité,
$L$ l'envergure de l'oiseau, et $M$ sa masse.

% et $\nu$ la viscosité cinématique de l'air.

On admettra (jusqu'à la question 9) que la viscosité de l'air $\nu$ n'est pas un paramètre pertinent.



%\item Commentez l'hypothèse faite ci-dessus ; quels paramètres physiques 
%n'ont pas été pris en compte et comment justifiez-vous leur omission ?

\item Donnez les dimensions physiques de $V$, $\rho$, $g$, $L$ et $M$.

\item En appliquant les principes de l'analyse dimensionnelle, montrez
que la loi de vitesse peut se mettre sous la forme :

$$
V = \sqrt{gL} \,\, {\cal F} \left(\frac{M}{\rho L^3} \right).
$$

 \item 
Calculez le paramètre $\frac{M}{\rho L^3}$ pour les différents animaux détaillés dans le tableau ci-dessous.
Justifiez pourquoi ce paramètre varie peu d'un animal à un autre.

\begin{center}
\begin{tabular}{|l|l|l|l|l|l|l|}
\hline 
Animal  & Poids & Envergure & Vitesse & $M/(\rho L^3)$ &  $V/M^{1/6}$ & Re \quad \quad \\
\hline 
Guêpe & $100 mg$ & $20mm$ & $20 km/h$ & & &\\
Colibri & $2.5 g$ & $10cm$ & $45 km/h$ & & & \\
Hirondelle & $17g$ & $28 cm$  &  $60 km/h$ & & &\\
Aigle & $6kg$ & $2m$ & $160 km/h$ & & &\\ 
Airbus A300  & $150 T$ & $ 60m$ & $855 km/h$ & & &\\
\hline
\end{tabular}
\end{center}

\item En déduire que  la vitesse d'un animal volant est proportionnel à
$M^{1/6}$.

\item Application : calculez le rapport $V/M^{1/6}$ pour les différents animaux
détaillés dans le tableau ci-dessus. Commentez les résultats.

\item En se basant sur l'analyse précédente, à combien estimez vous la vitesse d'une mouette de masse $400g$ ?

\item Calculez le nombre de Reynolds pour les différents animaux du tableau ci-dessus.

\item On justifiera plus tard (cf. chapitre 9 et programme de M1) que lorsque $Re \gg 10^4$, les forces exercées sur un objet "profilé" ne dépendent plus, en première approximation,  de la viscosité du fluide. Justifiez l'hypothèse faite à la question 1.




\end{enumerate}


\rep{
\begin{center}
\begin{tabular}{|l|l|l|l|l|l|l|}
\hline 
Animal  & Poids & Envergure & Vitesse & $M/(\rho L^3)$ &  $V/M^{1/6}$ & Re \quad \quad \\
\hline 
Guêpe & $100 mg$ & $45mm$ & $20 km/h$ 		& 0.91 & 25.6 & 1.6e4     \\
Colibri & $2.5 g$ & $15cm$ & $45 km/h$ 			& 0.61 & 33.9 & 1.2e5     \\
Hirondelle & $17g$ & $28 cm$  &  $60 km/h$ 		& 0.64 & 32.8 & 3.1e5     \\
Aigle & $6kg$ & $2m$ & $160 km/h$ 			& 0.62 & 32.9 & 5.9e6     \\ 
Airbus A300  & $150 T$ & $ 60m$ & $855 km/h$ 	& 0.57 & 32.6 & 9.5e8     \\
\hline
\end{tabular}
\end{center}


}






\comment{
\subsection{Avion et maquette$^*$}


\subsubsection{Avion en vol}

On considère un avion de transport, d'envergure 40 $m$, de longueur 45 $m$, 
de surface de voilure de 260 $m^2$. On suppose qu'il vole à 10 800 $m$ d'altitude à un nombre
de Mach $M_1 = 0.75$.  

Pour l'air on prend les caract\'eristiques suivantes :
  $r = 287.04$ et $\gamma= 1.4$ et 
  
\begin{center}
\begin{tabular}{c |cccc}
  $H$ en m     &  $T$ en   $^\circ K$    & $P$ en $kPa$   &  $\nu$ en $m^2/s$
  \\ \hline  
 10 800       & 218.08    & 23.422     &    $3.8202 \ 10^{-5}$\\
 1 800       & 276.46    & 81.494     &     $1.6869 \ 10^{-5}$
 \end{tabular}
\end{center} 
 

 \begin{enumerate}
 \item Calculer la masse volumique de l'air $\rho_1$, la vitesse du son $a_1$, 
 en déduire la vitesse de l'avion.
 \item Calculer la corde moyenne de l'aile $\ell_1$ et en déduire
 le nombre de Reynolds correspondant. Calculer le nombre de Reynolds $Re_ {L1}$
 basé sur la longueur de l'avion.
 \item Donner l'ordre de grandeur de l'épaisseur de la couche limite sur l'aile 
 à une distance $x_1= 4 \ m$ du  bord d'attaque. 
 
 \item Reprendre les questions précédentes à l'altitude de  1 800 $m$, pour le
 nombre de Mach de $M_1=0.4$.
 
 \item Conclusion
 \end{enumerate} 
 
 
 
\subsubsection{   Maquette de l'avion  au 1/10 ème}
 
   \begin{enumerate}
   \item Donner les grandeurs suivantes $\ell_2$, $L_2$, $S_2$.
    On teste la maquette dans une soufflerie reproduisant une atmosphère
   à $H=10800 \ \  m$. 
   
    \item On cherche une similitude sur le nombre de Reynolds, en déduire la vitesse
   que doit avoir l'air dans la soufflerie. Quelle est alors la valeur du nombre
   de Mach ? En tirer des conséquences sur l'aérodynamique.
   
   \item  On cherche une similitude sur le nombre de Mach, qu'elle est alors
   la valeur du nombre de Reynolds ? En tirer des conséquences sur l'aérodynamique.
   \end{enumerate}
 
 
 \comment{
 \subsubsection {   Force de portance}
 Dans les 2 cas précédents,  en déduire la valeur
 de la force de portance $L$, en sachant que le $C_L$ considéré est de $0.175$.
 Conclusions. 
 }


\subsection{Etude en similitude d'une aile de libellule}


\begin{enumerate}

\item On cherche à estimer la force de portance exercés sur une aile de libellule, de longueur $L = 5cm$ et surface $S = 5 cm^2$, se déplaçant dans l'air 
($\rho_a = 1.225 kg/m^3, \nu = 1.5 \cdot 10^{-5} m^2/s$) 
à la vitesse $U = 4m/s$. 

Une expérience avec une maquette à échelle augmentée d'un facteur $4$ (longueur de l'aile $L_m = 20cm$)  
en soufflerie hydraulique ($\rho_e = 1000 kg/m^3, \nu = 10^{-6} m^2/s$)  
effectuée {\em en similitude de Reynolds} a permis de mesurer une portance $L_m=  2.27\cdot 10^{-2} N$. 


%{\em NB : Les propriétés physiques de l'eau et de l'air sont données dans le formulaire.}

\item Calculez le nombre de Reynolds caractérisant le vol de la libellule. Que peut-on en déduire sur le régime d'écoulement ?

\rep{ $Re = 1333$. on est en régime dominé par l'inertie mais ou la viscosité reste importante. L'écoulement reste laminaire.}



\item Donnez la vitesse $U_M$ dans la soufflerie hydraulique pour être en similitude de Reynolds. 

\rep{ $U_m = 6.67  cm/s$}


\item 
En déduire la portance $L$ exercée sur l'aile de la libellule. L'ordre de grandeur ce cette force vous semble-t-elle compatible avec le vol de l'insecte ?

\rep{ On a égalité des coefficients de portance : $C_y = L/( 1/2 \rho_a S U^2) = L_m/( 1/2 \rho_e S_M U_M^2)$. La surface de la maquette est $S_m = 4^2 \cdot S$. On trouve $C_x = 1.275$ et donc $L = 0.0063 N$.

Cette portance permet de porter un poids d'environ $0.6g$. C'est compatible avec le poids de l'insecte qui est de quelques grammes.
}

\end{enumerate}
}
%
%\subsection{Analyse de similitude d'un moteur d'avion à hélice$^*$}
%
%
%On cherche des relations simples, fonctions de nombres sans dimension
%caractéristiques, qui donnent la force de traction $T$ de l'hélice, 
%le couple $Q$ exerçé par l'hélice sur l'arbre moteur
%et la puissance résistante $P_h$ de l'air sur l'hélice. 
%
%
%\begin{enumerate}
%
%\item
%Donner les dimensions et les unités de la force $T$, du diamètre de
%l'hélice $D$, de la fréquence de rotation $n$,
%de la masse volumique $\rho$, de la viscosité cinématique $\nu$, de la vitesse
%de l'avion $V_0$, et de la pression $p$
%
%
%\item En utilisant l'analyse dimensionnelle, montrer que la force de traction
%peut être mise sous la forme suivante :
%
%
%$$
%T =  \rho n^2 D^4 k_T(Re,M,J) ,
%$$
%
%où $Re = V_0 D / \nu$, $M = V_0/c$ avec $c = \sqrt{\gamma P/\rho}$, et 
%$J = V_0 /nD$.
%
%
%\item Interprétez physiquement les paramètres $Re,M$ et $J$.
%
%
%\item Trouver une relation simple du même type avec un coefficient $k_Q$ pour le
%couple $Q$.
%
%\item Ecrire la  puissance sur l'arbre de l'hélice  $P_h$  en fonction
%d'un paramètre sans dimension $C_P$.
%\item La puissance   utile pour la propulsion est 
%$P_u= T V_0$. Calculer le rendement propulsif $\eta_p$  en fonction de nombres sans dimension.
%
%\item Une hélice de diamètre $D=3.4 m$ a les caractéristiques détaillées dans
%la figure 3.
%
%%\begin{center}
%%\begin{tabular}{|c|cccc|}
%%$J$  &  1.06   & 1.19  & 1.34  &  1.44 \\
%%\hline $k_Q$ & 0.0410  & 0.04  & 0.0378  & 0.0355\\
%%\hline $\eta_p$ &  0.76  & 0.80 & 0.84  & 0.86
%%\end{tabular} 
%%\end{center}
%Déterminer la vitesse de l'avion $V_0$ qui lui permet d'absorber une puissance
%de 750 $kW$ à $n=1250  tr/mn$ et à 3660 m d'altitude ($\rho/\rho_0=0.639$) et
%donner la force de propulsion $T$. Pour le calcul numérique $n$ doit être en $tr/s$.
%
%Comparer le rendement de cette hélice à celle d'un propulseur idéal, de même
%surface, et donnant la même propulsion dans les mêmes conditions.
% 
%\end{enumerate}
%  
%   \begin{figure}
%   $$
%  \includegraphics[width = 8cm]{./courbepropulseur.eps}
%  $$
%\caption{   Figure 1 :  Abscisse : $J$, trait continu : $20 \times k_Q$, trait pointillé : $\eta_p$ }
%  \end{figure}
%

