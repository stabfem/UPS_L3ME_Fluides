%% Etude des lignes de courant d'un ecoulement 2D linéaire
%
% Ce programme sert de support au cours de mécaniques des fluides, L3 Mécanique, UPS
% -> chapitre 7 et exercice 7.0
%
% copyright D. Fabre, 2017 -> 2023

clear all; close all;

fprintf('\n Trace le champ de vecteur et les lignes de courant pour l''ecoulement defini par :');
fprintf('\n      u = a x + b y \n')
fprintf('      v = c x + d y \n \n')

% Valeur des paramètres a, b, c, d :
a = input(' a = ');
b = input(' b = ');
c = input(' c = ');
d = input(' d = ');
A11 = a;
A12 = b;
A21 = c;
A22 = d;

div = a+d;
rot = (c-b);

fprintf('\n Divergence : div u         = (du/dy+dv/dy) = %f',div);
fprintf('\n Vorticité  : (rot u) . e_z = (dv/dx-du/dy) = %f\n',rot);

%% Tracé du champ de vecteur de l'écoulement
xmin = -2; xmax = 2;ymin = -2; ymax = 2; % ranges for the figure
[xG, yG] = meshgrid(linspace(xmin, xmax, 21), linspace(ymin, ymax, 21)); %construction d'une grille
ux =  A11*xG+A12*yG;
uy =  A21*xG+A22*yG;
figure(1);hold off;
title({'Illustration of a 2D linear flow.','Left-click to draw a trajectory ; right-click to  draw the deformation of a square'})
hold on
quiver(xG, yG, ux, uy, 'Color', 'b');
axis([xmin xmax ymin ymax]);

%% Some parameters for the following
Tmax = 100; % max time for integratation of trajectory
Tmaxplot = 5; % max time for plot in figure 2.
Tsquare = 0.2;
h = 1; % size of the square
xA = [-h;-h];xB = [-h;h];xC = [h;h]; xD = [h;-h];% definition of the square

%% detect clicks on the figure
[xp,yp,button] = ginput(1); compteur = 0;
while((button>=1)&&(compteur<4))    
    if(button ==1)
        %% Left-click: drawing of a few trajectories to illustrate the flow
        xinit = [xp;yp];
        [t,xtraj] = ode45(@(t,x)([A11*x(1)+A12*x(2); A21*x(1)+A22*x(2)]),[0 Tmax],xinit);
        %[tb,xtrajback] = ode45(@(t,x)(-[A11*x(1)+A12*x(2); A21*x(1)+A22*x(2)]),[0 Tmax],xinit);
        figure(1);
        plot(xtraj(:,1),xtraj(:,2),'r',xtraj(1,1),xtraj(1,2),'ro');
        %figure(2); plot(t,sqrt(xtraj(:,1).^2+xtraj(:,2).^2));hold on;
        figure(1);
    else
        %% Right-click: deformation of an initially square material volume
        SQ = [xA,xB,xC,xD,xA];
        plot(SQ(1,:),SQ(2,:));
        [t,xtraj] = ode45(@(t,x)([A11*x(1)+A12*x(2); A21*x(1)+A22*x(2)]),[0 Tsquare],xA);xA = xtraj(end,:)';
        [t,xtraj] = ode45(@(t,x)([A11*x(1)+A12*x(2); A21*x(1)+A22*x(2)]),[0 Tsquare],xB);xB = xtraj(end,:)';
        [t,xtraj] = ode45(@(t,x)([A11*x(1)+A12*x(2); A21*x(1)+A22*x(2)]),[0 Tsquare],xC);xC = xtraj(end,:)';
        [t,xtraj] = ode45(@(t,x)([A11*x(1)+A12*x(2); A21*x(1)+A22*x(2)]),[0 Tsquare],xD);xD = xtraj(end,:)';
        SQ = [xA,xB,xC,xD,xA];
        plot(SQ(1,:),SQ(2,:));
        compteur = compteur+1;
    end
    [xp,yp,button] = ginput(1);
end

figure(1);
xlabel('x');  ylabel('y');  
title({'Ecoulement 2D linéaire', 
    ['A = [[',num2str(A11),',',num2str(A12),'];[',num2str(A21),',',num2str(A22),']]']});

set(gcf,'paperpositionmode','auto');
print('-dpng','-r80','Ecoulement2D_Linear.png');


