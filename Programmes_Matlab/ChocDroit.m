% Calcul et tracé des relations de saut a travers un choc droit.
% Programme conçu pour illustrer le chapitre 11 du cours de mécanique des
% fluides, L3 Mécanique, UPS, Toulouse.

%% Calcul des relations de saut a travers un choc droit en fonction de M1
gamma = 1.4; 
Mach1 = linspace(1,3,201);
rho2surrho1 = ((gamma + 1) * Mach1.^2) ./ ((gamma - 1) * Mach1.^2 + 2); % saut de masse volumique
p2surp1 = ((2 * gamma * Mach1.^2) - (gamma - 1)) / (gamma + 1); % saut de pression 
T2surT1 =  (2 + (gamma-1)*Mach1.^2)./((gamma+1)^2*Mach1.^2).*(2*gamma*Mach1.^2 + 1 -gamma); % saut de température
Mach2 = sqrt((2+(gamma-1)*Mach1.^2)./(2*gamma*Mach1.^2+1-gamma)); % mach aval
s2moinss1 = log(p2surp1.*rho2surrho1.^(-gamma) );

%% Figure pour saut de pression, masse volumique, température
figure(1);
plot(Mach1,p2surp1,'r-',Mach1,rho2surrho1,'b--',Mach1,T2surT1,'g-.');
set(0,'defaultLineLinewidth',2)
set(0,'defaultAxesFontSize',15)
title('Relations de saut pour un choc droit')
xlabel('M_1')
legend('p_2/p_1','\rho_2/\rho_1','T_2/T_1','location','NW','fontsize',14)
saveas(gcf,'ChocDroit_prhoT.png')

%% On calcule également le cas "antichoc" non physique 
Mach1_antichoc = linspace(.5,1,51);
Mach2_antichoc = sqrt((2+(gamma-1)*Mach1_antichoc.^2)./(2*gamma*Mach1_antichoc.^2+1-gamma)); % mach aval
rho2surrho1_antichoc = ((gamma + 1) * Mach1_antichoc.^2) ./ ((gamma - 1) * Mach1_antichoc.^2 + 2); % saut de masse volumique
p2surp1_antichoc = ((2 * gamma * Mach1_antichoc.^2) - (gamma - 1)) / (gamma + 1); % saut de pression 
s2moinss1_antichoc = log(p2surp1_antichoc.*rho2surrho1_antichoc.^(-gamma) );

%% Figure pour M2 et saut d'entropie 
% (les portions des courbes en pointillés correspondent à la solution "antichoc" non physique)
figure(2);subplot(2,1,1);
plot(Mach1,Mach2,'r-',Mach1_antichoc,Mach2_antichoc,'r--');
hold on; plot([.5 3],[1 1],'k:','linewidth',1);
xlabel('M_1');ylabel('M_2');
figure(2);subplot(2,1,2);
plot(Mach1,s2moinss1,'b-',Mach1_antichoc,s2moinss1_antichoc,'b:');
hold on; plot([.5 3],[0 0],'k:','linewidth',1);
xlabel('M_1');ylabel('(s_2-s_1)/c_v');
saveas(gcf,'ChocDroit_M2s.png')
