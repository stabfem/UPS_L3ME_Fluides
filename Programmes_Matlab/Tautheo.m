%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Tau = Tautheo(t,omega,h,nu,K,rho)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fonction donnant la force exercée sur les parois du tuyau en fonction du temps.

delta = sqrt(2*nu/omega);
tmp = (1+1i)/delta*tanh((1+1i)*h/delta);
Tau = 2*real(rho*nu*1i*K/omega * exp(1i*omega*t)*tmp);

end
