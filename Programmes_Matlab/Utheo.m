%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function U = Utheo(y, t,omega,h,nu,K)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fonction donnant la solution theorique du probl?me en regime etabli.
% NB la fonction est ecrite de maniere vectorielle ; y et t peuvent ?tre des vecteurs. 



delta = sqrt(2*nu/omega);
tmp = cosh((1+1i)*y/delta)/cosh((1+1i)*h/delta);
U = real(1i*K/omega * exp(1i*omega*t) * (1 - tmp));

end
