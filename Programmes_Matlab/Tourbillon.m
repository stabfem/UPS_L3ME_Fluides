%% Etude des lignes de courant d'un tourbillon (projection dans le plan horizontal)
%
% Ce programme sert de support au cours de mécaniques des fluides, L3 Mécanique, UPS
% -> chapitre 7 et exercice 7.0
%
% copyright D. Fabre, 2017 -> 2023

clear all; close all;
global D Gamma;
fprintf('\n Trace le champ de vecteur et les lignes de courant pour l''ecoulement defini par :');
fprintf('\n      u_r = -D / (2 pi r) \n')
fprintf('      u_theta = -Gamma (2 pi r) \n \n')

% Valeur des paramètres a, b, c, d :
Gamma = input(' Gamma = ');
D = input(' D = ');
a =1;
R0 = 3;

%% Tracé du champ de vecteur de l'écoulement
xmin = -R0; xmax = R0;ymin = -R0; ymax = R0; % ranges for the figure
[xG, yG] = meshgrid(linspace(xmin, xmax, 31), linspace(ymin, ymax, 31)); %construction d'une grille
for i=1:length(xG)
        for j= 1:length(yG)
            F = UU([xG(i,j),yG(i,j)]);
            ux(i,j) = F(1);
            uy(i,j) = F(2);
        end
end
figure(1);hold off;
title({'Illustration of a vortex/sink flow (in the horizontal plane)','Left-click to draw a trajectory ; right-click to  draw the deformation of a square'})
hold on
quiver(xG, yG, ux, uy, 'Color', 'b');
plot(a*cos(linspace(0,2*pi,100)),a*sin(linspace(0,2*pi,100)),'k:','LineWidth',2)
plot(R0*cos(linspace(0,2*pi,100)),R0*sin(linspace(0,2*pi,100)),'k:','LineWidth',2)
axis([xmin xmax ymin ymax]);

%% Some parameters for the following
Tmax = 100; % max time for integratation of trajectory
Tmaxplot = 5; % max time for plot in figure 2.
Tsquare = 2;
h = 1; % size of the square
xA = [.3;0];xB = [.5;0];xC = [.5;.2]; xD = [.3;.2];% definition of the square
xA2 = [1.3;0];xB2 = [1.5;0];xC2 = [1.5;.2]; xD2 = [1.3;.2];% definition of the square

%% detect clicks on the figure
[xp,yp,button] = ginput(1); compteur = 0;

while((button>=1)&&(compteur<6))    
    if(button ==1)
        %% Left-click: drawing of a few trajectories to illustrate the flow
        xinit = [xp;yp];
        [t,xtraj] = ode45(@(t,x)(UU(x)),[0 Tmax],xinit);
        %[tb,xtrajback] = ode45(@(t,x)(-[A11*x(1)+A12*x(2); A21*x(1)+A22*x(2)]),[0 Tmax],xinit);
        figure(1);
        plot(xtraj(:,1),xtraj(:,2),'r',xtraj(1,1),xtraj(1,2),'ro');
        %figure(2); plot(t,sqrt(xtraj(:,1).^2+xtraj(:,2).^2));hold on;
        figure(1);
    else
        %% Right-click: deformation of an initially square material volume
        SQ = [xA,xB,xC,xD,xA];
        plot(SQ(1,:),SQ(2,:));
        [t,xtraj] = ode45(@(t,x)(UU(x)),[0 Tsquare],xA);xA = xtraj(end,:)';
        [t,xtraj] = ode45(@(t,x)(UU(x)),[0 Tsquare],xB);xB = xtraj(end,:)';
        [t,xtraj] = ode45(@(t,x)(UU(x)),[0 Tsquare],xC);xC = xtraj(end,:)';
        [t,xtraj] = ode45(@(t,x)(UU(x)),[0 Tsquare],xD);xD = xtraj(end,:)';
        SQ = [xA,xB,xC,xD,xA];
        plot(SQ(1,:),SQ(2,:));

        SQ2 = [xA2,xB2,xC2,xD2,xA2];
        plot(SQ2(1,:),SQ2(2,:));
        [t,xtraj] = ode45(@(t,x)(UU(x)),[0 Tsquare],xA2);xA2 = xtraj(end,:)';
        [t,xtraj] = ode45(@(t,x)(UU(x)),[0 Tsquare],xB2);xB2 = xtraj(end,:)';
        [t,xtraj] = ode45(@(t,x)(UU(x)),[0 Tsquare],xC2);xC2 = xtraj(end,:)';
        [t,xtraj] = ode45(@(t,x)(UU(x)),[0 Tsquare],xD2);xD2 = xtraj(end,:)';
        SQ2 = [xA2,xB2,xC2,xD2,xA2];
        plot(SQ2(1,:),SQ2(2,:));

        compteur = compteur+1;
    end
    [xp,yp,button] = ginput(1);
end

figure(1);
xlabel('x');  ylabel('y');  
title({'Ecoulement 2D linéaire'});

set(gcf,'paperpositionmode','auto');
print('-dpng','-r80','Ecoulement2D_Linear.png');

function U = UU(X)
global D Gamma;
x = X(1); y = X(2); 
R = sqrt(x.^2+y.^2);
factorR = 1*(R<1)+1./(R.^2+1e-30).*(R>=1);
    Ux = (-Gamma/(2*pi)*y - D/(2*pi)*x).*factorR;
    Uy = (Gamma/(2*pi)*x - D/(2*pi)*y).*factorR;
    U = [Ux;Uy];
end

