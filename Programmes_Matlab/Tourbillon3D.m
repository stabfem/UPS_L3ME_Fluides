%% Etude des lignes de courant d'un ecoulement 2D linéaire
%
% Ce programme sert de support au cours de mécaniques des fluides, L3 Mécanique, UPS
% -> chapitre 7 et exercice 7.0
%
% copyright D. Fabre, 2017 -> 2023

clear all; close all;
global D Gamma a;
fprintf('\n Trace le champ de vecteur et les lignes de courant pour l''ecoulement defini par :');
fprintf('\n      u_r = -D / (2 pi r) \n')
fprintf('      u_theta = -Gamma (2 pi r) \n \n')

% Valeur des paramètres a, b, c, d :
%Gamma = input(' Gamma = ');
%D = input(' D = ');
Gamma = 1;
D = 0.2;
a =1;
R0 = 3;
H0 =3;



%for 
Tmax = 500;
yp = 0;
for zp = 0.5:.5:1
    for xp = -R0:2*R0:R0
%xp = R0; yp = 0; zp = 0.5;
        xinit = [xp;yp;zp];
        [t,xtraj] = ode45(@(t,x)(UU(x)),[0 Tmax],xinit);
        
        figure(1);hold on;
        plot3(xtraj(:,1),xtraj(:,2),xtraj(:,3),xtraj(1,1),xtraj(1,2),xtraj(1,3),'ro','Linewidth',2);
        zlim([0 H0]);
        xlim([-R0,R0]);ylim([-R0,R0]);
    end
end
%figure(1);
%xlabel('x');  ylabel('y');  
%title({'Ecoulement 2D linéaire'});

%set(gcf,'paperpositionmode','auto');
%print('-dpng','-r80','Ecoulement2D_Linear.png');

ZZ = linspace(0,H0,10);
theta = linspace(0,2*pi,50);
Xc = ones(size(ZZ))'*a*cos(theta);
Yc = ones(size(ZZ))'*a*sin(theta);
Zc = ZZ'*ones(size(theta));
figure(1);surf(Xc,Yc,Zc,'FaceAlpha',0.1)

ZZ = linspace(0,H0,10);
theta = linspace(0,2*pi,50);
Xc = ones(size(ZZ))'*R0*cos(theta);
Yc = ones(size(ZZ))'*R0*sin(theta);
Zc = ZZ'*ones(size(theta));
figure(1);surf(Xc,Yc,Zc,'FaceAlpha',0.1)
title({'Tourbillon de vidange (faites tourner les axes pour visualiser  en 3D'});


function U = UU(X)
global D Gamma a;
x = X(1); y = X(2); z= X(3);
R = sqrt(x.^2+y.^2);
factorR = 1*(R<1)+1./(R.^2+1e-30).*(R>=1);
    Ux = (-Gamma/(2*pi)*y - D/(2*pi)*x).*factorR;
    Uy = (Gamma/(2*pi)*x - D/(2*pi)*y).*factorR;
    Uz = D*z/(pi*a^2)*(R<a);
    U = [Ux;Uy;Uz];
end

