clear all; close all; clc;
	
%% Programme d'illustration de l'?coulement potentiel autour d'un cylindre
% Figure 1 : champ de vitesse, lignes de courant, isopotentielles.
% Figure 2 : pression et vitesse le long de la surface
% Figure 3 : champ de pression et resultante sur le cylindre


	fprintf('\n Trace le champ de vecteur, les isopotentielles et quelques lignes de courrant \n')
	fprintf('Pour l ecoulement autour d une sphère \n \n')
	
	% Valeur des parametres a, U
    a     = 1;
	U     = 1;

	
	% Maillage de l'espace en cordonn?es polaires : 
    % maillage fin pour Phi et Psi
	[r, theta] = meshgrid(linspace(1, 4, 100), linspace(-pi, pi, 100));
    x = r.*cos(theta);
    y = r.*sin(theta);
    % D?finition du potentiel et du champ de vitesse 
    Phi = U*(r+1./(2*r.^2)).*cos(theta);
    Psi = U*(r-1./r.^2).*sin(theta);
    ur = U*(1-1./r.^3).*cos(theta);
    ut = -U*(1+1./(2*r.^3)).*sin(theta);
    ux = ur.*cos(theta)-ut.*sin(theta);
    uy = ut.*cos(theta)+ur.*sin(theta);
    p = -(ut.^2+ur.^2-U^2)/2;
   
	
	% Trace des lignes de courant psi=constante et des isopotentielles :
    figure(1); 
    plot(cos(linspace(-pi, pi, 50)), sin(linspace(-pi, pi, 50)),'k');
    hold on;
    title({'Ecoulement potentiel autour d une sphere :' 'isopotentielles (rouge), lignes de courant (magenta) et champ de vitesse (bleu)'})
	contour(x, y, Phi, [-10:1:10], 'r:');
    contour(x, y, Psi, [-10:1:10], 'm');
    
	xlabel('x'); ylabel('y');
	axis equal tight
    
	% Maillage grossier pour tracer le champ de vecteurs
	[rG, thetaG] = meshgrid(linspace(1, 4, 10), linspace(-pi, pi, 17));
    xG = rG.*cos(thetaG);
    yG = rG.*sin(thetaG);
    urG = U*(1-1./rG.^3).*cos(thetaG);
    utG = -U*(1+1./(2*rG.^3)).*sin(thetaG);
    uxG = urG.*cos(thetaG)-utG.*sin(thetaG);
    uyG = utG.*cos(thetaG)+urG.*sin(thetaG);
    
	
	hold on
	quiver(xG, yG, uxG, uyG, 'Color', 'b');
    

  %  saveas(gcf,['Cylindre_Psi_U',num2str(U),'_G_',num2str(Gamma)],'png') % generate graphical file
	    

    figure(2);
    thetaS = linspace(0,2*pi,101);
    utS = -2*U.*sin(thetaS);
    pS = -(utS.^2-U^2)/2; %% equation de Bernouilli : p(a,theta) + ut(a,theta)^2/2 = p0 + U^2/2
   plot(thetaS,utS,'b',thetaS,pS,'g')
    hold on;
    plot(thetaS,0*thetaS,'k:')
    title('Champ de vitesse ut(a,theta) (bleu) et champ de pression p(a,theta) a la surface') 
    xlabel('theta');
    ylabel('u,p');
%    saveas(gcf,['Cylindre_Surface_U',num2str(U),'_G_',num2str(Gamma)],'png') % generate graphical file
	
    
    figure(3);
    plevels = linspace(min(min(p)),max(max(p)),15);
    contour(x, y, p, plevels, 'g--');hold on;
    ppos = p.*(p>=0);
    contour(x, y, ppos, plevels, 'g');
    title({'Champ de pression (vert) et force exercée sur l objet (rouge) ',' (pointillés : valeurs négatives; traits pleins : valeurs positives) '}) 
    hold on;
    plot(cos(linspace(-pi, pi, 50)), sin(linspace(-pi, pi, 50)),'k')
    quiver(cos(thetaS),sin(thetaS),-pS.*cos(thetaS),-pS.*sin(thetaS),'r');
%    plot(xA,yA,'bo');
    axis equal tight
    xlabel('x'); ylabel('y');
    axis([-2 2 -2 2]);
%    saveas(gcf,['Cylindre_Force_U',num2str(U),'_G_',num2str(Gamma)],'png') % generate graphical file
    
    %NB : essayer -3.336 pour avoir une ligne de courant qui passe par le
    %point selle
    
    
    %for i=1:10
    %xinit = ginput(1)
    %[t,xtraj] = ode45(@(t,x)(F_LV(x)),[0 10],xinit');
    %[t,xtrajback] = ode45(@(t,x)(-F_LV(x)),[0 10],xinit);
    %plot(xtraj(:,1),xtraj(:,2),'r',xtrajback(:,1),xtrajback(:,2),'m',xtraj(1,1),xtraj(1,2),'ro');
   %end
    
    
    
	% On sauvegarde la figure sur le disque :
	%print(gcf, 'streamlines.pdf', '-dpdf');  % format PDF
	%print(gcf, 'streamlines.eps', '-deps');  % format Postscript encapsule
	%print(gcf, 'streamlines.jpg', '-djpeg'); % format JPEG
