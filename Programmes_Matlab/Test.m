%
% Fichier de commande Visualisation_Ecoulement_Pulse.m
% visualisation de l'ecoulement pulse en conduite
%

%function Visualisation_Ecoulement_Pulse

close all;

%St = input('Nombre de Strouhal (par ex. 0.001 ou 1000) : ');
St = 5;
% dans ce programme on adimensionalise en posant h=1 et nu=1, donc omega se
% deduit du nombre de strouhal
h = 1;
nu = 1;
K = 1;
omega = 2*nu/h^2*St;


%Creation d'un vecteur y corresponant au domaine [-1,1] (on raffine pres des parois...)
%y1 = linspace(-1, -0.8, 30);
%y2 = linspace(-0.8, 0.8, 40);
%y3 = linspace(0.8, 1, 30);
y = linspace(-1,1,500);

% Discretisation du temps sur une periode:
tmax  = 2*pi/omega;
t = linspace(0,tmax,21);

umax = max(max( Utheo(y, t',omega,h,nu,K) )); % pour borner les axes 
umin = min(min( Utheo(y, t',omega,h,nu,K) )); % pour borner les axes 

%fprintf('Faire Ctrl C pour arreter...\n');
%while 1
%  for n = 1:length(t)
    n = 5;
    tn = t(n);
    plot(Utheo(y,t(n),omega,h,nu,K), y,'r');
    hold on
    Tau = Tautheo(tn,omega,h,nu,K,1);
    plot([0 Tau*.2],[1 .8],'b--') 
    plot(zeros(size(y)), y,'k--') % axe x = 0
    plot([umin, umax], [0, 0],'k--') % axe y = 0
    plot([umin, umax], [1 1], 'k') % paroi sup
    plot([umin, umax], [1.01 1.01], 'k') % paroi sup
    plot([umin, umax], [-1 -1], 'k') % paroi inf
    plot([umin, umax], [-1.01 -1.01], 'k') % paroi inf
    axis([umin, umax, -1.2, 1.2]);
    title(['profil de vitesse a t/T = ' num2str(omega*t(n)/(2*pi))]);
    xlabel('u(y, t)');
    ylabel('y');
    drawnow;
    pause(0.3);
    hold off;
%  end
%end