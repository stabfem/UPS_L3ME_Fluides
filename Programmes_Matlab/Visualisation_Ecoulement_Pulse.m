%
% Fichier de commande Visualisation_Ecoulement_Pulse.m
% visualisation de l'ecoulement pulse en conduite
%
% Ce programme utilise deux sous-programmes Utheo et Tautheo qui calculent
% la solution analytique

function Visualisation_Ecoulement_Pulse

close all;

St = input('Nombre de Strouhal (par ex. 0.001 ou 1000) : ');
% dans ce programme on adimensionalise en posant h=1 et nu=1, donc omega se
% deduit du nombre de strouhal
h = 1;
nu = 1;
K = 1;
omega = 2*nu/h^2*St;




% Figure 2 : tau_p en fonction du temps
figure(2);
tfig = linspace(0,4*pi/omega,200);
Tau = Tautheo(tfig,omega,h,nu,K,1);
plot(tfig*omega/(2*pi),Tau);xlabel('t/T');ylabel('\tau_p(t)');
title('Force exercée sur les parois pendant deux cycles d''oscillation')
pause(1);


%Creation d'un vecteur y corresponant au domaine [-1,1] (on raffine pres des parois...)
y = linspace(-1,1,500);
% Discretisation du temps sur une periode:
tmax  = 2*pi/omega;
t = linspace(0,tmax,21);

umax = max(max( Utheo(y, t',omega,h,nu,K) )); % pour borner les axes 
umin = min(min( Utheo(y, t',omega,h,nu,K) )); % pour borner les axes 

fprintf('Faire Ctrl C pour arreter...\n');
figure(1);
while 1
  for n = 1:length(t)
    plot(Utheo(y,t(n),omega,h,nu,K), y,'r')
    hold on
    plot(zeros(size(y)), y,'k--') % axe x = 0
    plot([umin, umax], [0, 0],'k--') % axe y = 0
    plot([umin, umax], [1 1], 'k') % paroi sup
    plot([umin, umax], [1.01 1.01], 'k') % paroi sup
    plot([umin, umax], [-1 -1], 'k') % paroi inf
    plot([umin, umax], [-1.01 -1.01], 'k') % paroi inf
    axis([umin, umax, -1.2, 1.2]);
    title(['profil de vitesse a t/T = ' num2str(omega*t(n)/(2*pi))]);
    xlabel('u(y, t)');
    ylabel('y');
    drawnow;
    pause(0.3);
    hold off;
  end
end