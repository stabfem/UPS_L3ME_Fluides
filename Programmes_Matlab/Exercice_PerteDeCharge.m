%% Calcul de la puissance d'une pompe pour alimenter un circuit solaire
% (exercice de cours, chapitre 9)


%% Donnees


d = 0.008            % diametre (m)
L = 60               % longueur du circuit
k = 1.5e-6           % rugosité (m)
rho = 980           % masse volumique (kg/m^3)
nu = 2e-6            % viscosité (m^2/s)
Q = 250*1e-3/3600    % debit de volume (m^3/s)


%% Parametres de lecoulement
S = pi/4*d^2  % surface
U = Q/S       % vitesse
Re = U*d/nu   % Reynolds
epsilon = k/d % rugosité relative

%% estimation du coef de perte de charge linéaire
lambda = 4e-2 % estimation avec le diagramme de moody
% on fait en suite 3 iterations avec la formule de colbroook
lambda = 1/(-2*log10(2.55/(Re*sqrt(lambda)))+epsilon/3.71)^2
lambda = 1/(-2*log10(2.55/(Re*sqrt(lambda)))+epsilon/3.71)^2
lambda = 1/(-2*log10(2.55/(Re*sqrt(lambda)))+epsilon/3.71)^2

%% calcul de la perte de charge linéaire
Deltalin = .5*rho*U^2*L/d*lambda

%% estimation de la perte de charge singulière 
K = 20*0.75+2*6.4
Deltasing = .5*rho*U^2*K

%% perte de charge totale 
Delta = Deltalin+Deltasing

%% Puissance de la pompe
Puiss = Delta*S*U
