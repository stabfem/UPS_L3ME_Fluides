# -*- coding: utf-8 -*-
"""
Created on Sun Apr  2 16:45:56 2023

@author: Taha-Yassine Sokakini & Oumaima El-Hassine (Etudiants M1 Méca UPS)
  (Adapted from a Matlab program from D. Fabre, 2017-2022) 
"""
#### Program Kinetics.m
####
#### Dynamics of a 2D gas of hard spheres 
#### Pedagogical program to illustrate the concepts of thermodynamics 
#### and fluid mechanics.
####
#### USAGE :
#### 1/ Launch program
#### 2/ Select an option
#### 3/ position the figures so that they do not overlap
#### 4/ type "enter" to launch the simulation
####
#### According to the cases the program may generate the following figures :
####      Figure 1 : Instantaneous position of particules
####      Figure 2 : Statistics of particules in vertical 'bins'
####      Figure 3 : x-Force exerted on right boundary due to collision and its averaged value ("Pressure") 
####      Figure 6 : y-Force exerted on right boundary due to collision and its averaged value ("Stress") 
#
# TOBEDONE for 2023 : switch 3 and 4, add description for all cases, switch x/y to fit with course (?)
import numpy as np
import matplotlib.pyplot as plt
import time
import pylab
plt.close('all')
print('\nProgram Kinetics.py\n')
print('Which kind of simulation do you want to do?')
print('[1] -> L équation d état des gaz')
print('[2] -> Processus de diffusion avec deux populations de particules')
print('[3] -> Ecoulement de cisaillement vertical ')
print(' [4] -> vertical shear flow (with initial discontinuity)')
print(' [5] -> diffusion of temperature')
print(' [6] -> shock wave');
print(' [7] -> gaz initially in uniform motion between two steady plates');
print(' [8] -> custom');
choice = int(input('Votre choix? '))

# Choice 1
if choice == 1: 
# Dimensions
    Lx = 1
    Ly = 1
    particulediameter = 0.01 # 0.01 is a good value if N is of order 1000
    g = 0
        
# Properties of particles 1 (initially in left half domain)
    Nparticules_1 = 950
    X1min = 0
    X1max = 1 #min and max position of domain initially occupied
    T1 = 4 # 'temperature' 
    vq1 = np.sqrt(T1)
    Um1 = 0
    Vm1 = 0 # mean horizontal and vertical velocities
    color_1 = 'b' # color used for plots

# Properties of particles 2 (initially in left half domain)
    Nparticules_2 = 50
    X2min = 0
    X2max = 1 #min and max position of domain initially occupied
    T2 = 4 # 'temperature' 
    vq2 = np.sqrt(T2)
    Um2 = 0
    Vm2 = 0 # mean horizontal and vertical velocities
    color_2 = 'r' # color used for plots 

# Properties of the boundaries
    BC_left = 'PerfectWall' # allowed values are 'PerfectWall' and 'thermostat'
    BC_updown = 'periodic' # allowed values are 'PerfectWall' and 'periodic'
    BC_right = 'PerfectWall'
    # in case left boundary is a thermostat, one can impose the mean velocity
    # and temperature in an oscillating way
    #Twall_m = 0;
    #Twall_a = 0;
    #Uwall_m = 0;
    #Uwall_a = 0;
    #Vwall_m = 1;
    #Vwall_a = 0;
    #omega = 0.1;
    Twall_left = T1
    Twall_right = T1
    Uwall_left = 0
    Uwall_right = 0
    Vwall_left = 0
    Vwall_right = 0
    
    X_fictive_surface = 0.5
# plotting parameters
    Nbin = 10 # number of vertical 'bins' for statistics
    fig1 = 1 # set to 1 for figure of particle positions in figure 1
    fig2 = 1 # set to 1 to plot 'pressure' force on right boundary and temperature in figure 2 
    fig3 = 1 # set to 1 to plot statistics in vertical bins in figure 3;  
    fig4 = 0
    fig5 = 0 # set to 1 to plot vertical velocities at successive instants in figure 5
    fig6 = 0
    stepfig = 2 # number of time steps between plot refreshing (for fig. 2,3) (raise to accelerate)
    stepfig4 = 20 # idem for fig. (4,5)
# Numerical parameters
    Tmax = 1000 # end of simulation 
    dt = 0.005 # time step ; dt should be smaller than diam/vq to resolve properly collisions
    diameter_for_plots = 345*particulediameter # diameter or particules in pixels for plots
    Naveraging = 100000 # for time-averaging of pressure, etc... if large value average since beginning of simulation
    pausemode = 0 # set to 0 for automatic time advance, and to 1 for manual (press enter to advance) 
# Choice 2
elif choice == 2:
    # Dimensions
# System parameters
    Lx = 1
    Ly = 1
    particulediameter = 0.01
    g = 0
    # Properties of particules 1 (initially in left half domain)
    Nparticules_1 = 500
    X1min = 0
    X1max = 0.5
    T1 = 2
    vq1 = np.sqrt(T1)
    Um1 = 0
    Vm1 = 0
    color_1 = 'b'
    # Properties of particules 2 (initially in right half domain)
    Nparticules_2 = 500
    X2min = 0.5
    X2max = 1
    T2 = 2
    vq2 = np.sqrt(T2)
    Um2 = 0
    Vm2 = 0
    color_2 = 'r'
    # Properties of the boundaries
    BC_left = 'PerfectWall'
    BC_updown = 'periodic'
    BC_right = 'PerfectWall'
    # In case left boundary is a thermostat, one can impose the mean velocity
    # and temperature in an oscillating way
    Twall_left = T1
    Twall_right = T1
    Uwall_left = 0
    Uwall_right = 0
    Vwall_left = 0
    Vwall_right = 0
    X_fictive_surface = 0.5
    # Plotting parameters
    Nbin = 10
    fig1 = 1
    fig2 = 1
    fig3 = 1
    fig4 = 0
    fig5 = 0
    fig6 = 0
    stepfig = 2
    stepfig4 = 20
    diameter_for_plots = 345 * particulediameter
    # Numerical parameters
    Tmax = 1000
    dt = 0.005
    Naveraging = 100000
    pausemode = 0
    print("###")
    print("### Illustration of diffusion of concentration in a mixture of two species ")
    print("###")
    print("### Initial Condition :")
    print("### Left half (x<1/2):")
    print("###      500 particules (blue) with average <v_x> = <v_y> = 0")
    print("### Right half (x>1/2):")
    print("###      500 particules (red) with average vertical velocity <v_x> =0, <v_y> = 0")
    print("### (Particules are supposed of same mass)")
    print("###")
# Choice 3
elif choice == 3:
# Dimensions
    Lx = 1
    Ly = 1
    particulediameter = 0.01 # 0.01 is a good value if N is of order 1000
    g = 0
    # Properties of particles 1 (initially in left half domain)
    Nparticules_1 = 950
    X1min = 0
    X1max = 1 # min and max position of domain initially occupied
    T1 = 4 # 'temperature'
    vq1 = np.sqrt(T1)
    Um1 = 0
    Vm1 = 0 # mean horizontal and vertical velocities
    color_1 = 'b' # color used for plots
    # Properties of particles 2 (initially in left half domain)
    Nparticules_2 = 50
    X2min = 0
    X2max = 1 # min and max position of domain initially occupied
    T2 = 4 # 'temperature'
    vq2 = np.sqrt(T2)
    Um2 = 0
    Vm2 = 0 # mean horizontal and vertical velocities
    color_2 = 'r' # color used for plots

# Properties of the boundaries
    BC_left = 'RealWall' # allowed values are 'PerfectWall' and 'thermostat'
    BC_updown = 'periodic' # allowed values are 'PerfectWall' and 'periodic'
    BC_right = 'RealWall'

# in case left boundary is a real wall, one can impose the mean velocity and temperature 
    Twall_left = T1
    Twall_right = T1
    Uwall_left = 0
    Uwall_right = 0
    Vwall_left = 0
    Vwall_right = 2

    X_fictive_surface = 0.5

# plotting parameters
    Nbin = 10 # number of vertical 'bins' for statistics
    fig1 = 1 # set to 1 for figure of particle positions in figure 1
    fig2 = 0# set to 1 to plot 'pressure' force on right boundary and temperature in figure 2 
    fig3 = 1 # set to 1 to plot statistics in vertical bins in figure 3;  
    fig4 = 0 # set to 1 to plot density of particles 1 at successive instants in figure 4 
    fig5 = 0 # set to 1 to plot vertical velocities at successive instants in figure 5
    fig6 = 1 # to plot 'stress' and flux of vertical momentum
    stepfig = 5 # number of time steps between plot refreshing (for fig. 2,3) (raise to accelerate)
    stepfig4 = 20 # idem for fig. (4,5)

# Numerical parameters
    Tmax = 10 # end of simulation 
    dt = 0.005 # time step; dt should be smaller than diam/vq to resolve properly collisions
    diameter_for_plots = 345*particulediameter # diameter or particles in pixels for plots
    Naveraging = 100000 # for time-averaging of pressure, etc... if large value average since beginning of simulation
    pausemode = 0 # set to 0 for automatic time advance, and to 1 for manual (press enter to advance)
##choice4
elif choice == 4 :

## Dimensions
    Lx = 1;
    Ly = 1;
    particulediameter = 0.01; # 0.01 is a good value if N is of order 1000
    g= 0.;
    # Properties of particules 1 (initially in left half domain)
    Nparticules_1 = 500; 
    X1min = 0; X1max = 0.5; #min and max position of domain initially occupied
    T1 = 4; # 'temperature' 
    vq1 = np.sqrt(T1);
    Um1 = 0; Vm1 = 0; # mean horizontal and vertical velocities
    color_1 = 'b'; # color used for plots
    # Properties of particules 2 (initially in left half domain)
    Nparticules_2 = 500; 
    X2min = 0.5; X2max = 1; #min and max position of domain initially occupied
    T2 = 4; # 'temperature' 
    vq2 = np.sqrt(T2); 
    Um2 = 0; Vm2 = 2; # mean horizontal and vertical velocities
    color_2 = 'r'; # color used for plots 
    # Properties of the boundaries
    BC_left = 'RealWall'; # allowed values are 'PerfectWall' and 'thermostat'
    BC_updown = 'periodic'; # allowed values are 'PerfectWall' and 'periodic'
    BC_right = 'RealWall';
    #in case left boundary is a real wall, one can impose the mean velocity and temperature 
    Twall_left = T1; 
    Twall_right = T1;
    Uwall_left = 0;
    Uwall_right = 0;
    Vwall_left = 0;
    Vwall_right = 2;
    
    X_fictive_surface= 0.5;
    # plotting parameters
    Nbin = 10; # number of vertical 'bins' for statistics
    fig1 = 1; # set to 1 for figure of particule positions in figure 1
    fig2 = 0; # set to 1 to plot 'pressure' force on right boundary and temperature in figure 2 
    fig3 = 1; # set to 1 to plot statistics in vertical bins in figure 3;  
    fig4 = 0; # set to 1 to plot density of particules 1 at successive instants in figure 4 
    fig5 = 0; # set to 1 to plot vertical velocities at successive instants in figure 5
    fig6 = 0; # to plot 'stress' and flux of vertical momentum
    stepfig = 5; # number of time steps between plot refreshing (for fig. 2,3) (raise to accelerate)
    stepfig4 = 20; # idem for fig. (4,5)
    # Numerical parameters
    Tmax = 10; # end of simulation 
    dt = 0.005; # time step ; dt should be smaller than diam/vq to resolve properly collisions
    diameter_for_plots = 345*particulediameter; # diameter or particules in pixels for plots
    Naveraging = 100000; # for time-averaging of pressure, etc... if large value average since beginning of simulation
    pausemode = 0; # set to 0 for automatic time advance, and to 1 for manual (press enter to advance) 
    
    print('###');
    print('### Illustration of diffusion of momentum for a gaz with initial discontinuity in "macroscopic velocity"');
    print('###');
    print('### Initial Condition :');
    print('### Left half (x<1/2):');
    print('###      500 particules (blue) with average <v_x> = <v_y> = 0');
    print('### Right half (x>1/2):');
    print('###      500 particules (red) with average vertical velocity <v_x> =0, <v_y> = 2');
    print('###');
    print('###');

# Choice 5 
elif choice==5:

# Dimensions
    Lx = 1;
    Ly = 1;
    particulediameter = 0.01; # 0.01 is a good value if N is of order 1000
    g= 0.;
    # Properties of particules 1 (initially in left half domain)
    Nparticules_1 = 500; 
    X1min = 0; X1max = .5; #min and max position of domain initially occupied
    T1 = 1; # 'temperature' 
    vq1 = np.sqrt(T1);
    Um1 = 0; Vm1 = 0; # mean horizontal and vertical velocities
    color_1 = 'b'; # color used for plots
    # Properties of particules 2 (initially in left half domain)
    Nparticules_2 = 500; 
    X2min = 0.5; X2max = 1; #min and max position of domain initially occupied
    T2 = 2; # 'temperature' 
    vq2 = np.sqrt(T2); 
    Um2 = 0; Vm2 = 0; # mean horizontal and vertical velocities
    color_2 = 'b'; # color used for plots 
    # Properties of the boundaries
    BC_left = 'PerfectWall'; # allowed values are 'PerfectWall' and 'thermostat'
    BC_updown = 'periodic'; # allowed values are 'PerfectWall' and 'periodic'
    BC_right = 'PerfectWall';
    Twall_left = T1; 
    Twall_right = T1;
    Uwall_left = 0;
    Uwall_right = 0;
    Vwall_left = 0;
    Vwall_right = 0;
    X_fictive_surface= 0.5;
    # plotting parameters
    Nbin = 10; # number of vertical 'bins' for statistics
    fig1 = 1; # set to 1 for figure of particule positions in figure 1
    fig2 = 0; # set to 1 to plot 'pressure' force on right boundary and temperature in figure 2 
    fig3 = 1; # set to 1 to plot statistics in vertical bins in figure 3;  
    fig4 = 0; # set to 1 to plot density of particules 1 at successive instants in figure 4 
    fig5 = 0; # set to 1 to plot vertical velocities at successive instants in figure 5
    fig6 = 0;
    stepfig = 2; # number of time steps between plot refreshing (for fig. 2,3) (raise to accelerate)
    stepfig4 = 20; # idem for fig. (4,5)
    # Numerical parameters
    Tmax = 1000; # end of simulation 
    dt = 0.005; #time step ; dt should be smaller than diam/vq to resolve properly collisions
    diameter_for_plots = 345*particulediameter; # diameter or particules in pixels for plots
    Naveraging = 100000; #for time-averaging of pressure, etc... if large value average since beginning of simulation
    pausemode = 0; # set to 0 for automatic time advance, and to 1 for manual (press enter to advance) 

# Choice 6 
elif choice == 6 :

# Dimensions
    Lx = 1;
    Ly = 1;
    particulediameter = 0.01; # 0.01 is a good value if N is of order 1000
    g= 0.;
    # Properties of particules 1 (initially in left half domain)
    Nparticules_1 = 1000; 
    X1min = 0; X1max = 0.5; #min and max position of domain initially occupied
    T1 = 2; # 'temperature' 
    vq1 = np.sqrt(T1);
    Um1 = 0; Vm1 = 0; # mean horizontal and vertical velocities
    color_1 = 'b'; # color used for plots
    # Properties of particules 2 (initially in left half domain)
    Nparticules_2 = 300; 
    X2min = 0.5; X2max = 1; #min and max position of domain initially occupied
    T2 = 2; # 'temperature' 
    vq2 = np.sqrt(T2); 
    Um2 = 0; Vm2 = 0; # mean horizontal and vertical velocities
    color_2 = 'b'; # color used for plots 
    # Properties of the boundaries
    BC_left = 'PerfectWall'; # allowed values are 'PerfectWall' and 'thermostat'
    BC_updown = 'periodic'; #allowed values are 'PerfectWall' and 'periodic'
    BC_right = 'PerfectWall';
    Twall_left = T1; 
    Twall_right = T1;
    Uwall_left = 0;
    Uwall_right = 0;
    Vwall_left = 0;
    Vwall_right = 0;
    X_fictive_surface= 0.5;
    # plotting parameters
    Nbin = 10; # number of vertical 'bins' for statistics
    fig1 = 1; # set to 1 for figure of particule positions in figure 1
    fig2 = 0; # set to 1 to plot 'pressure' force on right boundary and temperature in figure 2 
    fig3 = 1; # set to 1 to plot statistics in vertical bins in figure 3;  
    fig4 = 0; # set to 1 to plot density of particules 1 at successive instants in figure 4 
    fig5 = 0; # set to 1 to plot vertical velocities at successive instants in figure 5
    fig6 = 0;
    stepfig = 2; # number of time steps between plot refreshing (for fig. 2,3) (raise to accelerate)
    stepfig4 = 20; # idem for fig. (4,5)
    # Numerical parameters
    Tmax = 1000; # end of simulation 
    dt = 0.005; # time step ; dt should be smaller than diam/vq to resolve properly collisions
    diameter_for_plots = 345*particulediameter; # diameter or particules in pixels for plots
    Naveraging = 100000; #for time-averaging of pressure, etc... if large value average since beginning of simulation
    pausemode = 0; # set to 0 for automatic time advance, and to 1 for manual (press enter to advance) 


# Choice 7 
elif choice==7 :

# Dimensions
    Lx = 1;
    Ly = 1;
    particulediameter = 0.01; #0.01 is a good value if N is of order 1000
    g= 0.;
    # Properties of particules 1 (initially in left half domain)
    Nparticules_1 = 950; 
    X1min = 0; X1max = 1.; #min and max position of domain initially occupied
    T1 = 4; #'temperature' 
    vq1 = np.sqrt(T1);
    Um1 = 0; Vm1 = 2; # mean horizontal and vertical velocities
    color_1 = 'b'; # color used for plots
    # Properties of particules 2 (initially in left half domain)
    Nparticules_2 = 50; 
    X2min = 0.; X2max = 1; #min and max position of domain initially occupied
    T2 = 4; # 'temperature' 
    vq2 = np.sqrt(T2); 
    Um2 = 0; Vm2 = 2; # mean horizontal and vertical velocities
    color_2 = 'r'; # color used for plots 
    #Properties of the boundaries
    BC_left = 'RealWall'; # allowed values are 'PerfectWall' and 'thermostat'
    BC_updown = 'periodic'; # allowed values are 'PerfectWall' and 'periodic'
    BC_right = 'RealWall';
    # in case left boundary is a real wall, one can impose the mean velocity and temperature 
    Twall_left = T1; 
    Twall_right = T1;
    Uwall_left = 0;
    Uwall_right = 0;
    Vwall_left = 0;
    Vwall_right = 0;
    
    X_fictive_surface= 0.5;
    # plotting parameters
    Nbin = 10; # number of vertical 'bins' for statistics
    fig1 = 1; # set to 1 for figure of particule positions in figure 1
    fig2 = 0; # set to 1 to plot 'pressure' force on right boundary and temperature in figure 2 
    fig3 = 1; # set to 1 to plot statistics in vertical bins in figure 3;  
    fig4 = 0; # set to 1 to plot density of particules 1 at successive instants in figure 4 
    fig5 = 0; # set to 1 to plot vertical velocities at successive instants in figure 5
    fig6 = 1; # to plot 'stress' and flux of vertical momentum
    stepfig = 5; # number of time steps between plot refreshing (for fig. 2,3) (raise to accelerate)
    stepfig4 = 20; # idem for fig. (4,5)
    # Numerical parameters
    Tmax = 10; # end of simulation 
    dt = 0.005; # time step ; dt should be smaller than diam/vq to resolve properly collisions
    diameter_for_plots = 345*particulediameter; # diameter or particules in pixels for plots
    Naveraging = 100000; # for time-averaging of pressure, etc... if large value average since beginning of simulation
    pausemode = 0; # set to 0 for automatic time advance, and to 1 for manual (press enter to advance) 



elif choice>=8 :
  print("Nothing defined yet for choice {choice}. You can define your own case by modifying the code (lines 370 etc...)")

# Initialisations : initial positions and velocities of particules
Nparticules = Nparticules_1 + Nparticules_2

X = np.zeros(Nparticules)
Y = np.zeros(Nparticules)
U = np.zeros(Nparticules)
V = np.zeros(Nparticules)

X[:Nparticules_1] = X1min + (X1max - X1min) * np.random.rand(Nparticules_1)
Y[:Nparticules_1] = np.random.rand(Nparticules_1)
U[:Nparticules_1] = Um1 + vq1 / np.sqrt(2) * np.random.randn(Nparticules_1)
V[:Nparticules_1] = Vm1 + vq1 / np.sqrt(2) * np.random.randn(Nparticules_1)
U[:Nparticules_1] -= np.mean(U[:Nparticules_1]) - Um1
V[:Nparticules_1] -= np.mean(V[:Nparticules_1]) - Vm1

if Nparticules_2 > 0:
    X[Nparticules_1:Nparticules] = X2min + (X2max - X2min) * np.random.rand(Nparticules_2)
    Y[Nparticules_1:Nparticules] = np.random.rand(Nparticules_2)
    U[Nparticules_1:Nparticules] = Um1 + vq2 / np.sqrt(2) * np.random.randn(Nparticules_2)
    V[Nparticules_1:Nparticules] = Vm2 + vq2 / np.sqrt(2) * np.random.randn(Nparticules_2)
    U[Nparticules_1:Nparticules] -= np.mean(U[Nparticules_1:Nparticules]) - Um2
    V[Nparticules_1:Nparticules] -= np.mean(V[Nparticules_1:Nparticules]) - Vm2
# Various initialisations before simulation

# construction of two vectors used for drawing 'bins' in figure 1
Xcage = np.zeros(2*(Nbin+1))
Ycage = np.zeros(2*(Nbin+1))
for i in range(0, Nbin, 2):
    Xcage[2 * i-2 ] = (i -1) * Lx / Nbin
    Xcage[2 * i] = Xcage[2 * i -1 ]
    Xcage[2 * i +2] = i * Lx / Nbin
    Xcage[2 * i + 2] = Xcage[2 * i + 1]
    Ycage[2 * i -2] = - 1
    Ycage[2 * i-1] = Ly + 1
    Ycage[2 * i ] = Ly + 1
    Ycage[2 * i +1] = - 1


t_tab = []
Fx_tab = []
Fy_tab = []
P_tab = []
Tauxy_tab = []
Fx_left_tab = []
Fy_left_tab = []
P_left_tab = []
Tauxy_left_tab = []
flux_momentum_x_tab = []
flux_momentum_y_tab = []
flux_momentum_x_av_tab = []
flux_momentum_y_av_tab = []
T_tab = []

x_bin = np.zeros(2*Nbin)
x_bin_c=  np.zeros(2*Nbin)


for i in range(Nbin):
    x_bin[2*i] = i / 10
    x_bin[2*i+1] = (i+1) / 10
    x_bin_c[2*i] = (i+0.5) / 10
    x_bin_c[2*i+1] = (i+0.5) / 10

if fig4 == 1:
    plt.figure(4)
    plt.clf()

print('time loop')

 ## Time loop
for it in range(int(Tmax/dt)):
    t = it*dt
    print(str(t))
## balistic part of the trajectory
    X = X+dt*U
    Y = Y+dt*V
    
## collisions with boundaries
    Fx_right_boundary = 0
    Fy_right_boundary = 0
    Fx_left_boundary = 0
    Fy_left_boundary = 0
    for i in range(Nparticules):
        if(X[i]<0 and U[i]<0): # collision with left boundary
            Uians = U[i]
            Vians = V[i]
            if(BC_left == 'PerfectWall'): # left boundary treated as a wall 
                U[i] = -U[i]
                X[i] = -X[i]
#           elif(BC_left == thermostat') # left boundary treated as a "thermostat"
#                    X(i) = -X(i);
#                    U(i) = (Uwall_m+Uwall_a*np.sin(omega*t))+np.sqrt(Twall_m+Twall_a*np.sin(omega*t))/np.sqrt(2)*np.abs(np.random.normal());
#                    V(i) = (Vwall_m+Vwall_a*np.sin(omega*t))+np.sqrt(Twall_m+Twall_a*np.sin(omega*t))/np.sqrt(2)*(np.random.normal());
            elif(BC_left == 'RealWall'): # left boundary treated as a "real" wall
                X[i] = -X[i]
                U[i] = Uwall_left + np.sqrt(Twall_left/2)*np.abs(np.random.normal(1))
                V[i] = Vwall_left + np.sqrt(Twall_left/2)*np.random.normal(1)
            Fx_left_boundary = Fx_left_boundary + (U[i]-Uians)/dt
            Fy_left_boundary = Fy_left_boundary - (V[i]-Vians)/dt
            print('boundary')
        if(X[i]>Lx and U[i]>0): # collision with right boundary
            Uians = U[i]
            Vians = V[i]
            X[i] = Lx - (X[i]-Lx)
            if(BC_right == 'PerfectWall'):
                U[i] = -U[i]
            elif(BC_right == 'RealWall'):
                U[i] = Uwall_right - np.sqrt(Twall_right/2)*np.abs(np.random.normal(1))
                V[i] = Vwall_right + np.sqrt(Twall_right/2)*np.random.normal(1)
            Fx_right_boundary = Fx_right_boundary - (U[i]-Uians)/dt
            Fy_right_boundary = Fy_right_boundary + (V[i]-Vians)/dt
        if(Y[i]<0): # upper boundary
            Y[i] = Y[i] + Ly # periodicity
        if(Y[i]>1): # lower boundary
            Y[i] = Y[i] - Ly # periodicity

# Comptabilisation du flux à travers la surface médiane

    flux_momentum_x = 0
    flux_momentum_y = 0
    for i in range(Nparticules):
        if ((X[i]-dt*U[i]-X_fictive_surface)*(X[i]-X_fictive_surface) < 0):
            if (U[i] > 0):# Particule traversant dans la direction positive
                flux_momentum_x += U[i] / dt
                flux_momentum_y -= V[i] / dt
            else:
                # Particule traversant dans la direction négative
                flux_momentum_x -= U[i] / dt
                flux_momentum_y += V[i] / dt
# Collisions élastiques entre particules
    for i in range(0,Nparticules):
     for j in range(i+1, Nparticules):
        distance = np.sqrt((X[i]-X[j])**2 + (Y[i]-Y[j])**2)
        if (distance < particulediameter) and ((X[j]-X[i])*(U[j]-U[i])+(Y[j]-Y[i])*(V[j]-V[i]) < 0):
          tx = Y[j] - Y[i]
          ty = X[i] - X[j]
          U1a = U[j]
          U2a = U[i]
          V1a = V[j]
          V2a = V[i]
          U[j] = (U1a*tx*tx + U2a*ty*ty + V1a*tx*ty - V2a*tx*ty) / (tx*tx + ty*ty)
          U[i] = (U1a*ty*ty + U2a*tx*tx - V1a*tx*ty + V2a*tx*ty) / (tx*tx + ty*ty)
          V[j] = (U1a*tx*ty - U2a*tx*ty + V1a*ty*ty + V2a*tx*tx) / (tx*tx + ty*ty)
          V[i] = -(U1a*tx*ty - U2a*tx*ty + V1a*tx*tx + V2a*ty*ty) / (tx*tx + ty*ty)
# statistics
    t_tab.append(t)
    print('append397')
    # force exerted on right boundary
    Fx_tab.append(Fx_right_boundary)
    P = np.mean(Fx_tab[max(1, it-Naveraging):it])
    P_tab.append(P)
    
    Fy_tab.append(Fy_right_boundary)
    Tauxy = np.mean(Fy_tab[max(1, it-Naveraging):it])
    Tauxy_tab.append(Tauxy)
    
    # force exerted on left boundary
    Fx_left_tab.append(Fx_left_boundary)
    P_left = np.mean(Fx_left_tab[max(1, it-Naveraging):it])
    P_left_tab.append(P_left)
    
    Fy_left_tab.append(Fy_left_boundary)
    Tauxy_left = np.mean(Fy_left_tab[max(1, it-Naveraging):it])
    Tauxy_left_tab.append(Tauxy_left)
    
    # flux of momentum on mid plane
    flux_momentum_x_tab.append(flux_momentum_x)
    meanx = np.mean(flux_momentum_x_tab[max(1, it-Naveraging):it])
    flux_momentum_x_av_tab.append(meanx)
    
    flux_momentum_y_tab.append(flux_momentum_y)
    meany = np.mean(flux_momentum_y_tab[max(1, it-Naveraging):it])
    flux_momentum_y_av_tab.append(meany)
    
    Temp = np.mean(U**2+V**2)
    T_tab.append(Temp)
    print('temp427')
# Plots
    if fig1 == 1:
        f = plt.figure(1)
        plt.clf()  # supprime les positions précédentes dans la figure
        plt.plot([Lx, Lx], [0, Ly], 'm')
        plt.plot([0, 0], [0, Ly], 'g')
        plt.plot(X[:Nparticules_1], Y[:Nparticules_1], color_1 + 'o', markersize=diameter_for_plots)
        plt.plot(X[Nparticules_1+1:Nparticules], Y[Nparticules_1+1:Nparticules], color_2 + 'o', markersize=diameter_for_plots)
        if fig3 == 1:
            for i in range(1, 10):
                plt.axvline(i*Lx/10, color='k', linestyle='--')
        if fig6+fig2 > 0:
            plt.plot([Lx, Lx], [0, Ly], 'm')
            plt.plot([X_fictive_surface, X_fictive_surface], [0, Ly], 'c--')
        plt.axis('square')
        plt.axis([0, Lx, 0, Ly])
        plt.pause(0.01)
        #plt.show()

    print('fin figure 1')
    # fonctions
    def mod(x,y):
        return x % y
    
    # Figure 2 : Pression sur la frontière droite
    if mod(t/dt,stepfig) == 0:
        if fig2 == 1:
            plt.figure(2)
            plt.subplot(4,1,1)
            plt.plot(t_tab,Fx_tab,'k:',t_tab,P_tab,'m')
            plt.title('Force de pression sur la frontière droite (instantanée et moyennée)')
            plt.subplot(4,1,2)
            plt.plot(t_tab,Fx_left_tab,'k:',t_tab,P_left_tab,'g')
            plt.title('Force de pression sur la frontière gauche (instantanée et moyennée)')
            plt.subplot(4,1,3)
            plt.plot(t_tab,flux_momentum_x_tab,'k:',t_tab,flux_momentum_x_av_tab,'c')
            plt.title('Flux de moment horizontal à travers le plan médian (instantané et moyenné)')
            plt.subplot(4,1,4)
            plt.plot(t_tab,P_tab,'m',t_tab,P_left_tab,'g',t_tab,flux_momentum_x_av_tab,'c')
            plt.title('Pression : comparaison de trois estimations')
            plt.pause(0.01)
            #plt.show()
            print('478')
    #Figure 6 : STRESS sur la frontière droite
    if mod(t/dt,stepfig) == 0:
        if fig6 == 1:
            plt.figure(6)
            plt.subplot(4,1,1)
            plt.plot(t_tab,Fy_tab,'k:',t_tab,Tauxy_tab,'m')
            plt.title('Contrainte tangentielle sur la frontière droite (instantanée et moyennée)')
            plt.subplot(4,1,2)
            plt.plot(t_tab,Fy_tab,'k:',t_tab,Tauxy_left_tab,'g')
            plt.title('Contrainte tangentielle sur la frontière gauche (instantanée et moyennée)')
            plt.subplot(4,1,3)
            plt.plot(t_tab,flux_momentum_y_tab,'k:',t_tab,flux_momentum_y_av_tab,'c')
            plt.title('Flux de moment vertical à travers le plan médian (instantané et moyenné)')
            plt.subplot(4,1,4)
            plt.plot(t_tab,Tauxy_tab,'m',t_tab,Tauxy_left_tab,'g',t_tab,flux_momentum_y_av_tab,'c')
            plt.title('valeurs moyennées pour le flux du plan médian et la frontière droite')
            plt.pause(0.01)
    # Figure 3
# Initialize the arrays to store the results
    Vmoy_bin = np.zeros(2*Nbin)
    Umoy_bin = np.zeros(2*Nbin)
    T_bin = np.zeros(2*Nbin)
    N1_bin = np.zeros(2*Nbin)

    for i in range(Nbin):
        Vmoy_bin[2*i] = sum(V[(X>(i)/10) & (X<(i+1)/10)]) / sum((X>(i)/10) & (X<(i+1)/10))
        Vmoy_bin[2*i+1] = Vmoy_bin[2*i]
        Umoy_bin[2*i] = sum(U[(X>(i)/10) & (X<(i+1)/10)]) / sum((X>(i)/10) & (X<(i+1)/10))
        Umoy_bin[2*i+1] = Umoy_bin[2*i]
        T_bin[2*i] = (sum(((V[(X>(i)/10) & (X<(i+1)/10)]-Vmoy_bin[2*i])**2) + ((U[(X>(i)/10) & (X<(i+1)/10)]-Umoy_bin[2*i])**2)) / sum((X>(i)/10) & (X<(i+1)/10)))
        T_bin[2*i+1] = T_bin[2*i]
        N1_bin[2*i] = sum((X[:Nparticules_1]>(i)/10) & (X[:Nparticules_1]<(i+1)/10))
        N1_bin[2*i+1] = N1_bin[2*i]
    #Figure 3
    if fig3 == 1:
      fig = plt.figure(3)
      fig.clf()
    # Histogram of particle positions
      ax1 = fig.add_subplot(2,2,1)
      if color_1 == color_2:
          ax1.hist(X[:Nparticules],alpha=0.5, bins=np.arange(0,1.1,0.1),edgecolor = 'black')
      else:
        ax1.hist(X[:Nparticules_1], bins=np.arange(0,1.1,0.1),edgecolor = 'black')
        ax1.hist(X[Nparticules_1:Nparticules],color='r',alpha=0.5, bins=np.arange(0,1.1,0.1),edgecolor = 'black')
        ax1.hist(X, bins=np.arange(0,1.1,0.1),alpha=0.5, histtype='step',edgecolor = 'red')
      ax1.set_title('Histogramme des positions des particules ')
      ax1.set_xlim([0,1])
      ax1.set_ylim([0, Nparticules/Nbin*1.2])
      
      plt.subplot(2, 2, 2)
      plt.cla()  # Equivalent of `hold off` in Matlab
      plt.plot(x_bin,T_bin, 'g')
      plt.plot([x_bin[0], x_bin[-1]], [sum(T_bin)/(2*Nbin), sum(T_bin)/(2*Nbin)], 'g:')
      plt.title('"Temperature" (mean square velocity)')
      plt.axis([0, 1, 0, max(T1,T2)*1.5])

      print('fig3 2')
    # Mean vertical velocity
      plt.subplot(2, 2, 3)
      plt.plot(x_bin, Vmoy_bin, 'c')
      plt.plot([x_bin[0], x_bin[-1]], [sum(Vmoy_bin)/(2*Nbin), sum(Vmoy_bin)/(2*Nbin)], 'c:')
      plt.axis([0, 1, min([Vm1, Vm2, Vwall_left, Vwall_right])-0.2*max(vq1, vq2), max([Vm1, Vm2, Vwall_left, Vwall_right])+0.2*max(vq1, vq2)])
      plt.title('mean vertical velocity')
      
    
    # Mean horizontal velocity
      plt.subplot(2, 2, 4)
      plt.plot(x_bin, Umoy_bin, 'm')
      plt.plot([x_bin[0], x_bin[-1]],[sum(Umoy_bin)/(2*Nbin), sum(Umoy_bin)/(2*Nbin)], 'm:')
      plt.title('mean horizontal velocity')
      plt.axis([0, 1, min(Um1, Um2)-0.2*max(vq1, vq2), max(Um1, Um2)+0.2*max(vq1, vq2)])
      
      plt.pause(0.01)
      #plt.show()
        # Récupérez la taille de l'écran
    screenSize = pylab.get_current_fig_manager().window.screen().size()
        
        # Calculez la taille de chaque figure
    figureWidth = screenSize.width() //2  # la moitié de la largeur de l'écran
    figureHeight = screenSize.height() //2.5  # la moitié de la hauteur de l'écran
    fig1_height = int(figureHeight * 2.5)  # la hauteur de la figure 1 est de 70% de la hauteur totale
    fig2_height = int(figureHeight * 1.3)  # la hauteur de la figure 2 est de 20% de la hauteur totale
    fig3_height = int(figureHeight * 1.1)  # la hauteur de la figure 3 est de 10% de la hauteur totale
    fig6_height = int(figureHeight * 1.3)  # la hauteur de la figure 3 est de 10% de la hauteur totale

    # Définir les positions et tailles de chaque figure
    if fig3 == 1:
        plt.figure(3).canvas.manager.window.setGeometry(0, 0, figureWidth, 2.5*figureHeight)  
    if fig2 == 1:
        plt.figure(2).canvas.manager.window.setGeometry(figureWidth, 0,1.2* figureWidth, fig2_height)  # moitié droite, en haut
    if fig1 == 1:
        plt.figure(1).canvas.manager.window.setGeometry(figureWidth, fig2_height, figureWidth, fig3_height)  # moitié droite, en bas
    if fig6 == 1:
        plt.figure(6).canvas.manager.window.setGeometry(figureWidth, 0,1.2* figureWidth, fig2_height)  # moitié droite, en haut
    # Afficher les figures
    plt.show()

if it != 1 and pausemode == 0:
 time.sleep(0.001) # laisse 1 milliseconde pour remettre à jour les figures
else:
 input('Position the figures and press "enter" to launch the simulation : ')
