# -*- coding: utf-8 -*-
"""
Created on Tue Nov  8 07:41:15 2022

@author: F. Moulin + D. Fabre + T. Mouyen
"""
import gc
import time

import math
import cmath
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as anim

gc.collect()
plt.close('all')

# Donnees physiques du probleme

print('Nombre de Stokes (par ex. 0.001 ou 1000) : ')
St = float(input());
#St = 10
h   = 1.;
nu  = 1.;
K   = 1.;
rho = 1.;
omega = 2*nu/(h*h)*St;
T   = 2*cmath.pi/omega;


# Definition de la fonction Utheo : 
def Utheo(y,t,omega,h,nu,K,rho):
    delta   = cmath.sqrt(2*nu/omega)
    tmp     = cmath.cosh(complex(1,1)*y/delta)/cmath.cosh(complex(1.0,1.0)*h/delta)
    tmp2    = complex(0.0,1.0)*K/(rho*omega)*cmath.exp(complex(0.0,1.0)*omega*t)*(1.0-tmp)
    return tmp2.real

# Definition de la fonction Tautheo :
def Fxtheo(t,omega,h,nu,K):
    delta   = cmath.sqrt(2*nu/omega)
    tmp     = complex(1.,1.)/delta*cmath.tanh(complex(1.,1.)*h/delta)
    tmp2    = 2*rho*nu*complex(0.,1.)*K/omega * cmath.exp(complex(0.,1.)*omega*t)*tmp
    return tmp2.real


# Figure 1 : frottement parietal taup(t)
t   = np.linspace(0,2*T,100)
Fx  = np.zeros(100,'float')
for i in range(100):
    Fx[i] = Fxtheo(t[i],omega,h,nu,K) 

plt.plot(t/T,Fx)
plt.xlabel('$t/T$')
plt.ylabel('$F_x(t)$')
time.sleep(1)
plt.show()


# Figure 2 : profil u(y,t) et animation
Npts    = 500       # nombre de points
y       = np.linspace(-1.0,1.0,Npts)
Nt      = 30 # nombre de pas de temps affiches sur une periode
tloop   = np.linspace(0,T,Nt)
u       = np.zeros((Nt,Npts),'float')
ug      = np.zeros(Npts,'float')
for i in range(Nt):
    tloc = tloop[i]
    for j in range(Npts):
        yloc = y[j]
        u[i,j] = Utheo(yloc,tloc,omega,h,nu,K,rho)
                 

fig, ax = plt.subplots()
ax.set_title('vitesse a t = '+str(tloop[0]))
ax.set_xlabel('$u$')
ax.set_ylabel('$y/h$')
ax.set_xlim([np.min(u), np.max(u)])
line, = ax.plot(u[0,:],y)

def update(frame):
    # for each frame, update the data stored on each artist.
    x = u[frame,:]
    # update the line plot:
    line.set_data(x,y)
    ax.set_title('vitesse a t/T = '+str(np.round(tloop[frame]/T,3)))
    return line

ani = anim.FuncAnimation(fig=fig, func=update, frames=Nt, interval=250, repeat=True)
plt.show()



'''
# NB autre methode pour faire l'animation 
for i in range(Nt):
    for j in range(Npts):
        ug[j] = u[i,j]
    plt.plot(ug,y)
    if (i>-1):
        tloc = tloop[i]
        plt.xlabel('$u$')
        plt.ylabel('$y/h$')
        plt.title('vitesse a t='+str(tloc))
        plt.axis([-K/(rho*omega)*1.1,K/(rho*omega)*1.1,-1.0,1.0])
    time.sleep(0.1)
    plt.show() # affiche la figure à l'écran
'''