
#   --------------------------------------------------------------------
#  |        TRACE DES CHAMPS DE VITESSES DU VORTEX DE RANKINE           |
#  |                  Version du 30 janvier 2025   (clic on figures)  |
#  |           TD n1 CINEMATIQUE                                        |
#  |                     # -*- coding: utf-8 -*-                        |
#   --------------------------------------------------------------------

""" 
L'objet de ce programme est de représenter le champ Eulérien de vitesse V d'un 
écoulement de vidange, qui s'écrit: 
   Vr = -D r / (2 pi a^2)  ; Vt = K r / (2 pi a^2) ; Vz = D z / (pi a^2) pour r < a 
et Vr = - D / ( 2 pi r)    ; Vt = K / (2 pi r)     ; Vz = 0              pour a < r < Ro 
D et K  sont le débit et la circulation 
"""

#----------------------------- MODULES IMPORTES -------------------------------
import matplotlib.pyplot as plt           # Pour tracer
import numpy as np                        # Pour calculer
from numpy import*                        # Pour construire les tableaux
from matplotlib.pyplot import*            # Pour construire le graphe
from scipy.integrate import odeint
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
close ("all")
# -----------------------------------------------------------------------------

#--------------------------- ENTREE DES DONNEES -------------------------------
global D,G,a,H,R0
# Définition des dimensions (fixées)
taille = 3 #taille du domaine représenté
R0 = 3     # rayon de la cuve
a = 1      # rayon de l'orifice de vidange
H = 3      # hauteur de la cuve
#taille = int(input("Entrer la taille t du graphe (-t < x < t et -t < y < t) : "))
# Taille d la cuve
#R0 = float(input("Entrer le rayon R > 0 de la cuve : "))
# Taille du trou
#a = float(input("Entrer le rayon a > 0 du trou : "))
# Définition des constantes
#D = float(input("Entrer la constante D (débit): "))
#G = float(input("Entrer la constante K (circulation): "))
D =.5 
G = 1



# -----------------------------------------------------------------------------

#------------------------- DEFINITION DES VECTEURS ----------------------------
# Définition de la figure (2  graphes sur 2 colonnes)
plt.close("all")
plt.figure(1);
fig,ax = plt.subplots(figsize = (6,6))

# Définition des vecteurs
x = linspace(-taille, taille, 31) # nbre de points de grille entre x = -taille et x = taille  = taille/2
y = x
#z = linspace(0,H,31)
Xgrid,Ygrid = np.meshgrid(x,y)

Uh = np.zeros(Xgrid.shape)             # Matrice nulle
Vh = np.zeros(Xgrid.shape)             # Matrice nulle


# Definition du champ de vitesse via une fonction a valeur vectorielle
# Remarque : dans la définion D et G correspondent en réalité à (D/2pi) et (Gamma/2pi)
def Vitesse(X,t):
    global D,G,a,H,R0
    x,y,z=X 
    r = sqrt(x*x+y*y)
    theta = np.arctan2(y,x)
    if z>H:
        ur,ut,uz = 0,0,0
    elif r<a:
       ur = -D/(a*a)*r
       ut = G/(a*a)*r
       uz = 2*D*z
    elif r<=R0+.01:
       ur = -D/r
       ut = G/r
       uz = 0
    else:
       ur,ut,uz = 0,0,0
    ux =  ur*np.cos(theta) - ut*np.sin(theta)
    uy =  ur*np.sin(theta) + ut*np.cos(theta)
    return [ux,uy,uz]



def cylinder_surface(radius,H):
    # pour tracer les cylindres sur la figure
    theta = np.linspace(0, 2 * np.pi, 100)
    z = np.linspace(0, H, 100)
    thetaG,zG = np.meshgrid(theta,z)  
    x = radius * np.cos(thetaG)
    y = radius * np.sin(thetaG)
    return x, y, zG

# Création de la figure
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

# Tracé du premier cylindre (rayon a)
x1,y1,z1 = cylinder_surface(a,H)
ax.plot_surface(x1, y1, z1, color='blue', alpha=0.4, rstride=10, cstride=10, antialiased=True)

# Tracé du deuxième cylindre (rayon R0)
x2, y2, z2 = cylinder_surface(R0,H)
ax.plot_surface(x2, y2, z2, color='red', alpha=0.2, rstride=10, cstride=10, antialiased=True)




# Définition de certains paramètres affectant le comportement des figures au clic
Tmax = 20; # temps maximal pour l'intégration de la trajectoire
Tmaxplot = 5 ;# temps maximal pour l'affichage dans la figure
Tsquare = 0.3; # temps de déformation du carré
h = 0.5 ;# taille du carré
button = 1
compteur = 0
compteur2 = 0


# points de departs pour trace trajectoire
PointsT = np.array([
    [0, R0, .2], 
    [0, -R0, .2],[R0,0, 0],[-R0, 0,0]
])


# Définition des 8 sommets du parallélépipède de départ
R1 = 2;
points = np.array([
    [R1-h/2,    h/2, .5-h/2],  # Sommet 0
    [R1+h/2,  h/2, .5-h/2],  # Sommet 1
    [R1+h/2, -h/2, .5-h/2],  # Sommet 2
    [R1-h/2,   -h/2, .5-h/2],  # Sommet 3
    [R1-h/2,    h/2, .5+h/2],  # Sommet 4
    [R1+h/2,  h/2, .5+h/2],  # Sommet 5
    [R1+h/2, -h/2, .5+h/2],  # Sommet 6
    [R1-h/2,   -h/2, .5+h/2]   # Sommet 7
])


def TraceVolume(points):
  # Définition des faces du parallélépipède (chaque face est définie par une liste de sommets)
  faces = [
    [points[j] for j in [0, 1, 2, 3]],  # Face inférieure
    [points[j] for j in [4, 5, 6, 7]],  # Face supérieure
    [points[j] for j in [0, 1, 5, 4]],  # Face avant
    [points[j] for j in [2, 3, 7, 6]],  # Face arrière
    [points[j] for j in [1, 2, 6, 5]],  # Face droite
    [points[j] for j in [0, 3, 7, 4]]   # Face gauche
  ]
  # Création du parallélépipède
  poly3d = Poly3DCollection(faces, alpha=0.5, facecolors='cyan', edgecolors='r')
  ax.add_collection3d(poly3d)
  plt.show()
  
  

# Fonction qui sera appelée lorsqu'on clique sur la figure
def on_click(event):
    global compteur,compteur2,points,PointsT;
    if event.button == 1:
        # Left-click: drawing of a few trajectories to illustrate the flow
        xinit = PointsT[compteur,:];
        print("Trace d'une trajectoire pour xinit = "+str(xinit))
        t = np.linspace(0, Tmax, 1000)
        xtraj = odeint(Vitesse, xinit, t)
        #print(xtraj[:,2])
        plt.plot(xtraj[:,0], xtraj[:,1],xtraj[:,2], 'b')
        plt.pause(0.01);
        plt.show()
        compteur = min(compteur+1,3);
    elif event.button == 3:
        if (compteur2==0):
            # Au premier clic : detection du point et rectangle initial 
            TraceVolume(points)
        # Right-click: drawing the deformation of a square
        else:
           Tsquare = 1.5
           t = np.linspace(0, Tsquare, 1000)
           for j in range(0,8):
               xtraj = odeint(Vitesse, points[j], t)
               points[j] = xtraj[-1,:]
           TraceVolume(points)   
           
        compteur2 = compteur2+1;
        print("Volume materiel, instant "+str(compteur2))
  #      fig.canvas.mpl_connect('button_press_event', on_click)
        plt.pause(0.01);
        plt.show()
# Association de la fonction on_click à la figure
fig.canvas.mpl_connect('button_press_event', on_click)



     




# -----------------------------------------------------------------------------

#--------------------------- SAUVEGARDE DES GRAPHES ---------------------------
chemin_dossier = "./"
chemin_image = chemin_dossier + "Vidange_C.png" 
plt.savefig(chemin_image, bbox_inches='tight') # Sauvegarde

plt.show()                                     # Affichage graphe  
#plt.close()
# -----------------------------------------------------------------------------



#----------------------------------- F I N ------------------------------------