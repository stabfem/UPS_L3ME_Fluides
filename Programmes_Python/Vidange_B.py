
#   --------------------------------------------------------------------
#  |        TRACE DES CHAMPS DE VITESSES DU VORTEX DE RANKINE           |
#  |                  Version du 30 janvier 2025   (clic on figures)  |
#  |           TD n1 CINEMATIQUE                                        |
#  |                     # -*- coding: utf-8 -*-                        |
#   --------------------------------------------------------------------

""" 
L'objet de ce programme est de représenter le champ Eulérien de vitesse V d'un 
écoulement de vidange, qui s'écrit: 
   Vr = -D r / (2 pi a^2)  ; Vt = K r / (2 pi a^2) ; Vz = D z / (pi a^2) pour r < a 
et Vr = - D / ( 2 pi r)    ; Vt = K / (2 pi r)     ; Vz = 0              pour a < r < Ro 
D et K  sont le débit et la circulation 
"""

#----------------------------- MODULES IMPORTES -------------------------------
import matplotlib.pyplot as plt           # Pour tracer
import numpy as np                        # Pour calculer
from numpy import*                        # Pour construire les tableaux
from matplotlib.pyplot import*            # Pour construire le graphe
from scipy.integrate import odeint
# -----------------------------------------------------------------------------

#--------------------------- ENTREE DES DONNEES -------------------------------
global D,G,a,H,R0
close("all")
# Définition des dimensions (fixées)
taille = 3 #taille du domaine représenté
R0 = 3     # rayon de la cuve
a = 1      # rayon de l'orifice de vidange
H = 3      # hauteur de la cuve
#taille = int(input("Entrer la taille t du graphe (-t < x < t et -t < y < t) : "))
# Taille d la cuve
#R0 = float(input("Entrer le rayon R > 0 de la cuve : "))
# Taille du trou
#a = float(input("Entrer le rayon a > 0 du trou : "))
# Définition des constantes
# Ce programme ne marche que pour G=0,D=1
G = 0
D = 1



# -----------------------------------------------------------------------------

#------------------------- DEFINITION DES VECTEURS ----------------------------
# Définition de la figure (2  graphes sur 2 colonnes)
plt.close("all")
fig,ax = plt.subplots(figsize = (6,6))

# Définition des vecteurs
x = linspace(0, R0, 31) # nbre de points de grille entre x = -taille et x = taille  = taille/2
z = linspace(0,H,31)
Xgrid,Zgrid = np.meshgrid(x,z)

Uv = np.zeros(Xgrid.shape)             # Matrice nulle
Wv = np.zeros(Xgrid.shape)             # Matrice nulle


# Definition du champ de vitesse via une fonction a valeur vectorielle
# Remarque : dans la définion D et G correspondent en réalité à (D/2pi) et (Gamma/2pi)
def Vitesse(X,t):
    global D,G,a,H,R0
    x,y,z=X 
    r = sqrt(x*x+y*y)
    theta = np.arctan2(y,x)
    if r<a:
       ur = -D/(a*a)*r
       ut = G/(a*a)*r
       uz = 2*D*z
    elif r<R0:
       ur = -D/r
       ut = G/r
       uz = 0
    else:
       ur = 0
       ut = 0
       uz = 0
    ux =  ur*np.cos(theta) - ut*np.sin(theta)
    uy =  ur*np.sin(theta) + ut*np.cos(theta)
    return [ux,uy,uz]


# Boucles de calcul du champ vectoriel en coupe horizontale   
for i in range(Xgrid.shape[0]):
    for j in range(Xgrid.shape[1]):
        # Calcul du vecteur vitesse
        XX = (Xgrid[i,j],0,Zgrid[i,j])
        V = Vitesse(XX,0)
        Uv[i,j] = V[0]
        Wv[i,j] = V[2]
        
            
# Définition du graphe "0" : Vitesses horizontales  
ax.quiver(Xgrid, Zgrid, Uv, Wv, color="r", angles='xy',
          scale_units='xy', scale=7, width=0.0025,label = 'Champ de vecteurs')
ax.set_ylim(0, 4)

# Définition de certains paramètres affectant le comportement des figures au clic
Tmax = 10; # temps maximal pour l'intégration de la trajectoire
Tmaxplot = 5 ;# temps maximal pour l'affichage dans la figure
Tsquare = 0.3; # temps de déformation du carré
h = 0.5 ;# taille du carré
button = 1
compteur = 0


# Fonction pour générer les points sur les côtés d'un carré
def generer_points_carre(A,B,C,D):
    NUM = 10
    points_AB = [A + t * (B - A) for t in np.linspace(0, 1, NUM)]
    points_BC = [B + t * (C - B) for t in np.linspace(1/NUM, 1, NUM)]
    points_CD = [C + t * (D - C) for t in np.linspace(1/NUM, 1, NUM)]
    points_DA = [D + t * (A - D) for t in np.linspace(1/NUM, 1, NUM)]
    points = np.array(points_AB + points_BC + points_CD + points_DA)
    return points


# Fonction qui sera appelée lorsqu'on clique sur la figure
def on_click(event):
    global compteur,xA,xB,xC,xD,SQ;
    if event.button == 1:
        # Left-click: drawing of a few trajectories to illustrate the flow
        xinit2D = np.array([event.xdata, event.ydata])
        xinit = (xinit2D[0],0,xinit2D[1])
        t = np.linspace(0, Tmax, 1000)
        xtraj = odeint(Vitesse, xinit, t)
        plt.plot(xtraj[:,0], xtraj[:,2], 'b', xtraj[0,0], xtraj[0,2], 'bo')
        fig.canvas.mpl_connect('button_press_event', on_click)
        plt.pause(0.01);
        plt.show()
        compteur = 0;
    elif event.button == 3:
        if (compteur==0):
            # Au premier clic : detection du point et rectangle initial 
            xclick,yclick = event.xdata,event.ydata
            xA = np.array([xclick-h/2,0,yclick+h/2]);
            xB = np.array([xclick-h/2,0,yclick-h/2]);
            xC = np.array([xclick+h/2,0,yclick-h/2]);
            xD = np.array([xclick+h/2,0,yclick+h/2]);
            compteur = 1;
            SQ = generer_points_carre(xA,xB,xC,xD)
            plt.plot(SQ[:,0], SQ[:,2])
        # Right-click: drawing the deformation of a square
        else:
            t = np.linspace(0, Tsquare, 1000)
            for j in range(shape(SQ)[0]):
               xtraj = odeint(Vitesse, SQ[j,:], t)
               SQ[j,:] = xtraj[-1,:]   
        plt.plot(SQ[:,0], SQ[:,2])
        fig.canvas.mpl_connect('button_press_event', on_click)
        plt.pause(0.01);
        plt.show()
# Association de la fonction on_click à la figure
fig.canvas.mpl_connect('button_press_event', on_click)



     

#---------------- TRACER LES LIMITES DU TROU ET DE LA CUVE --------------------

theta = np.linspace(0, 2*np.pi, 100)
x1 = a*np.cos(theta)
x2 = a*np.sin(theta)
x3 = R0*np.cos(theta)
x4 = R0*np.sin(theta)

#ax.plot(x1, x2, ls='--', c = 'k', label = "Limite du trou")
#ax.plot(x3, x4, ls='-', c = 'black', label = "Limite de la cuve")
#ax.set_xlabel('X')                        # Légende des abscisses graphe "0"
#ax.set_ylabel('Y')                        # Légende des ordonnées graphe "0"
#ax.legend (loc ="lower left")            # Légende en bas à gauche

# -----------------------------------------------------------------------------

#---------------------------- TRACE DES GRAPHES -------------------------------

ax.set_title("Drain flow, vertical view \n Left click : trajectrory ;  \n  Right click : deformation of a material volume",
           fontsize=12,
           loc='center',                      # Centré
           fontweight='bold',                 # En gras
           style='italic',                    # En italique
           family='monospace',                # Caractères monospace
           pad = 10.0,                        # Distance entre titre et graphe
           color = 'b')                       # En bleu



# -----------------------------------------------------------------------------

#--------------------------- SAUVEGARDE DES GRAPHES ---------------------------
chemin_dossier = "./"
chemin_image = chemin_dossier + "Vidange_B.png" 
plt.savefig(chemin_image, bbox_inches='tight') # Sauvegarde

plt.show()                                     # Affichage graphe  
#plt.close()
# -----------------------------------------------------------------------------


if 0:
 figure(2)
 xclick = 2
 yclick = 2
 xA = np.array([xclick-h/2,0,yclick+h/2]);
 xB = np.array([xclick-h/2,0,yclick-h/2]);
 xC = np.array([xclick+h/2,0,yclick-h/2]);
 xD = np.array([xclick+h/2,0,yclick+h/2]);
 compteur = 1;
 SQ = generer_points_carre(xA,xB,xC,xD)
 plt.plot(SQ[:,0], SQ[:,2])

 t = np.linspace(0, Tsquare, 1000)
 for j in range(shape(SQ)[0]):
    xtraj = odeint(Vitesse, SQ[j,:], t)
    SQ[j,:] = xtraj[-1,:]
     
 plt.plot(SQ[:,0], SQ[:,2])    
    
    
 t = np.linspace(0, Tsquare, 1000)
 for j in range(shape(SQ)[0]):
     xtraj = odeint(Vitesse, SQ[j,:], t)
     SQ[j,:] = xtraj[-1,:]
   
     
 plt.plot(SQ[:,0], SQ[:,2])    





#----------------------------------- F I N ------------------------------------