#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on May 2022

@author: Ilias Sadouni (étudiant L3 Méca UPS)
  (Adapted from a Matlab program by D. Fabre)
"""

#Programme d'illustration de l'ecoulement potentiel autour d'un cylindre
#Figure 1 : champ de vitesse, lignes de courant, isopotentielles.
#Figure 2 : pression et vitesse le long de la surface
#Figure 3 : champ de pression et resultante sur le cylindre
from numpy import *
from matplotlib.pyplot import *
close('all')
print('\n Trace le champ de vecteur, les isopotentielles et quelques lignes de courant \n')
print('Pour l ecoulement autour d un cylindre \n \n')
#Valeur des parametres a, U, Gammma/2pi
a = 1
U = float(input('U = '))
Gamma = float(input('Gamma/2pi = '))
#Maillage de l'espace en cordonnees polaires
#maillage fin pour Phi et Psi
[r, theta] = meshgrid(linspace(1, 5, 100), linspace(-pi, pi, 100))
x = r*cos(theta)
y = r*sin(theta)
#Definition du potentiel et du champ de vitesse
10
Phi = U*(r+1/r)*cos(theta)+Gamma*theta #sym + antisym, a=1
Psi = U*(r-1/r)*sin(theta)-Gamma*log(r)
ur = U*(1-1/r**2)*cos(theta)
ut = -U*(1+1/r**2)*sin(theta)+Gamma/r
ux = ur*cos(theta)-ut*sin(theta)
uy = ut*cos(theta)+ur*sin(theta)
p = -(ut**2+ur**2-U**2)/2
#Trace des lignes de courant psi=constante et des isopotentielles
figure(1)
plot(cos(linspace(-pi, pi, 50)), sin(linspace(-pi, pi, 50)), 'k')
title('Ecoulement potentiel autour d un cylindre : isopotentielles (rouge),\nlignes de courant (magenta) et champ de vitesse (bleu)')
levels = linspace(-10, 10, 21)
contour(x, y, Phi, levels=levels, colors='r', linestyles='dotted') #colors pour avoir la meme couleur
contour(x, y, Psi, levels=levels, colors='m')
xlabel('x')
ylabel('y')
print('axis equal tight')
#Maillage grossier pour tracer le champ de vecteurs
[rG, thetaG] = meshgrid(linspace(1, 5, 10), linspace(-pi, pi, 17))
xG = rG*cos(thetaG)
yG = rG*sin(thetaG)
Phi = U*(rG+1/rG)*cos(thetaG)+Gamma*thetaG
urG = U*(1-1/rG**2)*cos(thetaG)
utG = -U*(1+1/rG**2)*sin(thetaG)+Gamma/rG
uxG = urG*cos(thetaG)-utG*sin(thetaG)
uyG = utG*cos(thetaG)+urG*sin(thetaG)
quiver(xG, yG, uxG, uyG, color='b')
#recherche des points d'arret
if U==0:
    thetaA = []
    xA = 0
    yA = 0
elif abs(Gamma/(2*(U*a)))<1:
    thetaA = [arcsin(Gamma/(2*U*a))%(2*pi),(pi-arcsin(Gamma/(2*U*a)))%(2*pi)]
    xA = a*cos(thetaA)
    yA = a*sin(thetaA)
elif abs(Gamma/(2*(U*a)))==1:
    thetaA = (pi/2*sign(Gamma))%(2*pi)
    xA = a*cos(thetaA)
    yA = a*sin(thetaA)
else:
    thetaA = []
    xA = 0
    yA = (1/2)*(Gamma+sign(Gamma)*sqrt(-4*U^2*a**2+Gamma**2))/U
plot(xA, yA, 'bo')
savefig('Cylindre_Psi_U.png', dpi='figure') #generate graphical file
figure(2)
thetaS = linspace(0,2*pi,101)
utS = -2*U*sin(thetaS)+Gamma
pS = -(utS**2-U**2)/2 #equation de Bernouilli : p(a,theta) + ut(a,theta)^2/2 = p0 + U^2/2
plot(thetaS, utS, 'b', thetaS, pS, 'g', thetaA, zeros(size(thetaA)),'bo')
plot(thetaS, zeros(size(thetaS)), 'k:')
title('Champ de vitesse ut(a,theta) (bleu) et\nchamp de pression p(a,theta) a la surface')
xlabel('theta')
ylabel('u,p')
savefig('Cylindre_Surface_U.png', dpi='figure') #generate graphical file
figure(3)
plevels = linspace(p.min(),p.max(),15)
contour(x, y, p, levels=plevels, colors='g', linestyles='dashed')
ppos = p*(p>=0)
contour(x, y, ppos, levels=plevels, colors='g')
title('Champ de pression (vert) et force exercee sur l objet (rouge) \n (pointilles : valeurs negatives; traits pleins : valeurs positives)')
plot(cos(linspace(-pi, pi, 50)), sin(linspace(-pi, pi, 50)), 'k')
quiver(cos(thetaS), sin(thetaS), -pS*cos(thetaS), -pS*sin(thetaS), color='r')
plot(xA,yA,'bo')
print('axis equal tight')
xlabel('x'); ylabel('y')
axis([-2, 2, -2, 2])
savefig('Cylindre_Force_U.png', dpi='figure') #generate graphical