# -*- coding: utf-8 -*-
"""
Created on Mon Jan  8 21:10:25 2024

@author: bardan
"""

from sympy import *
import numpy as np

W=1
g=10
Q=20*10**(-3)
hmin=0.01
hmax=0.12

h = symbols('h')

F=g*h+Q**2/(2*W**2*h**2)



plot(F, (h, hmin, hmax))

hc=np.cbrt(Q**2/(g*W**2))
print("hc=",hc)

Fc=F.subs(h,hc)
print("Fc=",Fc)

b=0.04
H=0.1

droite=g*(H-b)+Q**2/(2*W**2*H**2)
print("droite=", droite)