
from sympy import *



R,rho,g, P_A = symbols('R,rho,g,P_A')
theta, phi = symbols('theta,phi')


u = (-rho*g*(R*cos(theta)+R)+P_A)*cos(theta)*R**2*sin(theta)


R = integrate(u,(theta, 0, pi), (phi, 0, 2*pi))
print(simplify(R))