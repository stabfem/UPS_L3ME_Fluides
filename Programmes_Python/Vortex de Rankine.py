
#   --------------------------------------------------------------------
#  |        TRACE DES CHAMPS DE VITESSES DU VORTEX DE RANKINE           |
#  |                  Créé le mardi 20 Janvier  2024 
#           TD n1 CINEMATIQUE                    |
#  |                     # -*- coding: utf-8 -*-                        |
#   --------------------------------------------------------------------

""" 
L'objet de ce programme est de représenter le champ Eulérien de vitesse V du
Vortex de Rankine, qui s'écrit : 
   Vr = -Dr/2pia² ; Vthêta = Kr/2pia² ; Vz = Dz/pia² pour r < a 
et Vr = -D/2pir ; Vthêta = K/2pir ; Vz = 0 pour a < r < Ro 
D et K étant homogènes à une viscosité (en m²/s).
"""

#----------------------------- MODULES IMPORTES -------------------------------
import matplotlib.pyplot as plt           # Pour tracer
import numpy as np                        # Pour calculer
from numpy import*                        # Pour construire les tableaux
from matplotlib.pyplot import*            # Pour construire le graphe
# -----------------------------------------------------------------------------

#--------------------------- ENTREE DES DONNEES -------------------------------

# Définition de l'extension du graphe
taille = int(input("Entrer la taille t du graphe (-t < x < t et -t < y < t) : "))
# Taille d la cuve
R0 = float(input("Entrer le rayon R > 0 de la cuve : "))
# Taille du trou
a = float(input("Entrer le rayon a > 0 du trou : "))
# Définition des constantes
D = float(input("Entrer la constante D : "))
K = float(input("Entrer la constante K : "))
# -----------------------------------------------------------------------------

#------------------------- DEFINITION DES VECTEURS ----------------------------
# Définition de la figure (2  graphes sur 2 colonnes)
fig, ax = plt.subplots (nrows = 1, ncols = 2, figsize = (14,6)) # Figure a 2 graphes en ligne

# Définition des vecteurs
x = linspace(-taille, taille, taille) # nbre de points de grille entre x = -taille et x = taille  = taille/2

X,Y = np.meshgrid(np.arange(-taille +1, taille, 1), np.arange(-taille +2, taille-1, 1))
XX,Z = np.meshgrid(np.arange(-taille +1, taille, 1), np.arange(-taille +2, 0, 1))

x_shape = X.shape
xx_shape = XX.shape
z_shape = Z.shape

Uh = np.zeros(x_shape)             # Matrice nulle
Vh = np.zeros(x_shape)             # Matrice nulle
Uv = np.zeros(xx_shape)            # Matrice nulle
Vv = np.zeros(z_shape)             # Matrice nulle

# Boucles de calcul du champ vectoriel en coupe horizontale   
for i in range(x_shape[0]):
    for j in range(x_shape[1]):
        # Calcul du vecteur vitesse
        R2 = X[i,j]**2 + Y[i,j]**2
        if np.sqrt (R2) < a :                       # Si domaine r < a
            Uh[i,j] = (-D*X[i,j]-K*Y[i,j])/(a*a)       
            Vh[i,j] = (K*X[i,j]-D*Y[i,j])/(a*a)           
        elif np.sqrt (R2) < R0  :                   # Sinon pour a < r < Ro
            Uh[i,j] = -(1/R2)* (D*X[i,j]+K*Y[i,j])  
            Vh[i,j] = (1/R2)* (K*X[i,j]-D*Y[i,j])   
            
# Définition du graphe "0" : Vitesses horizontales  
ax [0].quiver(X, Y, Uh, Vh, color="r", angles='xy',
          scale_units='xy', scale=5, width=0.005,label = 'Champ de vecteurs')

# Boucles de calcul du champ vectoriel en coupe verticale   
for i in range(xx_shape[0]):
    for j in range(z_shape[1]):
        # Calcul du vecteur vitesse
        if XX[i,j]* XX[i,j] < a*a :                  # Si domaine r < a
            Uv[i,j] = (-D*XX[i,j])/(a*a)       
            Vv[i,j] = -2*D*Z[i,j]/(a*a)           
        elif X[i,j]* XX[i,j] < R0 *R0  :             # Sinon pour a < r < Ro
            Uv[i,j] = -(D*XX[i,j])*0.05  
            Vv[i,j] = 0                              # Vitesse verticale nulle
            
# Définition du graphe "1" : Vitesses verticales  
ax [1].quiver(XX, Z, Uv, Vv, color="r", angles='xy',
          scale_units='xy', scale=5, width=0.005,label = 'Champ de vecteurs')
# -----------------------------------------------------------------------------

#---------------- TRACER LES LIMITES DU TROU ET DE LA CUVE --------------------

theta = np.linspace(0, 2*np.pi, 100)
x1 = a*np.cos(theta)
x2 = a*np.sin(theta)
x3 = R0*np.cos(theta)
x4 = R0*np.sin(theta)

ax[0].plot(x1, x2, ls='--', c = 'blue', label = "Limite du trou")
ax[0].plot(x3, x4, ls='-', c = 'black', label = "Limite de la cuve")
ax[0].set_xlabel('X')                        # Légende des abscisses graphe "0"
ax[0].set_ylabel('Y')                        # Légende des ordonnées graphe "0"
ax [0].legend (loc ="lower left")            # Légende en bas à gauche

ax [1].plot([-a, -a], [-1, -taille + 1], 'b--', lw=2, label = "Limite du trou") # limite du trou
ax [1].plot([a, a], [-1, -taille + 1], 'b--', lw=2)
ax[1].set_xlabel('X')                        # Légende des abscisses graphe "1"
ax[1].set_ylabel('Profondeur Z')             # Légende des ordonnées graphe "1"
ax [1].legend (loc ="lower left")            # Légende en bas à gauche

# -----------------------------------------------------------------------------

#---------------------------- TRACE DES GRAPHES -------------------------------

ax[0].set_title("Coupe horizontale de l'écoulement",
           fontsize=12,
           loc='center',                      # Centré
           fontweight='bold',                 # En gras
           style='italic',                    # En italique
           family='monospace',                # Caractères monospace
           pad = 10.0,                        # Distance entre titre et graphe
           color = 'b')                       # En bleu


ax[1].set_title("Coupe verticale de l'écoulement",
           fontsize=12,
           loc='center',                      # Centré
           fontweight='bold',                 # En gras
           style='italic',                    # En italique
           family='monospace',                # Caractères monospace
           pad = 10.0,                        # Distance entre titre et graphe
           color = 'b')                       # En bleu 
# -----------------------------------------------------------------------------

#--------------------------- SAUVEGARDE DES GRAPHES ---------------------------
chemin_dossier = "C:\\Users\\gastjp\\Documents\\Python\\Images\\Rankine\\Champ_"
chemin_image = chemin_dossier + "vectoriel_des-vitesses.png" 
plt.savefig(chemin_image, bbox_inches='tight') # Sauvegarde

plt.show()                                     # Affichage graphe  
plt.close()
# -----------------------------------------------------------------------------

#----------------------------------- F I N ------------------------------------