# -*- coding: utf-8 -*-
"""
Created on Wed May  3 16:17:33 2023

@author: Taha-Yassine Sokakini & Oumaima El-Hassine (Etudiants M1 Méca UPS)
(adapted from a Matlab program by D. Fabre) 

* Remarque importante :
  Pour utiliser ce programme de manière optimale il faut modifier le mode graphique de Python.
  Pour cela (avec Spyder) :
  - Ouvrir le menu "Preferences / Ipython Console / Graphics"
  - Dans le menu "graphics backend" sélectionner "automatic"'
"""

import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt
plt.close("all")

print("Trace le champ de vecteur et les lignes de courant pour l'écoulement défini par :")
print("     u = a x + b y")
print("     v = c x + d y\n")
# Valeur des paramètres a, b, c, d :
a = float(input("a = "))
b = float(input("b = "))
c = float(input("c = "))
d = float(input("d = "))
A11 = a;
A12 = b;
A21 = c;
A22 = d;

div = a+d;
rot = (c-b);
print(f"\nDivergence : div u= (du/dy+dv/dy) = {div}")
print(f"Vorticité  : (rot u) . e_z = (dv/dx-du/dy) = {rot}\n")

# Tracé du champ de vecteur de l'écoulement

# Construction d'une grille de 21x21 points dans ces limites
xmin = -2; xmax = 2; ymin = -2; ymax = 2;
[xG, yG] = np.meshgrid(np.linspace(xmin, xmax, 21), np.linspace(ymin, ymax, 21))

# Calcul des composantes x et y du champ de vecteurs à partir des coefficients A11, A12, A21 et A22
ux = A11*xG + A12*yG
uy = A21*xG + A22*yG

# Affichage du champ de vecteurs
fig, ax = plt.subplots()
ax.quiver(xG, yG, ux, uy, color='b')
ax.set_xlim(xmin, xmax)
ax.set_ylim(ymin, ymax)

# Ajout d'un titre et d'une légende
ax.set_title('Illustration of a 2D linear flow.\nLeft-click to draw a trajectory; right-click to draw the deformation of a square')
ax.set_xlabel('x')
ax.set_ylabel('y')

# Affichage de la figure
plt.show()


# Définition de certains paramètres
Tmax = 100; # temps maximal pour l'intégration de la trajectoire
Tmaxplot = 5 ;# temps maximal pour l'affichage dans la figure 2
Tsquare = 0.2; # temps de déformation du carré
h = 1 ;# taille du carré
xA = [-h, -h]
xB = [-h, h]
xC = [h, h]
xD = [h, -h]
button = 1
compteur = 0


# Fonction qui sera appelée lorsqu'on clique sur la figure
def on_click(event):
    if event.button == 1:
        # Left-click: drawing of a few trajectories to illustrate the flow
        xinit = np.array([event.xdata, event.ydata])
        t = np.linspace(0, Tmax, 1000)
        xtraj = odeint(lambda x, t: [A11*x[0]+A12*x[1], A21*x[0]+A22*x[1]], xinit, t)
        plt.figure(1)
        plt.plot(xtraj[:,0], xtraj[:,1], 'r', xtraj[0,0], xtraj[0,1], 'ro')
        fig.canvas.mpl_connect('button_press_event', on_click)
        plt.xlim((xmin,xmax));plt.ylim((ymin,ymax))
        plt.pause(0.01);
        plt.show()

    elif event.button == 3:
        # Right-click: drawing the deformation of a square
        global xA,xB,xC,xD
        t = np.linspace(0, Tsquare, 1000)
        SQ = np.array([xA, xB, xC, xD, xA])
        plt.plot(SQ[:,0], SQ[:,1])
        xtraj = odeint(lambda x, t: [A11*x[0]+A12*x[1], A21*x[0]+A22*x[1]], xA, t)
        xA = xtraj[-1,:]
        xtraj = odeint(lambda x, t: [A11*x[0]+A12*x[1], A21*x[0]+A22*x[1]], xB, t)
        xB = xtraj[-1,:]
        xtraj = odeint(lambda x, t: [A11*x[0]+A12*x[1], A21*x[0]+A22*x[1]], xC, t)
        xC = xtraj[-1,:]
        xtraj = odeint(lambda x, t: [A11*x[0]+A12*x[1], A21*x[0]+A22*x[1]], xD, t)
        xD = xtraj[-1,:]
        SQ = np.array([xA, xB, xC, xD, xA])
        plt.figure(1)
        plt.plot(SQ[:,0], SQ[:,1])
        plt.xlim((xmin,xmax));plt.ylim((ymin,ymax))
        fig.canvas.mpl_connect('button_press_event', on_click)
        plt.pause(0.01);
        plt.show()
# Association de la fonction on_click à la figure
fig.canvas.mpl_connect('button_press_event', on_click)


plt.figure(1)
#plt.xlabel('x')
#plt.ylabel('y')
plt.title('Ecoulement 2D linéaire\nA = [[{},{}],[{},{}]]'.format(A11, A12, A21, A22))
plt.savefig('Ecoulement2D_Linear.png', dpi=80)
plt.show()