% This file is part of the project 
% UPS-L3 Fluid Mechanics online material
% Copyright (C) 2021-2023,  David Fabre et al.
% Distributed under GNU-GPL licence.

\documentclass[12pt,a4paper]{article}
\usepackage[top=1.5cm,left=1.5cm,right=1.5cm,bottom=1.5cm]{geometry}
\usepackage[french]{babel}
%\usepackage[francais]{babel}
\usepackage[utf8]{inputenc}
%\usepackage[T1]{fontenc}
\usepackage{graphicx, amsmath, amssymb, latexsym}

%\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{color}
%\usepackage{amsfonts}
%\usepackage{graphicx}
%\usepackage{enumerate}
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}
\newcommand{\Ro}{\ensuremath{{\mathcal{R}_0}}}
\newcommand{\RM}{\ensuremath{{\mathcal{R}_M}}}
\newcommand{\ex}{\ensuremath{\vec{e}_{x}}}
\newcommand{\ey}{\ensuremath{\vec{e}_{y}}}
\newcommand{\ez}{\ensuremath{\vec{e}_{z}}}
\newcommand{\er}{\ensuremath{\vec{e}_{\rho}}}
\newcommand{\et}{\ensuremath{\vec{e}_{\varphi}}}
\newcommand{\vp}{\ensuremath{\varphi}}
\newcommand{\e}  {\mathrm{e}}   
\newcommand{\dd}  {\textrm d}      
\newcommand{\im}  {\mathrm{i}} %\newcommand{\im}  {\textrm i}
\newcommand{\dpa}[2]  {\frac {\partial #1} {\partial #2}}   
\newcommand{\ddpa}[2] {\frac {\partial^{2} #1} {\partial #2 ^{2}}}   
\newcommand{\dto}[2]  {\frac {{\textrm d} #1} {{\textrm d} #2}}   
\newcommand{\ddto}[2] {\frac {{\textrm d}^{2} #1} {{\textrm d} #2 ^{2}}}

\begin{document}
\noindent
{\sc{Universit\'e Paul Sabatier}}\hfill{\sc{Ann\'ee universitaire 2022--2023}}\\
{\sc{N3 M\'ecanique, \'Energ\'etique}}\hfill{\sc{S5 MECA3\textunderscore FluME1-TP}}\\[.15cm]
\HRule\\[0.25cm]

\begin{center}
{\Large \bfseries Enonc\'e du TP 3  \\  Ecoulement visqueux pulsé}\\
%{\Large \bfseries séances 1 et 2}\\
\end{center}
\vspace{-.25cm}
\HRule\\[-.25cm]
\begin{center}
{\it Dur\'ee~: 2 heures (1 séance) \\
}
\end{center}

L'objectif de ce TP est d'approximer num\'eriquement un \'ecoulement induit entre deux plans par un gradient 
de pression oscillant.
Ce type de probl\`eme peut mod\'eliser la mani\`ere dont les battements du c{\oe}ur induisent un 
\'ecoulement puls\'e dans les vaisseaux sanguins.
En effet ces battements imposent une variation p\'eriodique du gradient de pression impos\'e 
entre deux sections transverses des vaisseaux.
L'objectif de cet exercice est de caract\'eriser ce type d'\'ecoulement
en particulier dans les limites des basses fr\'equences (organisme au repos) 
et hautes fr\'equences (activit\'e cardiaque intense).

Consid\'erons un mod\`ele d'\'ecoulement plan entre deux plaques
planes distantes de $2h$, g\'en\'er\'e par un gradient de pression
sinuso\"{\i}dal \`a pulsation $\omega$ fix\'ee~:
\[
\frac{dp}{dx} = K \cos\omega t.
\]
\noindent
Ici $K$ correspond à l'amplitude du gradient de pression impos\'e.
\begin{figure}[htb]
  \begin{center}
    \setlength{\unitlength}{1mm}
    \begin{picture}(60, 40)(0, 0)
      \put(0, 0){\includegraphics[width=6cm]{ecoulement_pulse}}
      \put(0, 6){$-h$}
      \put(0, 22){$+h$}
      \put(20, 20){$\partial p/\partial x = K \cos \omega t$}
    \end{picture}
  \end{center}
  \caption{\'Ecoulement puls\'e en conduite.}
  \label{fig:ecoulement_pulse}
\end{figure}
\section{Visulation de la solution analytique (en régime établi)}

\begin{enumerate}
\item Dans l'hypoth\`ese d'un \'ecoulement plan parall\`ele, et d'une vitesse $u$ nulle sur les 
parois inf\'erieure et sup\'erieure,
 montrer que
la vitesse horizontale $u(y, t)$ v\'erifie l'\'equation:
\begin{equation}
%\label{par}
  \frac{\partial u}{\partial t} 
  = 
  - \frac{K}{\rho} \cos \omega t + \nu \frac{\partial^2 u}{\partial y^2}
  \label{eq:ns}
\end{equation}

\item Par analyse dimensionnelle de cette équation, montrez que l'ordre de grandeur  du rapport entre le terme instationnaire et le terme visqueux est donné par le nombre de Stokes $St = \frac{\omega h^2}{\nu}$. 
A quoi peut-on s'attendre respectivement dans les cas $St\ll 1$ et $St \gg 1$ ? 
\end{enumerate}


Sous l'hypothèse d'un {\em régime établi}, le problème admet une solution analytique notée $u_{th}(y,t)$. Le forçage étant de la forme $\cos \omega t $ on peut rechercher la solution sous la forme $u_{th}(y,t) = u_c(y) \cos \omega t + u_s(y) \sin(\omega t)$. Il est cependant plus efficace d'utiliser la méthode de la variable complexe en posant $u_{th}(y, t) = Re \left \{ \underline{U}(y) e^{i \omega t} \right \}$ où $\underline{U}(y) = u_c(y) - i u_s(y)$ est une fonction à valeur complexe. En remplaçant $\cos \omega t$ par $e^{i\omega t}$  dans l'\'equation~(\ref{eq:ns}), et en cherchant la solution vérifiant les conditions limites $u(\pm h,t) = 0$, on obtient finalement l'expression du profil de vitesse instationnaire suivant~:
\begin{equation}
\label{sol}
u_h(y,t) = Re \left \{ \underline{U}(y) e^{i \omega t} \right \},\quad  \mbox{ avec }  \underline{U}(y) = \frac{iK}{\rho \omega} \left \{ 
1 - \frac{\cosh [ y ( 1+i) \sqrt{\omega / 2\nu} ]}{\cosh [ 
h ( 1+i) \sqrt{\omega / 2\nu} ]}
\right \}
\end{equation}

D'autre part, la force exercée sur les parois du tuyau, par unité de longueur dans la direction $x$, vaut 
\begin{equation}
F_x(t) = \tau_{xy}(y=h,t) - \tau_{xy}(y=-h,t), \quad \mbox{ avec  } \tau_{xy} = \mu \partial u/\partial y. 
\label{eq:defFx}
\end{equation}
en utilisant de nouveau la méthode de la variable complexe, on aboutit à :

\begin{equation}
\label{frot}
F_x(t) = Re \left\{ \underline{F_x} e^{i \omega t} \right\}, \quad \mbox{ avec } 
 \underline{F_x} = \frac{2 (1-i) \mu K}{\omega \sqrt{2\nu/\omega}} \tanh \left[ \frac{(1+i)h}{ \sqrt{2\nu/\omega}} \right].
\end{equation}


\begin{enumerate}
\setcounter{enumi}{2}
\item Retrouver les solutions (\ref{sol}) et (\ref{frot}) \`a partir de l'\'equation (\ref{eq:ns}).

\item La solution complète du problème est la solution établie précédente,
plus une solution particulière sans second membre, qui correspond à un
régime transitoire amorti et qui dépend des conditions
initiales. 

Estimez le temps caractéristique $\tau$ d'amortissement de ce transitoire.
 
%%% Pauline : tau = h^2/nu par analyse dimensionnelle de l'équation sans second membre

%\item En adimensionnant $t$ et $y$ respectivement par $1/\omega$
%et $h$ la solution se met sous la forme :
%\begin{equation}
%\label{soladim}
% \underline{U}^\star(y^\star) = \frac{i\gamma}{\Omega^2} \left \{
%1 - \frac{\cosh [ y^\star ( 1+i) \sqrt{\Omega} ]}{\cosh [ 
%( 1+i) \sqrt{\Omega} ]} \right \}
%\quad
%\mbox{avec} \quad
%\Omega = \frac{\omega h^2}{2\nu}, \; \gamma = \frac{Kh^3}{4\rho \nu^2}
%\end{equation}
%\noindent 
%avec $^\star$ exprimant le fait que ces variables sont adimensionn\'ees. 

\item On fournit un programme en \textsc{Python} \texttt{pulse.py} (ainsi qu'un programme équivalent en \textsc{Matlab} \texttt{pulse.m}) qui permet de visualiser cette solution théorique. A l'aide de ce programme visualiser les profils de vitesse pour différentes valeurs du nombre de Stokes $St$ prises dans l'intervalle $10^{-3} \leq St <10^3$.

Repr\'esenter sch\'ematiquement sur votre compte-rendu la forme du profil de vitesse
à différents instants du cycle, dans les deux r\'egimes asymptotiques des grands et petits $St$.
Commentez physiquement les résultats.

%\item  D'apr\`es vous que repr\'esente physiquement le param\`etre $\Omega$ ? 


\end{enumerate}

\section{Calcul num\'erique}
On propose de r\'esoudre num\'eriquement cette \'equation de diffusion 
1D instationnaire forc\'ee.
Ceci permettra d'une part de comparer l'approximation numérique avec
la solution théorique en régime établi, et d'autre part d'observer
le régime transitoire précédent la convergence vers ce régime établi.
Vous utiliserez les valeurs des paramètres donnés dans le tableau: 
$$
\begin{array}{|c|c|c|c|c|}
\hline
h & K/\rho & \nu & \omega & N_y \\
\hline
\hspace{2cm} & \hspace{2cm} & \hspace{2cm} & \hspace{2cm} & 100 \\
\hline
\end{array}
$$

 La solution num\'erique est calcul\'ee en utilisant des m\'ethodes aux diff\'erences finies.


La discrétisation spatiale est effectuée en introduisant $N_y+1$ points de maillage définis
par $y_i = i \Delta y-h$ avec $i=0..N_y$ et $\Delta y = 2 h /N_y$.

La discrétisation temporelle est effectuée en posant  $t^{(n)}=n\Delta t$ avec $n=0,..,N_t$.

Enfin, on pose $u_i^{(n)} = u(y_i,t^{(n)})$.

Le sch\'ema utilis\'e sera un Euler explicite d'ordre 1 en temps, et des diff\'erences finies centr\'ees d'ordre 2 en espace.
En utilisant ce schéma l'\'equation (\ref{eq:ns}) prend la forme:
\begin{equation}
\label{disc}
\frac{u_i^{(n+1)}-u_i^{(n)}}{\Delta t}=-\frac{K}{\rho} \cos( \omega t^{(n)} ) + \frac{\nu}{\Delta y^2} 
\left[ u_{i+1}^{(n)}-2u_i^{(n)}+u_{i-1}^{(n)} \right] 
\end{equation}
\noindent


Remarquons que du fait des conditions aux limites 
de non-glissement impos\'ees sur les parois on a $u(y_0,t^{(n)})=u(y_{N_y},t^{(n)})=0, \ \forall n$.

Pour la résolution numérique deux stratégies sont possibles :
\begin{itemize}
\item 
On peut construire un tableau à deux dimensions \verb| U(i,n)| contenant toutes les valeurs $u_{i}^{(n)}$, et remplir les unes après les autres les colonnes du tableau correspondant aux valeurs de $u$ aux instants successifs. L'inconvénient est de nécessiter un important stockage de mémoire, inutile si l'on ne s'intéresse qu'à l'état final.
\item 
En notant que le schéma ne fait intervenir que la solution à deux pas de temps successifs, on peut se contenter d'utiliser seulement deux vecteurs colonnes 
\verb| Ufutur | et \verb| Upresent |  contenant à chaque pas de temps les valeurs à l'instant 
"présent" $t^{(n)}$ et "futur"  $t^{(n+1)}$ (c.a.d. $ U^{present} = [u_0^{(n)};  u_1^{(n)}; ... ; u_{N_y}^{(n)} ] $ et   $U^{futur} = [u_0^{(n+1)}; u_1^{(n+1)};  ... ; u_{N_y}^{(n+1)}$].
\end{itemize}


\begin{enumerate}
\setcounter{enumi}{5}
\item Montrez qu'avec cette seconde idée le calcul de la solution à chaque itération temporelle se met sous la forme :
\begin{equation}
\label{mat}
U^{futur} =A\, U^{present} +F cos(\omega t^{(n)}).
\end{equation}
\noindent
Où $A$ est une matrice carrée correspondant 
\`a la discr\'etisation de l'op\'erateur Laplacien et $F$ est un vecteur colonne 
repr\'esentant le terme de for\c{c}age. Pr\'eciser les dimensions des
matrices et vecteurs mis en jeu dans (\ref{mat}), {\color{red} et expliquez la manière dont ont été intégrées les conditions aux limites.}

\item Ecrire un programme effectuant l'intégration temporelle du problème,
en fonction des différents paramètres physiques et numériques,
en partant de la condition initiale $u(y,t=0) = 0$, et jusqu'à un instant final $t_f$.

Votre programme devra de plus afficher deux figures :
\begin{itemize}
\item La forme de la solution  $u(y,t_f)$ à l'instant final de la simulation.
\item La force de frottement $F_x(t)$ exercée sur les parois, donnée par l'équation \ref{eq:defFx}, en fonction de $t$ au cours de l'intervalle $[0,t_f]$.
\end{itemize}



Dans votre comte-rendu, vous détaillerez la stratégie de programmation utilisée.


\item A l'aide de votre programme, calculez et tracez la solution numérique obtenue 
à un instant $t_f$ suffisamment grand pour que le régime établi soit atteint (on pourra prendre
par exemple $t_f = 10T$ où $T = 2\pi/\omega$, ou adapter cette valeur selon le cas étudié).

{\color{red}
Pour les paramètres physiques, vous prendrez les valeurs donnés dans le tableau ci-dessus. 

Pour les paramètres numériques, vous prendrez initialement $N_y= 100$ et $\Delta t = 10^{-5}$.
Vous déterminerez vous-même des valeurs de $N_y$ et $\Delta t$ permettant un bon compromis entre précision et temps de calcul.
}
%%% PAULINE : la condition de stabilité théorique est delta t / (delta y)^2 < 1/2

\item Qu'observe-t-on avec des valeurs de $\Delta t$ trop grandes ? Expliquez.

\item Superposez sur le même graphique la solution théorique $u_{th}(y,t_f)$ et la solution calculée numériquement.
Commentez les différences.

\item Sur la figure représentant $F_x(t)$, superposez la solution théorique (\ref{frot}) à la solution numérique. En déduire une estimation du temps caractéristique d'amortissement du régime transitoire. 

{\color{red} {\bf Annexe : }valeurs des paramètres à utiliser.

\footnotesize
$$
\begin{array}{|c|c|c|c|c|}
\hline 
\mbox{Trinôme} & h & K/\rho & \nu & \omega  \\
\hline
  1     &0.0120   &1  &5\cdot 10^{-5}  &    11 \\
    2    &0.0140  &1  &5\cdot 10^{-5} &   12\\
    3    &0.0160  &1  &5\cdot 10^{-5}   &13\\
    4    &0.0180  &1  &5\cdot 10^{-5}   &14\\
    5    &0.0200  &1  &7.5\cdot 10^{-5}   &15\\
    6    &0.0120  &1  &7.5\cdot 10^{-5}   &16\\
    7    &0.0140  &1  &7.5\cdot 10^{-5}   &17\\
    8    &0.0160  &1  &7.5\cdot 10^{-5}   &18\\
    9    &0.0180  &1  &7.5\cdot 10^{-5}   &19\\
   10    &0.0200 &1  &7.5\cdot 10^{-5}   &20\\
   11    &0.0120 &1  &1\cdot 10^{-4}   &21\\
   12    &0.0140  &1  &1\cdot 10^{-4}   &22\\
   13    &0.0160  &1  &1\cdot 10^{-4}   &23\\
   14    &0.0180  &1  &1\cdot 10^{-4}   &24\\
   15    &0.0200 &1   &1\cdot 10^{-4}   &25\\
%\hspace{2cm} & \hspace{2cm} & \hspace{2cm} & \hspace{2cm} & 100 \\
\hline
\end{array}
$$

%% NB : valeurs correspondantes du nombre de Stokes
%31.6800   47.0400   66.5600   90.7200  120.0000   
%30.7200   44.4267   61.4400   82.0800  106.6667   
%30.2400   43.1200   58.8800   77.7600  100.0000
}
%\item R\'e\'ecrire le sch\'ema (\ref{disc}) de mani\`ere implicite avec un schéma de Cranck-Nicholson.
%(c'est-\`a-dire que la discr\'etisation du laplacien est exprim\'ee au temps $n+1/2$, défini comme 
%moyenne des valeurs aux instants $n$ et $n+1$). Précisez l'ordre de ce schéma, et le traitement 
%choisi pour le second membre de l'équation différentielle.

%%% PAULINE : prendre $cos ( \omega t^{n+1/2} ) $ pour être cohérent 

%Ecrire ce nouveau schéma sous forme matricielle. 

%\item Ecrire un nouveau programme effectuant l'intégration numérique de ce nouveau schéma.

%\item Toujours pour les paramètres donnés dans le tableau et pour $t_f = 10 (2\pi/\omega)$, tracez et imprimer la solution numérique obtenue pour plusieurs valeurs de $\Delta t$. Comparez ces résultats à ceux obtenus avec le schéma explicite, et discutez en terme de précision, de stabilité, 
%et de temps de calcul.


\end{enumerate}

\end{document}