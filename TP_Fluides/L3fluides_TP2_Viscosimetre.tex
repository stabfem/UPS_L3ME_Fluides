% This file is part of the project 
% UPS-L3 Fluid Mechanics online material
% Copyright (C) 2021-2023,  David Fabre et al.
% Distributed under GNU-GPL licence.

\documentclass[12pt,a4paper]{article}
\usepackage[top=1.5cm,left=1.5cm,right=1.5cm,bottom=1.5cm]{geometry}
\usepackage[french]{babel}
%\usepackage[francais]{babel}
\usepackage[utf8]{inputenc}
%\usepackage[T1]{fontenc}
\usepackage{graphicx, amsmath, amssymb, latexsym}
\usepackage{hyperref}
%\usepackage{amsmath}
%\usepackage{amsfonts}
%\usepackage{graphicx}
%\usepackage{enumerate}
\usepackage{color}
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}
\newcommand{\Ro}{\ensuremath{{\mathcal{R}_0}}}
\newcommand{\RM}{\ensuremath{{\mathcal{R}_M}}}
\newcommand{\ex}{\ensuremath{\vec{e}_{x}}}
\newcommand{\ey}{\ensuremath{\vec{e}_{y}}}
\newcommand{\ez}{\ensuremath{\vec{e}_{z}}}
\newcommand{\er}{\ensuremath{\vec{e}_{\rho}}}
\newcommand{\et}{\ensuremath{\vec{e}_{\varphi}}}
\newcommand{\vp}{\ensuremath{\varphi}}
\newcommand{\e}  {\mathrm{e}}   
\newcommand{\dd}  {\textrm d}      
\newcommand{\im}  {\mathrm{i}} %\newcommand{\im}  {\textrm i}
\newcommand{\dpa}[2]  {\frac {\partial #1} {\partial #2}}   
\newcommand{\ddpa}[2] {\frac {\partial^{2} #1} {\partial #2 ^{2}}}   
\newcommand{\dto}[2]  {\frac {{\textrm d} #1} {{\textrm d} #2}}   
\newcommand{\ddto}[2] {\frac {{\textrm d}^{2} #1} {{\textrm d} #2 ^{2}}}

\newcommand{\rrtensor}[1]{\overline{\overline{#1}}}
\newcommand{\EE}[1]{ \cdot 10^{#1}}

\begin{document}
\noindent
{\sc{Universit\'e Paul Sabatier}}\hfill{\sc{Ann\'ee universitaire 2023--2024}}\\
{\sc{L3 M\'ecanique, \'Energ\'etique}}\hfill{\sc{S5 MECA3\textunderscore FluME1-TP}}\\[.15cm]
\HRule\\[0.25cm]
%{\large Nom~:\hfill Section~:\hspace{4cm}~\\[0.5cm]
 %Pr\'enom~:\hfill Groupe~:\hspace{3.85cm}~\vspace{-.0cm}}\\
%\HRule\\[-.25cm]
\begin{center}
{\Large \bfseries Enonc\'e du TP 2  \\  Viscosimètre rotatif}\\
%{\Large \bfseries séances 1 et 2}\\
\end{center}
\vspace{-.25cm}
\HRule\\[-.25cm]
\begin{center}
{\it Dur\'ee~: 2 heures (1 séance) \\
}
\end{center}

%\begin{figure}[!b]
%\begin{center}
%\includegraphics[width=17cm]{theorie.png}
%\end{center}
%\end{figure}

\section{Objectif}

\noindent Il s'agit de comprendre le fonctionnement d'un viscosimètre rotatif, et de l'utiliser pour caractériser la variation avec la température de la viscosité de la glycérine, puis de caractériser le caractère Newtonien ou non Newtonien de plusieurs fluides alimentaires.


\section{Elements de théorie}

La viscosité se définit, de manière très générale, comme le rapport entre les {\bf efforts} et les {\bf taux de déformation} dans un fluide en mouvement. Cette relation peut s'écrire de plusieurs manières différentes :

\begin{itemize}
\item Pour un écoulement élémentaire parallèle de la forme ${\bf u} = u(y) {\bf e}_x$, la viscosité est le rapport entre la contrainte de cisaillement $\tau_{xy}$ et le taux de cisaillement $\dot \gamma = \partial u/\partial y$ :
\begin{equation}
\tau_{xy} = \mu \dot \gamma
\label{eq:visc1}
\end{equation}

\item Pour un écoulement général (de fluide Newtonien), cette relation se généralise en faisant intervenir le tenseur 
$\rrtensor{\tau}$ des contraintes visqueuses et le tenseur $\rrtensor{D}$ des taux de déformation :

\begin{equation}
\rrtensor{\tau} = 2 \mu \rrtensor{D}
\label{eq:visc2}
\end{equation}

\item Pour l'écoulement autour de l'aiguille d'un viscosimètre, la relation est entre le couple $M_z$ exercé sur l'aiguille et le la vitesse de rotation angulaire $\Omega$ de celle-ci:

\begin{equation}
M_z = K \mu \Omega,
\label{eq:visc3}
\end{equation}
où $K$ est une constante dépendant de la géométrie de l'aiguille.

\end{itemize}

Un fluide est dit {\bf Newtonien} si la loi rhéologique reliant efforts et déformations est linéaire, et représentée par une droite. Dans ce cas, la viscosité est constante, et la viscosité $\mu$ correspond à la pente de la droite.

Inversement, un fluide est dit {\bf non Newtonien} si la loi rhéologique reliant efforts et déformations est non linéaire. Pour mettre en évidence ce caractère non linéaire, on peut tracer l'effort $M_z$ en fonction du taux de rotation 
$\Omega$. On distingue deux principaux types de comportements : si la pente de la courbe décroît (c.a.d. si la viscosité effective décroît avec le taux de rotation) le fluide est dit {\em rhéofluidifiant} ; si au contraire la pente de la courbe croît (c.a.d. si la viscosité effective croît avec le taux de rotation) le fluide est dit {\em rhéoépaississant}.


\section{Utilisation du viscosimètre}

Le viscosimètre dispose de 4 aiguilles, adaptées à l'étude de fluides ayant des viscosités très diverses (l'aiguille 1 étant adaptée aux fluides les moins visqueux et l'aiguille 4 aux fluides les plus visqueux).  Le taux de rotation $\Omega$ peut être choisi entre $1.5 RPM$ (rotations par minute) et 60 RPM.

Une mesure consiste à :

1/ Choisir une aiguille et un taux de rotation, 2/ lancer la mesure (appuyer sur  \texttt{ Measure}). 



Après un temps de stabilisation (qui peut être long pour les valeurs les plus faibles du taux de rotation), le viscosimètre affiche deux informations : $(a)$ le couple mesuré, exprimé en pourcentage du couple maximal $M_{z,max}$ dont la valeur est donnée dans le tableau 1, et $(b)$ la viscosité dynamique\footnote{Le viscosimètre affiche également la valeur de la viscosité cinématique $\nu = \mu/\rho$, à condition que la masse volumique $\rho$ ait été saisie dans celui-ci.} déduite de cette mesure,  compte tenu de la constante $K$ de l'aiguille, également donnée dans le tableau (cette constante est réglée dans le viscosimètre par un calibrage initial).

La valeur mesurée est fiable si le couple affiché est compris entre $20\%$ et $100\%$. Pour effectuer une mesure il vous faudra déterminer par vous-même quelle aiguille et quelle valeur du taux de rotation utiliser pour avoir une mesure fiable. 

{\color{red} {\bf Remarque importante :} après chaque mesure appuyez sur \texttt{Stop} avant de changer le taux de rotation et/ou l'aiguille, puis relancer la mesure suivante en appuyant sur \texttt{Measure}, sinon les mesures ne seront pas valides !
} 



%Ce calibrage a été réalisé par le fabriquant de l'appareil, qui affiche donc directement une valeur de la viscosité.
\begin{table}
$$
\begin{array}{ll}
\hline
\text{Couple maximal} & M_{z,max} = 4.74 \EE{-5} Nm \\
\hline
\end{array}
$$
$$ 
\begin{array}{lll}
\hline
\text{Aiguille} & \text{Constante } K & \\
\hline
1 & 7.475 \EE{-5} & Nm \cdot (rad/s)^{-1} \cdot (Pa.s)^{-1} \\
2 & 1.092\EE{-5} & Nm \cdot (rad/s)^{-1} \cdot (Pa.s)^{-1} \\
3 & 3.063\EE{-6} & Nm \cdot (rad/s)^{-1} \cdot (Pa.s)^{-1} \\
4 & 7.065e\EE{-7} & Nm \cdot (rad/s)^{-1} \cdot (Pa.s)^{-1} \\
\hline

\end{array}
$$
\caption{Paramètres du viscosimètre : Couple maximal, et constante $K$ pour les aiguilles 1,2,3,4.}
\end{table}










%\newpage


\section{Partie théorique : écoulement entre deux cylindres}



La constante de proportionnalité $K$ apparaissant dans la loi \ref{eq:visc3} n'est en général pas connue analytiquement et doit être déterminée par un calibrage de l'appareil. 

Il y a cependant un cas classique dans lequel la constante peut être déterminée par le calcul ; il s'agit du cas où 
l'aiguille est un cylindre de rayon $R_1$ et de hauteur $H$, et où le récipient contentant le fluide est lui aussi un cylindre de rayon $R_2$ et de même hauteur $H$. Cette situation est une configuration classique appelée {\bf écoulement de Couette-Taylor}.

Dans ce cas on peut démontrer que la valeur de $K$ est la suivante :

\begin{equation}
 K = \frac{ 4 \pi R_1^2 R_2^2 H}{R_2^2-R_1^2}
\label{eq:KCT}
\end{equation}

\begin{enumerate}
\item Démontrez cette relation (on pourra suivre la démarche de l'exercice 3.3 du fascicule de TD).

\item Parmi les 4 aiguilles fournies par le fabriquant du viscosimètre, les aiguilles numéro 1 et 4 sont de forme approximativement cylindrique. Après avoir mesuré leurs dimensions, donnez la valeur attendue du coefficient de proportionnalité $K$. Comparez avec les valeurs données dans le tableau ci-dessus. Que constatez-vous ?
\end{enumerate}



\clearpage

\section{Manipulation}

\subsection{Mise en évidence de la loi rhéologique de plusieurs fluides alimentaires }

On propose de caractériser la loi rhéologique de différents fluides, principalement alimentaires.
{\color{red} Vous utiliserez au moins 3 fluides parmi les suivants mis à disposition :
\begin{itemize}
\item Huile de friture, 
\item Ketchup, 
\item Mayonnaise,
\item Pâte à tartiner, 
\item Miel, 
\item Solution de maïzena (amidon de maïs).
%\item Suspension de micro-sphères.
\end{itemize}
}
\,

Pour chacun des trois fluides :
\begin{enumerate}
\setcounter{enumi}{2}
\item Après avoir déterminé la bonne aiguille à utiliser, effectuez plusieurs mesures (3 ou 4) pour plusieurs valeurs du taux de rotation choisies dans la gamme de fiabilité du viscosimètre.
{\color{red} On pourra retenir également des valeurs avec un couple compris entre 5\% et 20\%, mais on précisera dans le compte-rendu que la mesure est moins fiable.}

\item Tracez le couple $M_z$ en fonction de $\Omega$, puis la viscosité cinématique effective en fonction de $\Omega$. 
Que pouvez-vous en conclure concernant la loi rhéologique du fluide ?
\end{enumerate}



\subsection{Variation de la viscosité en fonction de la température}

Pour cette seconde expérience, on utilisera de la glycérine. On utilisera le chauffage thermostatique pour contrôler la température.

\begin{enumerate}
\setcounter{enumi}{4}
\item A l'aide du viscosimètre, Mesurer la viscosité dynamique $\mu$ du fluide pour 4 à 8 valeurs de la température prises dans l'intervalle $T \in [8^oC : 50 ^oC]$.

\item {\color{red} Tracez $\mu$ en fonction de $T$ (en Kelvin) en graduations linéaires, puis $\mu$ en fonction de $1/T$ en graduations semi-logarithmiques}.

%en coordonnées semi-logarithmiques, la viscosité $\mu$ en fonction de $T$ (en Kelvin).

\item Un modèle théorique classique, appelé loi d'Andrage, prédit que la viscosité d'un liquide varie selon une expression de la forme. 
$$
\mu = A e^{b/T}
$$
A l'aide de vos résultats expérimentaux, estimez les coefficients $A$ et $b$ (en précisant leur unité) caractérisant la glycérine.

 
\end{enumerate}


\end{document}

